<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

use Auth;
use Illuminate\Http\Request;
use App\Models\Menu;
use App\Models\Domain;
use Webpatser\Uuid\Uuid;
use DB;

class MenuController extends Controller
{
    
    public function __construct(){
        $this->storage=Redis::connection();
    }

    public function index($accountId)
    {
        $menus= Cache::rememberForever($accountId . '_menu',function() use($accountId) {

            $menus = Menu::where('domain_uuid',$accountId)->get();

            $i=0;
            $menuJson = array();
            foreach($menus as $menu) {
                
                $menuJson[$i] = [
                'displayName'=> $menu->menu_name,
                'name'=>$menu->menu_name,
                'id'=>$menu->menu_uuid,
                'record_pin'=>$menu->menu_number,
                'menu_greeting_long'=>$menu->menu_greeting_long,
                'menu_greeting_short'=>$menu->menu_greeting_short,
                'menu_timeout'=>$menu->menu_timeout,
                'interdigit_timeout'=>$menu->interdigit_timeout,
                ];

                $i++;
            }
            return $menuJson;
        });
        
        return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>$menus,
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);
    }

    public function show($accountId,$menuId)
    {
        try {
            $menu= Cache::rememberForever($accountId . '_menu_' . $menuId,function() use($accountId,$menuId) {
                
                $menu = Menu::where('domain_uuid',$accountId)->where('menu_uuid',$menuId)->first();
               
                $menuJson=[
                    'id'=>$menu->menu_uuid,
                    'record_pin'=> $menu->menu_number,
                    'retries'=>$menu->number_of_incorrect_retries,
                    'name'=>$menu->menu_name,
                    'max_extension_length'=>$menu->max_extension_length,
                    // 'menu_greeting_long'=>$menu->menu_greeting_long,
                    // 'menu_greeting_short'=>$menu->menu_greeting_short,
                    // 'menu_timeout'=>$menu->menu_timeout,
                    'interdigit_timeout'=>$menu->interdigit_timeout,
                    'allow_record_from_offnet'=>false,
                    'suppress_media'=>false,
                    'timeout'=>$menu->timeout,
                    'hunt'=>$menu->hunt,
                    'type'=>"main",
                    'media'=>[
                        'exit_media'=>true,
                        'invalid_media'=>true,
                        'transfer_media'=>true,
                        ],
                    'ui_metadata'=>[
                        'origin'=>"callflows",
                        'ui'=>"monster-ui",
                        'version'=>"3.23",
                    ]
                ];

                return $menuJson;
            });
            
            return response()->json([
                    'auth_token'=> (string)Auth::getToken(),
                    'data'=>$menu,
                    'status'=>'success',
                    'request_id'=> uniqid(),
                    'revision'=> '{REVISION}',
                    'status_code'=>200
                ]);
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        }
    }

    public function create(Request $request,$accountId)
    {
        try {
            
            $menu = new Menu();
            $menu->menu_uuid =(string) Uuid::generate();
            $menu->domain_uuid=$accountId;
            
            if(isset($request->data['record_pin'])){
                $menu->menu_number=$request->data['record_pin'];
            } 
            
            if(isset($request->data['name'])){
                $menu->menu_name = $request->data['name'];
            }
            
            if(isset($request->data['retries'])){
                $menu->number_of_incorrect_retries = $request->data['retries'];
            }

            if(isset($request->data['extra']['retries'])){

            }
            if(isset($request->data['timeout'])){
                $menu->menu_timeout = $request->data['timeout'];
            }
            
            if(isset($request->data['interdigit_timeout'])){
                $menu->interdigit_timeout = $request->data['interdigit_timeout'];
            }
            
            if(isset($request->data['suppress_media'])){

            }

            if(isset($request->data['max_extension_length'])){

            }
            
            if(isset($request->data['allow_record_from_offnet'])){

            }

            if(isset($request->data['hunt'])){

            }
            
            if(isset($request->data['media'])){
                if(isset($request->data['media']['greeting'])){
                    
                }
                if(isset($request->data['media']['invalid_media'])){
                    
                }
                if(isset($request->data['media']['transfer_media'])){
                    
                }
                if(isset($request->data['media']['exit_media'])){

                }
            }

            if(isset($request->data['ui_metadata'])){
                if($request->data['ui_metadata']['version']){
                    
                }
                if($request->data['ui_metadata']['ui']){
                    
                }
                if($request->data['ui_metadata']['origin']){

                }
            }

            $menu->save();
            
            Cache::forget($accountId . '_menu');

            
            return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>array_merge(['id'=>$menu->menu_uuid],$request->data),
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);

        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        }
    }
    public function update(Request $request,$accountId,$menuId)
    {
        try {

            $menu = Menu::find($menuId);
            
            if(isset($request->data['record_pin'])){
                $menu->menu_number=$request->data['record_pin'];
            } 
            
            if(isset($request->data['name'])){
                $menu->menu_name = $request->data['name'];
            }
            
            if(isset($request->data['retries'])){
                $menu->number_of_incorrect_retries = $request->data['retries'];
            }
            if(isset($request->data['extra']['retries'])){

            }
            if(isset($request->data['timeout'])){
                $menu->menu_timeout = $request->data['timeout'];
            }
            
            if(isset($request->data['interdigit_timeout'])){
                $menu->interdigit_timeout = $request->data['interdigit_timeout'];
            }
            
            if(isset($request->data['suppress_media'])){

            }

            if(isset($request->data['max_extension_length'])){

            }
            
            if(isset($request->data['allow_record_from_offnet'])){

            }

            if(isset($request->data['hunt'])){

            }
            
            if(isset($request->data['media'])){
                if(isset($request->data['media']['greeting'])){
                    
                }
                if(isset($request->data['media']['invalid_media'])){
                    
                }
                if(isset($request->data['media']['transfer_media'])){
                    
                }
                if(isset($request->data['media']['exit_media'])){

                }
            }

            if(isset($request->data['ui_metadata'])){
                if($request->data['ui_metadata']['version']){
                    
                }
                if($request->data['ui_metadata']['ui']){
                    
                }
                if($request->data['ui_metadata']['origin']){

                }
            }
            $menu->save();
            Cache::forget($accountId . '_menu');
            Cache::forget($accountId . '_menu_' . $menuId);
            
            return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>array_merge(['id'=>$menu->menu_uuid],$request->data),
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);
            
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        }
          
    }
    
    public function delete($accountId,$menuId)
    {
        if($menu=Menu::find($menuId)){
            if(Menu::find($menuId)->delete()){
                Cache::forget($accountId . '_menu');
                Cache::forget($accountId . '_menu_' . $menuId);
                
                $menuJson=[
                    'id'=>$menu->menu_uuid,
                    'menu_number'=> $menu->menu_number,
                    'retries'=>$menu->number_of_incorrect_retries,
                    'name'=>$menu->menu_name,
                    'max_extension_length'=>$menu->max_extension_length,
                    'menu_greeting_long'=>$menu->menu_greeting_long,
                    'menu_greeting_short'=>$menu->menu_greeting_short,
                    'menu_timeout'=>$menu->menu_timeout,
                    'interdigit_timeout'=>$menu->interdigit_timeout,
                    'allow_record_from_offnet'=>false,
                    'record_pin'=>"3232",
                    'suppress_media'=>false,
                    'timeout'=>100,
                    'hunt'=>$menu->hunt,
                    'type'=>"main",
                    'media'=>[
                        'exit_media'=>true,
                        'invalid_media'=>true,
                        'transfer_media'=>true,
                        ],
                    'ui_metadata'=>[
                        'origin'=>"callflows",
                        'ui'=>"monster-ui",
                        'version'=>"3.23",
                    ]
                ];
                
                return response()->json([
                    'auth_token'=> (string)Auth::getToken(),
                    'data'=>$menuJson,
                    'status'=>'success',
                    'request_id'=> uniqid(),
                    'revision'=> '{REVISION}',
                    'status_code'=>200
                ]);
            }
        }
    }
}

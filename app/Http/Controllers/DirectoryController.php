<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

use Auth;
use Illuminate\Http\Request;
use App\Models\Directory;
use Webpatser\Uuid\Uuid;
use DB;

class DirectoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->storage=Redis::connection();
    }

    public function index($accountId)
    {
        $directories= Cache::rememberForever($accountId . '_directory',function() use($accountId){

            $directories=Directory::where('domain_uuid',$accountId)->get();

            $i=0;
            $directoryJson = array();

            foreach($directories as $directory) {

                $directoryJson[$i]=[
                    'id'=> $directory->directory_uuid,
                    'name'=>$directory->name,
                    'displayName'=>$directory->name,
                    'users'=>[]
                ];

                $i++;
            }
            return $directoryJson;
        });

        return response()->json([
            'auth_token'=> (string)Auth::getToken(),
            'data'=>$directories,
            'status'=>'success',
            'request_id'=> uniqid(),
            'revision'=> '{REVISION}',
            'status_code'=>200
        ]);
    }


    public function show($accountId,$directoryId)
    {
        try {
            $directory= Cache::rememberForever($accountId . '_directory_' . $directoryId,function() use($accountId,$directoryId){           

                $directory=Directory::where('domain_uuid',$accountId)->where('directory_uuid',$directoryId)->first();
            
                
                $directoryJson['id']=$directory->directory_uuid; 

                $directoryJson['name']=$directory->name;
                $directoryJson['max_dtmf']=$directory->max_dtmf;
                $directoryJson['min_dtmf']=$directory->min_dtmf;
                $directoryJson['sort_by']=$directory->sort_by;
                $directoryJson['confirm_match']= $directory->confirm_match;

                $directoryJson['ui_metadata']= [
                    'origin'=> 'voip',
                    'ui'=> 'monster-ui',
                    'version'=> '3.23'
                ];
                $directoryJson['users']= [];

                return $directoryJson;
            });
            
            return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>$directory,
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);

        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        }
        
    }

    
    public function store(Request $request,$accountId)
    {
        
        try {
            // return response()->json(['status'=>'ok']);
            $directory=new Directory();
            $directory->directory_uuid=(string) Uuid::generate();
            $directory->domain_uuid=$accountId;
            $directory->name=$request->data['name'];
            $directory->confirm_match=$request->data['confirm_match'];
            $directory->max_dtmf=$request->data['max_dtmf'];
            $directory->min_dtmf=$request->data['min_dtmf'];
            
            if(isset($request->data['select_user_id'])){
                $directory->select_user_id=$request->data['select_user_id'];
            }
            
            $directory->sort_by=$request->data['sort_by'];
            
            $directory->save();
            
            Cache::forget($accountId . '_directory');

            return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>array_merge(['id'=>$directory->directory_uuid],$request->data),
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);

            
        } catch (\Illuminate\Database\QueryException $e) {
             return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        }
    }


    public function update(Request $request, $accountId,$directoryId)
    {
        try {
            $directory=Directory::find($directoryId);
            $directory->name=$request->data['name'];
            $directory->confirm_match=$request->data['confirm_match'];
            $directory->max_dtmf=$request->data['max_dtmf'];
            $directory->min_dtmf=$request->data['min_dtmf'];
            $directory->select_user_id=$request->data['select_user_id'];
            $directory->sort_by=$request->data['sort_by'];

            $directory->save();


            Cache::forget($accountId . '_directory_'. $directoryId);
            Cache::forget($accountId . '_directory');


            return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>array_merge(['id'=>$directory->directory_uuid],$request->data),
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);

        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        } catch(\Exception $e){
           return response()->json(array(
            'error' => $e->getMessage(),
            'status' => 'failed'
        ));
       }
    }


    public function destroy($accountId,$directoryId)
    {   
        if($directory=Directory::find($directoryId)){
            if(Directory::find($directoryId)->delete()){
            
                Cache::forget($accountId . '_directory_'. $directoryId);
                Cache::forget($accountId . '_directory');

                $directoryJson['id']=$directory->directory_uuid;  
                $directoryJson['name']=$directory->name;
                $directoryJson['max_dtmf']=$directory->max_dtmf;
                $directoryJson['min_dtmf']=$directory->min_dtmf;
                $directoryJson['sort_by']=$directory->sort_by;

                $directoryJson['confirm_match']= false;

                $directoryJson['ui_metadata']= [
                    'origin'=> 'voip',
                    'ui'=> 'monster-ui',
                    'version'=> '3.23'
                ];
                $directoryJson['users']= [];

                return response()->json([
                    'auth_token'=> (string)Auth::getToken(),
                    'data'=>$directoryJson,
                    'status'=>'success',
                    'request_id'=> '{REQUEST_ID}',
                    'revision'=> '{REVISION}',
                    'status_code'=>200
                ]);
            }
        }
    }
}

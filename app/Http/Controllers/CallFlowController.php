<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

use Auth;
use Illuminate\Http\Request;
use App\Models\CallFlow;
use App\Models\CallFlowNumber;
use Webpatser\Uuid\Uuid;
use DB;

class CallFlowController extends Controller
{

    public function __construct(){
        $this->storage=Redis::connection();
    }

    
    public function index($accountId)
    {
        //$callflows= Cache::rememberForever($accountId . '_callflow',function() use($accountId){
            $callflows = CallFlow::where('domain_uuid',$accountId)->get();
            $i=0;
            $callflowJson = array();
            foreach($callflows as $callflow) {

                $numbers=CallFlowNumber::where('callflow_uuid',$callflow->callflow_uuid)->get();
                $j=0;
                $numbersJson=array();
                foreach ($numbers as $number) {
                    $numbersJson[$j]= $number->number;
                    $j++;
                }
                

                $callflowJson[$i]=[
                    'id'=> $callflow->callflow_uuid,
                    'name'=>$callflow->name,
                    'numbers'=>$numbersJson,
                    'group_id'=>$callflow->group_uuid,
                    'owner_id'=>$callflow->owner_uuid,
                    'patterns'=>explode(',',$callflow->patterns),
                    'description'=>$callflow->description,
                    'type'=>$callflow->type,
                    'contact_list_excluded'=>'',
                    'ui_is_main_numbers_cf'=>$callflow->ui_is_main_numbers_cf,
                    'ui_metadata'=>[
                        'origin'=>"callflows",
                        'ui'=>"monster-ui",
                        'version'=>"4.1-49",
                    ],
                    'messages'=>"messages"

                ];

                if($callflow->featurecode!=null or $callflow->featurecode!=''){
                    $callflowJson[$i]['featurecode']=json_decode($callflow->featurecode);
                }else{
                    $callflowJson[$i]['featurecode']=false;
                }
                

                $i++;
            }
            //return $callflowJson;
        //});
        
        return response()->json([
            'auth_token'=> (string)Auth::getToken(),
            'data'=>$callflowJson,
            'status'=>'success',
            'request_id'=> uniqid(),
            'revision'=> '{REVISION}',
            'status_code'=>200
        ]);
    }


    public function show($accountId,$callflowId)
    {
        try {

            // $callflow= Cache::rememberForever($accountId . '_callflow_'. $callflowId,function() use($accountId,$callflowId){

                $callflow=CallFlow::where('callflow_uuid',$callflowId)->where('domain_uuid',$accountId)->first();
                
                $callflowJson=[];

                if($callflow){
                    $numbers=CallFlowNumber::where('callflow_uuid',$callflow->callflow_uuid)->get();
                    $j=0;
                    $numbersJson=array();
                    foreach ($numbers as $number) {
                        $numbersJson[$j]= $number->number;
                        $j++;
                    }
                    
                    $flow=json_decode($callflow->flow);
                    
                    /*
                        'metadata'=>[
                            'cb69bf00-fd2a-11e7-89d6-cfe1d4e75f8b'=>[
                                'name'=>"test media",
                                'pvt_type'=>"media"
                            ]
                        ],
                    */
                    
                    
                    $callflowJson=[
                        'id'=>$callflow->callflow_uuid,
                        'title'=>$callflow->name,
                        'name'=>$callflow->name,
                        'numbers'=>$numbersJson,
                        'group_id'=>$callflow->group_uuid,
                        'owner_id'=>$callflow->owner_uuid,
                        'patterns'=>explode(',',$callflow->patterns),
                        'description'=>$callflow->description,
                        'type'=>$callflow->type,
                        'metadata'=>$flow,
                        'ui_metadata'=>[
                            'origin'=>"callflows",
                            'ui'=>"monster-ui",
                            'version'=>"4.1-49",
                        ],
                        'contact_list_excluded'=>'',
                        'ui_is_main_numbers_cf'=>'ui_is_main_numbers_cf',
                        'flow'=>$flow,
                        'messages'=>"messages"
                    ];

                    if($callflow->featurecode!=null or $callflow->featurecode!=''){
                        $callflowJson['featurecode']=json_decode($callflow->featurecode);
                    }else{
                        $callflowJson['featurecode']=false;
                    }
                }
                
                // return $callflowJson;
            // });
            
            return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>$callflowJson,
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);

        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        }
    }

    public function searchByNumber($accountId,$number){
        // t=callflow&q=number&v={value}
        try {
            $callflowId=CallFlowNumber::where('number',$number)->first();
            $callflowJson=[];
            if($callflowId){
                $callflow=CallFlow::where('domain_uuid',$accountId)->where('callflow_uuid')->first();
                if($callflow){
                    $numbers=CallFlowNumber::where('callflow_uuid',$callflow->callflow_uuid)->get();
                    $j=0;
                    $numbersJson=array();
                    foreach ($numbers as $number) {
                        $numbersJson[$j]= $number->number;
                        $j++;
                    }
                    // $numbers=json_decode($callflow->numbers);
                    
                    $callflowJson=[
                        'id'=>$callflow->callflow_uuid,
                        'title'=>$callflow->name,
                        'name'=>$callflow->name,
                        'numbers'=>$numbersJson,
                        'group_id'=>$callflow->group_uuid,
                        'owner_id'=>$callflow->owner_uuid,
                        'patterns'=>$callflow->patterns,
                        'description'=>$callflow->description,
                        'type'=>$callflow->type,
                        'featurecode'=>json_decode($callflow->featurecode),
                        // 'contact_list'=>['exclude'=>$callflow->contact_list_excluded],
                        // 'ui_is_main_numbers_cf'=>$callflow->ui_is_main_numbers_cf,
                        'flow'=>json_decode($callflow->flow)
                    ];

                    if($callflow->featurecode){
                        $callflowJson['featurecode']=$callflow->featurecode;
                    }else{
                        $callflowJson['featurecode']=false;
                    }
                }
            }
            return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>$callflowJson,
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        }
    }
    

    public function store(Request $request,$accountId)
    {
        try {

            $callflow=new CallFlow();
            $callflow->callflow_uuid=(string) Uuid::generate();
            $callflow->domain_uuid=$accountId;
            
            if(isset($request->data['name'])){
                $callflow->name=$request->data['name'];
            }
            
            if(isset($request->data['numbers'])){
                foreach ($request->data['numbers'] as $num) {
                    $number=new CallFlowNumber();
                    $number->domain_uuid=$accountId;
                    $number->callflow_uuid=$callflow->callflow_uuid;
                    $number->number=$num;
                    $number->save();
                }
            }
            
            if(isset($request->data['patterns'])){
                $callflow->patterns=json_encode($request->data['patterns']);
            }

            if(isset($request->data['type'])){
                $callflow->type=$request->data['type'];
            }

            if(isset($request->data['owner_id'])){
                $callflow->owner_uuid=$request->data['owner_id'];
            }

            if(isset($request->data['group_id'])){
                $callflow->group_uuid=$request->data['group_id'];
            }

            if(isset($request->data['description'])){
                $callflow->description=$request->data['description'];
            }
            
            
            if(isset($request->data['featurecode'])){
                $callflow->featurecode=json_encode($request->data['featurecode']);
            }
            
            if(isset($request->data['flow'])){
                //Added the flag JSON_FORCE_OBJECT coz empty children were getting saved as array [] and not as object {}, causing an issue for backend telephony system
                $callflow->flow=json_encode($request->data['flow'], JSON_FORCE_OBJECT);
            }
            
           
            Cache::forget($accountId . '_callflow');
            
            $callflow->save();
            // $request->data['id']=$callflow->callflow_uuid;

            // $request->data=array_merge($request->data['id'],$request->data);

            return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>array_merge(['id'=>$callflow->callflow_uuid],$request->data),
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);

        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        }
    }

    
    public function update(Request $request,$accountId,$callflowId)
    {

        try {
            // return json_encode($request->data['flow']);
            $callflow=CallFlow::find($callflowId);

            if(isset($request->data['name'])){
                $callflow->name=$request->data['name'];
            }

            if(isset($request->data['numbers'])){
                foreach ($request->data['numbers'] as $num) {
                    $number=new CallFlowNumber();
                    $number->domain_uuid=$accountId;
                    $number->callflow_uuid=$callflow->callflow_uuid;
                    $number->number=$num;
                    $number->save();
                }
            }
            
            if(isset($request->data['owner_id'])){
                $callflow->owner_uuid=$request->data['owner_id'];
            }

            if(isset($request->data['type'])){
                $callflow->type=$request->data['type'];
            }
            
            if(isset($request->data['group_id'])){
                $callflow->group_uuid=$request->data['group_id'];
            }

            if(isset($request->data['patterns'])){
                $callflow->patterns=json_encode($request->data['patterns']);
            }

            if(isset($request->data['description'])){
                $callflow->description=$request->data['description'];
            }
            
            if(isset($request->data['featurecode'])){
                $callflow->featurecode=json_encode($request->data['featurecode']);
            }
            
            if(isset($request->data['flow'])){
                //Added the flag JSON_FORCE_OBJECT coz empty children were getting saved as array [] and not as object {}, causing an issue for backend telephony system
                $callflow->flow=json_encode($request->data['flow'], JSON_FORCE_OBJECT);
            }

            Cache::forget($accountId . '_callflow_'. $callflowId);
            Cache::forget($accountId . '_callflow');
            
            $callflow->save();
            $request->data['id']=$callflow->callflow_uuid;
           

            return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>array_merge(['id'=>$callflow->callflow_uuid],$request->data),
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);

        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        }
    }

    
    public function destroy($accountId,$callflowId)
    {
        if($callflow=CallFlow::find($callflowId)){
            if(CallFlow::find($callflowId)->delete()){
                
                Cache::forget($accountId . '_callflow_'. $callflowId);
                Cache::forget($accountId . '_callflow');
                
                $flows=json_decode($callflow->flow);

                // deleting the numbers against callflow
                CallFlowNumber::where('callflow_uuid',$callflow->callflow_uuid)->delete();
                
                $callflowJson=[
                    'id'=>$callflow->callflow_uuid,
                    'title'=>$callflow->name,
                    'name'=>$callflow->name,
                    'numbers'=>'',
                    'group_id'=>$callflow->group_uuid,
                    'owner_id'=>$callflow->owner_uuid,
                    'patterns'=>$callflow->patterns,
                    'description'=>$callflow->description,
                    'type'=>$callflow->type,
                    'featurecode'=>json_decode($callflow->featurecode),
                    // 'contact_list'=>['exclude'=>$callflow->contact_list_excluded],
                    // 'ui_is_main_numbers_cf'=>$callflow->ui_is_main_numbers_cf,
                    'flow'=>json_decode($callflow->flow)
                ];

                return response()->json([
                    'auth_token'=> (string)Auth::getToken(),
                    'data'=>$callflowJson,
                    'status'=>'success',
                    'request_id'=> uniqid(),
                    'revision'=> '{REVISION}',
                    'status_code'=>200
                ]);
            }
        }
    } 
}

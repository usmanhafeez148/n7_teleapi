<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

use Auth;
use Illuminate\Http\Request;
use App\Models\Media;
use App\Models\Domain;
use Webpatser\Uuid\Uuid;
use DB;

class MediaController extends Controller
{
   
    public function __construct(){
        $this->storage=Redis::connection();
    }



    public function index($accountId)
    {
        $medias= Cache::rememberForever($accountId . '_media',function() use($accountId) {
            $medias = Media::where('domain_uuid',$accountId)->get();
           
            $i=0; 
            $jsonMedias = array();
            
            foreach($medias as $media) {
                $jsonMedias[$i]=[
                'id'=> $media->media_uuid,
                'name'=>$media->name,
                'media_source'=>$media->media_source,
                'language'=>"en-us",
                'is_prompt'=>true,
                ];
                $i++;
            }

            return $jsonMedias;
        });

        
        return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>$medias,
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);
    }

    
    public function show($accountId,$mediaId)
    {
        try {
            $media= Cache::rememberForever($accountId . '_media_' . $mediaId,function() use($accountId,$mediaId) {
                
                $media = Media::where('media_uuid',$mediaId)->where('domain_uuid',$accountId)->first();

                $jsonMedia['id']= $media->media_uuid;
                $jsonMedia['name']=$media->name;
                $jsonMedia['media_source']=$media->media_source;
                $jsonMedia['language']= "en-us";
                $jsonMedia['auth_token']=(string)Auth::getToken();
                $jsonMedia['streamable']= true;
                $jsonMedia['ui_metadata']=[
                    'origin'=>'callflows',
                    'ui'=>'monster-ui',
                    'version'=>'4.1-49'
                ];
                
                if($media->media_source=="tts"){
                    $jsonMedia['tts']=[
                            'voice'=>$media->voice,
                            'text'=>$media->tts_script
                        ];

                    $jsonMedia['description']=$media->tts_script;
                     
                }else{
                    $jsonMedia['description']=$media->path;
                }

                return $jsonMedia;
            });
            return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>$media,
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);

        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        }
        
    }

  
    public function store(Request $request,$accountId)
    {
        try {   

            // return response()->json($request->data);
            $media=new Media();
            $media->media_uuid=(string) Uuid::generate();
            $media->domain_uuid=$accountId;

            if(isset($request->data['name'])){
                $media->name=$request->data['name'];
            }

            if(isset($request->data['description'])){
                $media->description=$request->data['description'];
            }
            
            if(isset($request->data['streamable'])){
                 // $media->streamable=$request->data['streamable'];
            }
           
            $media->media_source=$request->data['media_source'];
            if($request->data['media_source']=='tts'){    
                // $media->number=$request->data['number'];
                $media->media_source='tts';
                $media->voice=$request->data['tts']['voice'];
                $media->tts_script=$request->data['tts']['text'];

            }elseif($request->data['media_source']=='upload'){
            
                // $media->number=$request->data['number'];#number field is placed in filed but not in json_post

                // actually path is placed in description variable
                $media->path=$request->data['description'];
                // $media->media_source='file';
            }

            $media->save();
            Cache::forget($accountId .'_media');
            

            return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>array_merge(['id'=>$media->media_uuid],$request->data),
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);
            
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        }
       
    }

    
    public function update(Request $request, $accountId,$mediaId)
    {

        try {
            $media=Media::find($mediaId);

            if(isset($request->data['name'])){
                $media->name=$request->data['name'];
            }

            if(isset($request->data['description'])){
                $media->description=$request->data['description'];
            }
            
            if(isset($request->data['streamable'])){
                 // $media->streamable=$request->data['streamable'];
            }
            
            if(isset($request->data['media_source'])){
                $media->media_source=$request->data['media_source'];
            }
            
            if($request->data['media_source']=='tts'){    
                // $media->number=$request->data['number'];
                $media->voice=$request->data['tts']['voice'];
                $media->tts_script=$request->data['tts']['text'];

            }elseif($request->data['media_source']=='upload'){
                // $media->number=$request->data['number'];
                $media->path=$request->data['description'];
            }
            
            if(isset($request->data['ui_metadata'])){
                if($request->data['ui_metadata']['version']){
                    
                }
                if($request->data['ui_metadata']['ui']){
                    
                }
                if($request->data['ui_metadata']['origin']){

                }
            }

            $media->save();
            
            Cache::forget($accountId .'_media');
            Cache::forget($accountId . '_media_' . $mediaId);

            return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>array_merge(['id'=>$media->media_uuid],$request->data),
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);
            
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        }
    }

    
    public function destroy($accountId,$mediaId)
    {
       // return response()->json(['status'=>'ok']);
        $media=Media::find($mediaId);
        if(Media::find($mediaId)->delete()){
            Cache::forget($accountId .'_media');
            Cache::forget($accountId . '_media_' . $mediaId);

            $jsonMedia['id']= $media->media_uuid;
            $jsonMedia['name']=$media->name;
            $jsonMedia['media_source']=$media->media_source;
            $jsonMedia['language']= "en-us";
            
            $jsonMedia['streamable']= true;
            
            if($media->media_source=="tts"){
                $jsonMedia['tts']=[
                        'voice'=>$media->voice,
                        'text'=>$media->tts_script
                    ];

                $jsonMedia['description']=$media->tts_script;
                 
            }else{
                $jsonMedia['description']=$media->path;
            }

            return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>$jsonMedia,
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);
        }
    }

    public function upload(Request $request,$accountId,$mediaId){
        return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>'',
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);
    }
}

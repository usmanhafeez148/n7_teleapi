<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

use Auth;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\FaxBox;
use Webpatser\Uuid\Uuid;
use DB;

class FaxBoxController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->storage=Redis::connection();
    }

    public function index($accountId)
    {
        $faxboxes= Cache::rememberForever($accountId . '_faxbox',function() use($accountId){
            $faxboxes=FaxBox::where('domain_uuid',$accountId)->get();
            
            $i=0;
            $faxboxJson = array();

            foreach($faxboxes as $faxbox) {
                $faxboxJson[$i]=[
                    'id'=> $faxbox->faxbox_uuid,
                    'attempts'=>0,
                    'name'=>$faxbox->name,
                    'caller_id'=>$faxbox->caller_id,
                    'caller_name'=>$faxbox->caller_name,
                    'displayName'=>$faxbox->name,
                    'fax_header'=>$faxbox->fax_header,
                    'fax_identity'=>$faxbox->fax_identity,
                    'owner_id'=>$faxbox->owner_uuid,
                    'retries'=>$faxbox->retries,
                    'media'=>[],
                    'notifications'=>[
                        'inbound'=>[
                            'email'=>[
                                'send_to'=>$faxbox->inbound_faxes
                            ]
                        ],
                        'outbound'=>[
                            'email'=>[
                                'send_to'=>$faxbox->outbound_faxes
                            ]
                        ]
                    ],
                    'smtp_email_address'=>[],
                    'smtp_permission_list'=>[],
                ];

                $i++;
            }
            return $faxboxJson;
        });


        return response()->json([
            'auth_token'=> (string)Auth::getToken(),
            'data'=>$faxboxes,
            'status'=>'success',
            'request_id'=> uniqid(),
            'revision'=> '{REVISION}',
            'status_code'=>200
        ]);
    }

    
    public function show($accountId,$faxboxId)
    {
        try {
            
            $faxbox= Cache::rememberForever($accountId . '_faxbox_' . $faxboxId,function() use($accountId,$faxboxId){

                $faxbox=FaxBox::where('domain_uuid',$accountId)->where('faxbox_uuid',$faxboxId)->first();

                $faxboxJson['id']= $faxbox->faxbox_uuid;
                $faxboxJson['attempts']=0;
                $faxboxJson['name']=$faxbox->name;
                $faxboxJson['caller_id']=$faxbox->caller_id;
                $faxboxJson['caller_name']=$faxbox->caller_name;
                $faxboxJson['displayName']=$faxbox->name;
                $faxboxJson['fax_header']=$faxbox->fax_header;
                $faxboxJson['fax_identity']=$faxbox->fax_identity;
                $faxboxJson['owner_id']=$faxbox->owner_uuid;
                $faxboxJson['retries']=$faxbox->retries;
                $faxboxJson['media']=[];
                $faxboxJson['notifications']=[
                    'inbound'=>[
                        'email'=>[
                            'send_to'=>5
                        ]
                    ],
                    'outbound'=>[
                        'email'=>[
                            'send_to'=>5
                        ]
                    ]
                ];
                $faxboxJson['smtp_email_address']="";
                $faxboxJson['smtp_permission_list']="";
                return $faxboxJson;
            });

            return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>$faxbox,
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);

        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        }
    }

    
    public function store(Request $request,$accountId)
    {
        try {
            $faxbox=new FaxBox();
            // update User feature also
            $faxbox->faxbox_uuid=(string) Uuid::generate();
            $faxbox->domain_uuid=$accountId;
            $faxbox->name=$request->data['name'];
            $faxbox->caller_id=$request->data['caller_id'];
            $faxbox->caller_name=$request->data['caller_name'];
            $faxbox->fax_header=$request->data['fax_header'];
            $faxbox->fax_identity=$request->data['fax_identity'];
            $faxbox->retries=$request->data['retries'];
            
            if(isset($request->data['owner_id'])){
                $faxbox->owner_uuid=$request->data['owner_id'];
                $user=User::find($faxbox->owner_uuid);
                if($user){
                    $user->features= $user->features . "faxing,";
                    $user->save();
                    Cache::forget($accountId . '_user');
                    Cache::forget($accountId . '_user_' . $user->user_uuid);
                }
            }
            /*$faxbox->inbound_faxes=$request->data['name'];
            $faxbox->outbound_faxes=$request->data['name'];*/
            
            $faxbox->save();
            Cache::forget($accountId . '_faxbox');

            return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>array_merge(['id'=>$faxbox->faxbox_uuid],$request->data),
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);
            
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        }
    }

   
    public function update(Request $request, $accountId,$faxboxId)
    {
        try {
            $faxbox=FaxBox::find($faxboxId);
            // update User feature also
            $faxbox->name=$request->data['name'];
            $faxbox->caller_id=$request->data['caller_id'];
            $faxbox->caller_name=$request->data['caller_name'];
            $faxbox->fax_header=$request->data['fax_header'];
            $faxbox->fax_identity=$request->data['fax_identity'];
            $faxbox->retries=$request->data['retries'];
            
            if(isset($request->data['owner_id'])){
                $faxbox->owner_uuid=$request->data['owner_id'];
                $user=User::find($faxbox->owner_uuid);
                if($user){
                    $user->features= $user->features . "faxing,";
                    $user->save();
                    Cache::forget($accountId . '_user');
                    Cache::forget($accountId . '_user_' . $user->user_uuid);
                }
            }
            
            /*$faxbox->inbound_faxes=$request->data['name'];
            $faxbox->outbound_faxes=$request->data['name'];*/

            $faxbox->save();
            
            Cache::forget($accountId . '_faxbox_'. $faxboxId);
            Cache::forget($accountId . '_faxbox');

            return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>array_merge(['id'=>$faxbox->faxbox_uuid],$request->data),
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);
            
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        }
    }

    
    public function destroy($accountId,$faxboxId)
    {
        if($faxboxJson=FaxBox::find($faxboxId)){
            if(FaxBox::find($faxboxId)->delete()){
                Cache::forget($accountId . '_faxbox_'. $faxboxId);
                Cache::forget($accountId . '_faxbox');

                $faxboxJson['id']= $faxbox->faxbox_uuid;
                $faxboxJson['attempts']=0;
                $faxboxJson['name']=$faxbox->name;
                $faxboxJson['caller_id']=$faxbox->caller_id;
                $faxboxJson['caller_name']=$faxbox->caller_name;
                $faxboxJson['displayName']=$faxbox->name;
                $faxboxJson['fax_header']=$faxbox->fax_header;
                $faxboxJson['fax_identity']=$faxbox->fax_identity;
                $faxboxJson['owner_id']=$faxbox->owner_uuid;
                $faxboxJson['retries']=$faxbox->retries;
                $faxboxJson['media']=[];
                $faxboxJson['notifications']=[
                    'inbound'=>[
                        'email'=>[
                            'send_to'=>$faxbox->inbound_faxes
                        ]
                    ],
                    'outbound'=>[
                        'email'=>[
                            'send_to'=>$faxbox->outbound_faxes
                        ]
                    ]
                ];
                $faxboxJson['smtp_email_address']="";
                $faxboxJson['smtp_permission_list']="";

                return response()->json([
                    'auth_token'=> (string)Auth::getToken(),
                    'data'=>$faxboxJson,
                    'status'=>'success',
                    'request_id'=> uniqid(),
                    'revision'=> '{REVISION}',
                    'status_code'=>200
                ]);
            }
        }
    }

    public function inboundFaxes($accountId,$faxboxId){
        $faxbox=FaxBox::find($faxboxId);
        
        return response()->json([
            'auth_token'=> (string)Auth::getToken(),
            'data'=>'',
            'status'=>'success',
            'request_id'=> uniqid(),
            'revision'=> '{REVISION}',
            'status_code'=>200
        ]);
    }
    public function outboundFaxes($accountId,$faxboxId){
        $faxbox=FaxBox::find($faxboxId);
        
        return response()->json([
            'auth_token'=> (string)Auth::getToken(),
            'data'=>'',
            'status'=>'success',
            'request_id'=> uniqid(),
            'revision'=> '{REVISION}',
            'status_code'=>200
        ]);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

use Auth;
use Illuminate\Http\Request;
use App\Models\VoicemailUsers;
use App\Models\VoicemailMessage;
use App\Models\Domain;
use App\Models\Extensions;
use App\Models\User;
use Webpatser\Uuid\Uuid;
use DB;

class ResourceController extends Controller
{
    
    public function __construct(){
        $this->storage=Redis::connection();
    }
    
    // global_resources
    public function globalIndex(){

    }

    public function globalShow($resourceId){

    }

    public function globalStore(Request $request){

    }

    public function globalUpdate(Request $request,$resourceId){

    }

    public function globalDestroy($resourceId){

    }

    public function globalJobList(){

    }

    public function globalJobGet($jobId){

    }

    public function globalJobCreate(Request $request){

    }

    public function globalUpdateCollection(Request $request){

    }

    // local_resouces
    public function localIndex($accountId){

    }

    public function localShow($accountId,$resourceId){

    }

    public function localStore(Request $request,$accountId){

    }

    public function localUpdate(Request $request,$accountId,$resourceId){

    }

    public function localDestroy($accountId,$resourceId){

    }


    public function localJobList($accountId){

    }

    public function localJobGet($accountId,$jobId){

    }

    public function localJobCreate(Request $request,$accountId){

    }

    public function localUpdateCollection(Request $request,$accountId){

    }
}

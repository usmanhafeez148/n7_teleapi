<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

use Auth;
use Illuminate\Http\Request;
use App\Models\TimeConditionSet;
use App\Models\Domain;
use Webpatser\Uuid\Uuid;
use DB;

class TimeConditionSetController extends Controller
{
    
    public function __construct(){
        $this->storage=Redis::connection();
    }



    public function index($accountId)
    {
        $temporalSetRules= Cache::rememberForever($accountId . '_temporalruleset',function() use($accountId){
            $temporalSetRules = TimeConditionSet::where('domain_uuid',$accountId)->get();
            
            $temporalSetRulesJson = array();
            $i=0; 
            foreach($temporalSetRules as $temporalSetRule) {
                // $comma_separated = implode(",", $value-);
                $temporalSetRulesJson[$i]=[
                'id'=> $temporalSetRule->time_condition_set_uuid,
                'name'=>$temporalSetRule->name,
                'rules'=>json_decode($temporalSetRule->time_condition_uuid),
                ];

                $i++;
            }
            return $temporalSetRulesJson;
        });

        return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>$temporalSetRules,
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);
    }

   
    public function show($accountId,$ruleId)
    {
        try {

            $timeSet= Cache::rememberForever($accountId . '_temporalruleset_' . $ruleId,function() use($accountId,$ruleId) {
                // return response()->json([$accountId,$ruleId]);
                $timeSet = TimeConditionSet::find($ruleId);
                
                
                $jsonTime['id']=$timeSet->time_condition_set_uuid;
                $jsonTime['name']=$timeSet->name;
                // here to apply business logic
                $jsonTime['temporal_rules']=json_decode($timeSet->time_condition_uuid);
                
                return $jsonTime;
            });

            return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>$timeSet,
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);
            
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        }
        
    }

    
    public function store(Request $request,$accountId)
    {
        
        try {
            
            $timeSet=new TimeConditionSet();
            
            
                
            $timeSet->time_condition_set_uuid=(string) Uuid::generate();
            $timeSet->domain_uuid=$accountId;
            
            $timeSet->time_condition_uuid=json_encode($request->data['temporal_rules']);
            $timeSet->name=$request->data['name'];
                
            $timeSet->save();
           Cache::forget($accountId . '_temporalruleset');
            
            return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>array_merge(['id'=>$timeSet->time_condition_set_uuid],$request->data),
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);
                   
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        }
       
    }

    
    public function update(Request $request, $accountId,$ruleId)
    {
        try {
            
            // $domain=Domain::all()->first();
            $timeSet=TimeConditionSet::find($ruleId);
        
            $timeSet->name=$request->data['name'];
            $timeSet->time_condition_uuid=json_encode($request->data['temporal_rules']);
            
            $timeSet->save(); 
            Cache::forget($accountId . '_temporalruleset');
            Cache::forget($accountId . '_temporalruleset_' . $ruleId);
            
            return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>array_merge(['id'=>$timeSet->time_condition_set_uuid],$request->data),
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);
            
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        }
    }

    
    public function destroy($accountId,$ruleId)
    {
        if($timeSet=TimeConditionSet::find($ruleId)){
            if(TimeConditionSet::find($ruleId)->delete()){
                Cache::forget($accountId . '_temporalruleset');
                Cache::forget($accountId . '_temporalruleset_' . $ruleId);
                return response()->json([
                    'auth_token'=> (string)Auth::getToken(),
                    'data'=>$timeSet,
                    'status'=>'success',
                    'request_id'=> uniqid(),
                    'revision'=> '{REVISION}',
                    'status_code'=>200
                ]);
            }
        }
        
    }

}

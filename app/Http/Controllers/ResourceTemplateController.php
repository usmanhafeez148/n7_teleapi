<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

use Auth;
use Illuminate\Http\Request;
use App\Models\FollowmeExtensions;
use Webpatser\Uuid\Uuid;
use DB;

class ResourceTemplateController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->storage=Redis::connection();
    }

    public function index($accountId)
    {
        
    }

    
    public function show($accountId,$resourceId)
    {
    
    }

    
    public function store(Request $request,$accountId)
    {
       
    }

    
    public function update(Request $request, $accountId,$resourceId)
    {
       
    }

    
    public function destroy($accountId,$resourceId)
    {
        
    }
}

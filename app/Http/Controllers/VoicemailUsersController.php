<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

use Auth;
use Illuminate\Http\Request;
use App\Models\VoicemailUsers;
use App\Models\VoicemailMessage;
use App\Models\Domain;
use App\Models\Extensions;
use App\Models\User;
use Webpatser\Uuid\Uuid;
use DB;

class VoicemailUsersController extends Controller
{
    
    public function __construct(){
        $this->storage=Redis::connection();
    }
    
    
    public function index($accountId)
    {
        $voiceMails= Cache::rememberForever($accountId . '_voicemail',function() use ($accountId) {
             $voiceMails = VoicemailUsers::where('domain_uuid',$accountId)->get();
             
             $i=0; 
             $jsonVoiceMails = array();
             foreach($voiceMails as $voiceMail) {
                
                $jsonVoiceMails[$i]=[
                    'id'=> $voiceMail->vmbox_uuid,
                    'name'=>$voiceMail->name,
                    'friendlyOwnerName'=>$voiceMail->name,
                    'pin'=>$voiceMail->pin,
                    'mailbox'=>$voiceMail->mailbox,
                    'owner_id'=>$voiceMail->owner_uuid,
                    'messages'=>$voiceMail->message_count,
                ];
                $i++;

                
            }
            return $jsonVoiceMails;
        });
         
        return response()->json([
            'auth_token'=> (string)Auth::getToken(),
            'data'=>$voiceMails,
            'status'=>'success',
            'request_id'=> uniqid(),
            'revision'=> '{REVISION}',
            'status_code'=>200
        ]);
    }

    
    public function show($accountId,$voicemailId)
    {
        try {
            $voicemail= Cache::rememberForever($accountId . '_voicemail_' . $voicemailId,function() use ($accountId,$voicemailId) {
                $voicemailUser = VoicemailUsers::where('vmbox_uuid',$voicemailId)->first();
                
                // media section
                $media=[];

                $voicemailJson['id']= $voicemailUser->vmbox_uuid;
                $voicemailJson['name']=$voicemailUser->name;
                $voicemailJson['pin']=$voicemailUser->pin;
                $voicemailJson['mailbox']=$voicemailUser->mailbox;
                $voicemailJson['require_pin']= $voicemailUser->require_pin;
                $voicemailJson['is_setup']= $voicemailUser->is_setup;
             
                $voicemailJson['timezone']= "America/Los_Angeles";
                $voicemailJson['check_if_owner']= $voicemailUser->check_if_owner;
                $voicemailJson['delete_after_notify']= $voicemailUser->delete_after_notify;
                $voicemailJson['not_configurable']= $voicemailUser->not_configurable;

                $voicemailJson['notify_email_addresses']= $voicemailUser->voicemail_emailto;
                $voicemailJson['save_after_notify']= $voicemailUser->attach_voicemail;
                $voicemailJson['skip_greeting']= $voicemailUser->skip_greeting;
                $voicemailJson['skip_instructions']= $voicemailUser->skip_instructions;

                $voicemailJson['owner_id']= $voicemailUser->owner_uuid;
                $voicemailJson['media']= $media;

                return $voicemailJson;
            });

            return response()->json([
                'auth_token'=> '{AUTH_TOKEN}',
                'data'=>$voicemail,
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);

        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        }
    
    }

    
    public function store(Request $request,$accountId)
    {
        try {
            /*Set Domain_uuid from Domain Model*/
            
            $voiceMailUser=new VoicemailUsers();

            $voiceMailUser->vmbox_uuid =(string) Uuid::generate();
            
            $voiceMailUser->domain_uuid = $accountId;
            
            $voiceMailUser->name = $request->data['name'];
            
            $voiceMailUser->pin = isset($request->data['pin'])? $request->data['pin'] : null;

            $voiceMailUser->owner_uuid=isset($request->data['owner_id'])? $request->data['owner_id'] : null;
            
            $user=User::find($voiceMailUser->owner_uuid);

            if($user){
                $user->features= $user->features . "vm_to_email,";
                $user->save();
                Cache::forget($accountId . '_user');
                Cache::forget($accountId . '_user_' . $user->user_uuid);
            }
            
            $voiceMailUser->mailbox = $request->data['mailbox'];
            
            $voiceMailUser->require_pin = isset($request->data['require_pin'])? $request->data['require_pin'] : null;

            $voiceMailUser->delete_after_notify = isset($request->data['delete_after_notify'])? $request->data['delete_after_notify'] : null;

            $voiceMailUser->not_configurable = isset($request->data['not_configurable'])? $request->data['not_configurable'] : null;

            $voiceMailUser->skip_greeting = isset($request->data['skip_greeting'])? $request->data['skip_greeting'] : null;

            $voiceMailUser->skip_instructions = isset($request->data['skip_instructions'])? $request->data['skip_instructions'] : null;

            $voiceMailUser->check_if_owner = isset($request->data['check_if_owner'])? $request->data['check_if_owner'] : null;

            

            $voiceMailUser->is_setup = isset($request->data['is_setup'])? $request->data['is_setup'] : null;

            if(isset($request->data['media']['unavailable'])){
                $voiceMailUser->unavailable_media=$request->data['media']['unavailable'];
            }

            $voiceMailUser->save();

            // Cache::tags('voicemails')->flush();
            Cache::forget($accountId . '_voicemail');

            return response()->json([
                'auth_token'=> '{AUTH_TOKEN}',
                'data'=>array_merge(['id'=>$voiceMailUser->vmbox_uuid],$request->data),
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);
            
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        }
        
    }

    public function update(Request $request, $accountId,$voicemailId)
    {
        try {
            /*Set Domain_uuid from Domain Model*/
            // fields
            $voiceMailUser=VoicemailUsers::find($voicemailId);

            $voiceMailUser->name = $request->data['name'];
            $voiceMailUser->pin = $request->data['pin'];
            $voiceMailUser->mailbox = $request->data['mailbox'];
            $voiceMailUser->owner_uuid=isset($request->data['owner_id'])? $request->data['owner_id'] : null;

            $user=User::find($voiceMailUser->owner_uuid);

            if($user){
                $user->features= $user->features . "vm_to_email,";
                $user->save();
                Cache::forget($accountId . '_user');
                Cache::forget($accountId . '_user_' . $user->user_uuid);
            }
            
            $voiceMailUser->require_pin = $request->data['require_pin'];
            $voiceMailUser->delete_after_notify = $request->data['delete_after_notify'];
            $voiceMailUser->not_configurable = $request->data['not_configurable'];
            $voiceMailUser->skip_greeting = $request->data['skip_greeting'];
            $voiceMailUser->skip_instructions = $request->data['skip_instructions'];
            $voiceMailUser->check_if_owner = $request->data['check_if_owner'];
            $voiceMailUser->delete_after_notify = $request->data['delete_after_notify'];
            $voiceMailUser->is_setup = $request->data['is_setup'];
            
            if(isset($request->data['media']['unavailable'])){
                $voiceMailUser->unavailable_media=$request->data['media']['unavailable'];
            }
            
            $voiceMailUser->save();
            
            Cache::forget($accountId . '_voicemail');
            Cache::forget($accountId . '_voicemail_' . $voicemailId);

            return response()->json([
                'auth_token'=> '{AUTH_TOKEN}',
                'data'=>array_merge(['id'=>$voiceMailUser->vmbox_uuid],$request->data),
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);
            

        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        }
    }

    
    public function destroy($accountId,$voicemailId)
    {
        if($voiceMailUser=VoicemailUsers::find($voicemailId)){
            if(VoicemailUsers::find($voicemailId)->delete()){
                Cache::forget($accountId . '_voicemail');
                Cache::forget($accountId . '_voicemail_' . $voicemailId);

                return response()->json([
                    'auth_token'=> '{AUTH_TOKEN}',
                    'data'=>'',
                    'status'=>'success',
                    'request_id'=> uniqid(),
                    'revision'=> '{REVISION}',
                    'status_code'=>200
                ]);

            }
        }
        
    }
}

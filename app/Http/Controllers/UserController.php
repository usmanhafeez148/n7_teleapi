<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

use Auth;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Domain;
use App\Models\CallForward;
use App\Models\AppStore;
use App\Models\BlackListApp;

use App\Models\CallRecord;
use App\Models\CallRestriction;
use App\Models\DialPlan;
use App\Models\UiHelp;
use App\Models\HotDesk;
use Webpatser\Uuid\Uuid;
use App\Models\Directory;
use DB;

class UserController extends Controller
{
    
    public function __construct(){
        $this->storage=Redis::connection();
    }

    
    public function index($accountId)
    {
        $users= Cache::rememberForever( $accountId . '_user',function() use ($accountId) {
            $users=DB::select("SELECT * , n7c_ca_users.user_uuid FROM n7c_ca_users 
                LEFT JOIN n7c_ca_user_call_recording 
                ON n7c_ca_users.user_uuid = n7c_ca_user_call_recording.user_uuid
                LEFT JOIN n7c_ca_user_hot_desking 
                ON n7c_ca_users.user_uuid = n7c_ca_user_hot_desking.user_uuid
                LEFT JOIN n7c_ca_user_ui_help 
                ON n7c_ca_users.user_uuid = n7c_ca_user_ui_help.user_uuid
                ");
                /*LEFT JOIN n7c_ca_user_call_forwarding 
                ON n7c_ca_users.user_uuid = n7c_ca_user_call_forwarding.user_uuid*/
            
            $usersJson = array();
            $i=0;
            
            foreach($users as $user) {
                $features=explode(',', $user->features);
                
                $call_recording=[
                    'inbound'=>[
                        'offnet'=>[
                            'enabled'=>$user->inbound_internal,
                            'format'=>$user->format,
                            'time_limit'=>$user->time_limit,
                            'url'=>$user->url
                        ],
                        'onnet'=>[
                            'enabled'=>$user->inbound_external,
                            'format'=>$user->format,
                            'time_limit'=>$user->time_limit,
                            'url'=>$user->url
                        ]
                    ],
                    'outbound'=>[
                        'offnet'=>[
                            'enabled'=>$user->outbound_internal,
                            'format'=>$user->format,
                            'time_limit'=>$user->time_limit,
                            'url'=>$user->url
                        ],
                        'onnet'=>[
                            'enabled'=>$user->outbound_external,
                            'format'=>$user->format,
                            'time_limit'=>$user->time_limit,
                            'url'=>$user->url
                        ]
                    ]
                ];

                $usersJson[$i]=[
                    'id'=> $user->user_uuid,
                    'username'=>$user->username,
                    'first_name'=>$user->first_name,
                    'last_name'=>$user->last_name,
                    'priv_level'=>$user->priv_level,
                    'presence_id'=>$user->presence_id,
                    'username'=>$user->username,
                    'email'=>$user->email,
                    'displayName'=>$user->first_name . ' ' . $user->last_name,
                    // 'send_email_on_creation'=>"send_email_on_creation",
                    'require_password_update'=>$user->require_password_update,

                    'features'=>$features,
                    'call_recording'=>$call_recording,
                    
                    'language'=>$user->language,
                    'timezone'=>$user->timezone,
                   
                ];


                $i++;
            }
            return $usersJson;
        });

        return response()->json([
            'auth_token'=> (string)Auth::getToken(),
            'data'=>$users,
            'status'=>'success',
            'request_id'=> uniqid(),
            'revision'=> '{REVISION}',
            'status_code'=>200
        ]);
    }
   
    public function show($accountId,$userId)
    {
        $user=Cache::rememberForever( $accountId . '_user_' . $userId,function() use ($accountId,$userId) {
            
            $users=DB::select("SELECT *, n7c_ca_users.user_uuid FROM n7c_ca_users 
                LEFT JOIN n7c_ca_user_call_forwarding 
                ON n7c_ca_users.user_uuid = n7c_ca_user_call_forwarding.user_uuid
                LEFT JOIN n7c_ca_user_call_recording 
                ON n7c_ca_users.user_uuid = n7c_ca_user_call_recording.user_uuid
                LEFT JOIN n7c_ca_user_hot_desking 
                ON n7c_ca_users.user_uuid = n7c_ca_user_hot_desking.user_uuid
                LEFT JOIN n7c_ca_user_ui_help 
                ON n7c_ca_users.user_uuid = n7c_ca_user_ui_help.user_uuid
                WHERE n7c_ca_users.user_uuid='$userId' LIMIT 1
                ");
             
            $user=array_first($users);
            $jsonUser=array();

            if(!empty($user)){

                // Call Restriction
                $call_restriction=[];
                $restriction=CallRestriction::find($user->domain_uuid);
                if($restriction){
                    $call_restriction=[
                            'caribbean'=>[
                                'action'=> $restriction->caribbean
                            ],
                            'did_us'=>[
                                'action'=> $restriction->uk_did
                            ],
                            'emergency'=>[
                                'action'=> $restriction->emergency_dispatcher
                            ],
                            'international'=>[
                                'action'=> $restriction->international
                            ],
                            'toll_us'=>[
                                'action'=> $restriction->uk_toll
                            ],
                            'tollfree_us'=>[
                                'action'=> $restriction->uk_toll_free
                            ],
                            'unknown'=>[
                                'action'=> $restriction->unknown
                            ],
                        ];
                }
                
                // call_recording
                $call_recording=[
                    'inbound'=>[
                        'offnet'=>[
                            'enabled'=>$user->inbound_internal,
                            'format'=>$user->format,
                            'time_limit'=>$user->time_limit,
                            'url'=>$user->url
                        ],
                        'onnet'=>[
                            'enabled'=>$user->inbound_external,
                            'format'=>$user->format,
                            'time_limit'=>$user->time_limit,
                            'url'=>$user->url
                        ]
                    ],
                    'outbound'=>[
                        'offnet'=>[
                            'enabled'=>$user->outbound_internal,
                            'format'=>$user->format,
                            'time_limit'=>$user->time_limit,
                            'url'=>$user->url
                        ],
                        'onnet'=>[
                            'enabled'=>$user->outbound_external,
                            'format'=>$user->format,
                            'time_limit'=>$user->time_limit,
                            'url'=>$user->url
                        ]
                    ]
                ];
                
                // call_forward from user_call_forwarding relational table;
                $call_forward=[
                    'direct_calls_only'=>$user->direct_calls_only,
                    'enabled'=>"enabled",
                    'failover'=>"failover",
                    'ignore_early_media'=>$user->ignore_early_media,
                    'keep_caller_id'=>$user->keep_caller_id,
                    'require_keypress'=>$user->require_keypress,
                    'substitute'=>"substitute",
                ];

                // appList
                $apps=BlackListApp::where('user_uuid',$user->user_uuid)->where('domain_uuid',$accountId)->get();
                $appList=[];
                if($apps){
                    $a=0;
                    foreach ($apps as $app) {
                        $appList[$a]=$app->appstore_uuid;
                        $a++;
                    }
                }
                
                // caller_id
                $caller_id=[
                    'internal'=>[
                        'name'=>'first_name',
                        'number'=>'external_number'
                    ],
                    'external'=>[
                        'name'=>'first_name',
                        'number'=>'external_number'
                    ]
                ];
                
                // contact list
                $contact_list=[
                    'exclude'=>$user->contact_list
                ];
                
                // dial_plans
                $dial_plan=[
                ];
                
                // User Hotdesking
                $hotdesk=[
                    'enabled'=>false,
                    'id'=>$user->id,
                    'language'=>$user->language,
                    'last_name'=>$user->last_name
                ];
                
                // User Medias and Music On Hold
                $media=[
                    'audio'=>[
                        'codecs'=>[]
                    ],
                    'encryption'=>[
                        'enforce_security'=>'enforce_security',
                        'methods'=>'methods',
                    ],
                    'video'=>[
                        'codecs'=>[]
                    ],
                ];

                // music on hold
                $music_on_hold=[];

                // profiles
                $profile=[];
                
                // directories
                $directories=[];
                $dirs=Directory::where('domain_uuid',$accountId)->get();
                if($dirs){
                    $k=0;
                    foreach ($dirs as $dir) {
                        $directories[$k]=[
                            $dir->directory_uuid=>"callflow_id",       
                        ];
                        $k++;
                    }
                    
                }
                
                // smartpbx
                $smartpbx=[];

                // ringtones
                $ringtones=[];
                
                // Ui_help
                $ui_help=[];

                $jsonUser['id']= $user->user_uuid;
                $jsonUser['username']=$user->username;
                $jsonUser['first_name']=$user->first_name;
                $jsonUser['last_name']=$user->last_name;
                $jsonUser['priv_level']=$user->priv_level;
                $jsonUser['presence_id']=$user->presence_id;
                $jsonUser['username']=$user->username;
                $jsonUser['email']=$user->email;
                $jsonUser['displayName']=$user->first_name . ' ' . $user->last_name;
                $jsonUser['send_email_on_creation']=false;
                $jsonUser['require_password_update']=$user->require_password_update;
                $jsonUser['appList']=$appList;
                $jsonUser['features']=explode(',', $user->features);
                $jsonUser['call_recording']=$call_recording;
                $jsonUser['call_forward']=$call_forward;
                $jsonUser['call_restriction']=$call_restriction;
                $jsonUser['caller_id']=$caller_id;
                $jsonUser['contact_list']=$contact_list;
                $jsonUser['dial_plan']=$dial_plan;
                $jsonUser['fax_to_email_enabled']=true;
                $jsonUser['language']=$user->language;
                $jsonUser['timezone']=$user->timezone;
                $jsonUser['vm_to_email_enabled']=$user->vm_to_email_enabled;
                $jsonUser['profile']=$profile;
                $jsonUser['verified']=$user->enabled;
                $jsonUser['enabled']= true;
                $jsonUser['ui_help']=$ui_help;
                $jsonUser['directories']=$directories;
            }

            return $jsonUser;
        });
        return response()->json([
            'auth_token'=> (string)Auth::getToken(),
            'data'=>$user,
            'status'=>'success',
            'request_id'=> uniqid(),
            'revision'=> '{REVISION}',
            'status_code'=>200
        ]);
    }

    
    public function store(Request $request,$accountId)
    {
        try {        
            $data=$request->data;
            
            // creating a user
            $user=new User();
            $user->user_uuid=(string) Uuid::generate();
            $user->domain_uuid=$accountId;
            
            // accountName
            if(isset($data['accout_name'])){
                $user->accout_name=$data['accout_name'];
            }
            
            // Apps
            if(isset($data['apps'])){

            }

            // AppLists
            if(isset($data['appList'])){

            }

            // callforward
            if(isset($data['call_forward'])){
                
            }

            // callrecord
            if(isset($data['call_recording'])){
                $record=$data['call_recording'];
                
                $call_record = CallRecord::find($user->user_uuid);
                if(!$call_record){
                    $call_record = new CallRecord();
                }

                $call_record->save();
                $user->features= $user->features . "call_recording,";
            }

            // call restriction
            if(isset($data['call_restriction'])){
                $restriction=$data['call_restriction'];

                $call_restriction = CallRestriction::find($user->user_uuid);
                
                if(!$call_restriction){
                    $call_restriction = new CallRecord();
                }

                $call_restriction->save();
                $user->features= $user->features . "call_restriction,";
            }

            // caller_id
            if(isset($data['caller_id'])){
                $caller_id=$data['caller_id'];
                $user->features= $user->features . "caller_id,";
            }

            // contact list
            if(isset($data['contact_list'])){
                $contact_list=$data['contact_list'];
                // contact_list: {exclude: 0}
            }

            // Dial Plan
            if(isset($data['dial_plan'])){

            }

            // directory
            if(isset($data['directories'])){
                
            }

            // hotdesking
            if(isset($data['hotdesk'])){
                $desk=$data['hotdesk'];
                
                $hotdest = HotDesk::find($user->user_uuid);
                if(!$hotdest){
                    $hotdest = new HotDesk();
                }

                $hotdest->id=$desk['id'];
                
                if(isset($desk['keep_logged_in_elsewhere'])){
                    $hotdest->keep_logged_in_elsewhere=$desk['keep_logged_in_elsewhere'];
                }

                if(isset($desk['pin'])){
                     $hotdest->pin=$desk['pin'];
                }

                if(isset($desk['require_pin'])){
                    $hotdest->require_pin=$desk['require_pin'];
                }
                
                $user->features= $user->features . "hotdesk,";
                
                $hotdest->save();
            }

            // Media
            if(isset($data['media'])){

            }

            // Music On Hold
            if(isset($data['music_on_hold'])){
                $music_on_hold=$data['music_on_hold'];

                $user->features= $user->features . "music_on_hold,";
            }

            // profile
            if(isset($data['profile'])){
                // $user->profile= $data['profile'];
            }

            // ringtones
            if(isset($data['ringtones'])){

            }

            // smartpbx
            if(isset($data['smartpbx'])){
                /*call_recording:{enabled: true}
                conferencing:{enabled: false}
                faxing:{enabled: false}
                find_me_follow_me:{enabled: false}*/
                
                $user->features= $user->features . "smartpbx,";

                // when to update find_me_follow_me feature for user
                // $user->features=$user->features . "find_me_follow_me";
            }

            // ui_flags
            if(isset($data['ui_flags'])){

            }

            // ui_help
            if(isset($data['ui_help'])){

            }
            
            // ui_metadata
            if(isset($data['ui_metadata'])){

            }


            $user->username=$data['username'];
            $user->first_name=$data['first_name'];
            $user->last_name=$data['last_name'];
            $user->email=$data['email'];
            
            if(isset($data['verified'])){
                // $user->verified= $data['verified'];
            }
            
            if(isset($data['enabled'])){
                $user->enabled=$data['enabled'];
            }

            if(isset($data['presence_id'])){
                $user->presence_id=$data['presence_id'];
            }
            
            if(isset($data['username']) and isset($data['password'])){
                $user->password=md5($data['username'] . ':' . $data['password']);
            }
            if(isset($data['priv_level'])){
                $user->priv_level=$data['priv_level'];
            }
            if(isset($data['timezone'])){
                $user->timezone=$data['timezone'];
            }

            if(isset($data['language'])){
                $user->language=$data['language'];
            }

            if(isset($data['send_email_on_creation'])){

            }
            
            if(isset($data['vm_to_email_enabled'])){
                $user->vm_to_email_enabled= $data['vm_to_email_enabled'];
            }
            
            if(isset($data['require_password_update'])){
                $user->require_password_update=$data['require_password_update'];
            }
            
            $user->save();
            Cache::forget($accountId . '_user');
            
            return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>array_merge(['id'=>$user->user_uuid],$request->data),
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);

        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        }
    }

    
    public function update(Request $request, $accountId ,$userId)
    {   
        // return response()->json($request);
        try {
                
            $data=$request->data;
            $user=User::find($userId);

            // accountName
            if(isset($data['accout_name'])){
                $user->accout_name=$data['accout_name'];
            }
            
            // Apps
            if(isset($data['apps'])){

            }

            // AppLists
            if(isset($data['appList'])){

            }

            // callforward
            if(isset($data['call_forward'])){
                
            }

            // callrecord
            if(isset($data['call_recording'])){
                $record=$data['call_recording'];
                
                $call_record = CallRecord::find($user->user_uuid);
                if(!$call_record){
                    $call_record = new CallRecord();
                }

                $call_record->save();
                $user->features= $user->features . "call_recording,";
            }

            // call restriction
            if(isset($data['call_restriction'])){
                $restriction=$data['call_restriction'];

                $call_restriction = CallRestriction::find($user->user_uuid);
                
                if(!$call_restriction){
                    $call_restriction = new CallRecord();
                }

                $call_restriction->save();
                $user->features= $user->features . "call_restriction,";
            }

            // caller_id
            if(isset($data['caller_id'])){
                $caller_id=$data['caller_id'];
                $user->features= $user->features . "caller_id,";
            }

            // contact list
            if(isset($data['contact_list'])){
                $contact_list=$data['contact_list'];
                // contact_list: {exclude: 0}
            }

            // Dial Plan
            if(isset($data['dial_plan'])){

            }

            // directory
            if(isset($data['directories'])){

            }

            // hotdesking
            if(isset($data['hotdesk'])){
                $desk=$data['hotdesk'];
                
                $hotdest = HotDesk::find($user->user_uuid);
                if(!$hotdest){
                    $hotdest = new HotDesk();
                }

                $hotdest->id=$desk['id'];
                
                if(isset($desk['keep_logged_in_elsewhere'])){
                    $hotdest->keep_logged_in_elsewhere=$desk['keep_logged_in_elsewhere'];
                }

                if(isset($desk['pin'])){
                     $hotdest->pin=$desk['pin'];
                }

                if(isset($desk['require_pin'])){
                    $hotdest->require_pin=$desk['require_pin'];
                }
                
                $user->features= $user->features . "hotdesk,";
                
                $hotdest->save();
            }

            // Media
            if(isset($data['media'])){

            }

            // Music On Hold
            if(isset($data['music_on_hold'])){
                $music_on_hold=$data['music_on_hold'];

                $user->features= $user->features . "music_on_hold,";
            }

            // profile
            if(isset($data['profile'])){
                // $user->profile= $data['profile'];
            }

            // ringtones
            if(isset($data['ringtones'])){

            }

            // smartpbx
            if(isset($data['smartpbx'])){
                /*call_recording:{enabled: true}
                conferencing:{enabled: false}
                faxing:{enabled: false}
                find_me_follow_me:{enabled: false}*/
                
                $user->features= $user->features . "smartpbx,";

                // when to update find_me_follow_me feature for user
                // $user->features=$user->features . "find_me_follow_me";
            }

            // ui_flags
            if(isset($data['ui_flags'])){

            }

            // ui_help
            if(isset($data['ui_help'])){

            }
            
            // ui_metadata
            if(isset($data['ui_metadata'])){

            }


            $user->username=$data['username'];
            $user->first_name=$data['first_name'];
            $user->last_name=$data['last_name'];
            $user->email=$data['email'];
            
            if(isset($data['verified'])){
                // $user->verified= $data['verified'];
            }
            
            if(isset($data['enabled'])){
                $user->enabled=$data['enabled'];
            }

            if(isset($data['presence_id'])){
                $user->presence_id=$data['presence_id'];
            }
            
            if(isset($data['username']) and isset($data['password'])){
                $user->passwrod=md5($data['username'] . ':' . $data['password']);
            }
            if(isset($data['priv_level'])){
                $user->priv_level=$data['priv_level'];
            }
            if(isset($data['timezone'])){
                $user->timezone=$data['timezone'];
            }

            if(isset($data['language'])){
                $user->language=$data['language'];
            }

            if(isset($data['send_email_on_creation'])){

            }
            
            if(isset($data['vm_to_email_enabled'])){
                $user->vm_to_email_enabled= $data['vm_to_email_enabled'];
            }
            
            if(isset($data['require_password_update'])){
                $user->require_password_update=$data['require_password_update'];
            }
            
            $user->save();

            Cache::forget($accountId . '_user');
            Cache::forget($accountId . '_user_' . $userId);
            return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>array_merge(['id'=>$user->user_uuid],$request->data),
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);
            
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        }
    }

    
    public function destroy(Request $request,$accountId,$userId)
    {
    
        if($user=User::find($userId)){
            if(User::find($userId)->delete()){
                Cache::forget($accountId . '_user');
                Cache::forget($accountId . '_user_' . $userId);
                
                return response()->json([
                    'auth_token'=> (string)Auth::getToken(),
                    'data'=>['id'=>$user->user_uuid],
                    'status'=>'success',
                    'request_id'=> uniqid(),
                    'revision'=> '{REVISION}',
                    'status_code'=>200
                ]);
            }
        }
        
    }
}

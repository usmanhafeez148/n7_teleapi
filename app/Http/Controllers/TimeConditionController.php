<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

use Auth;
use Illuminate\Http\Request;
use App\Models\TimeCondition;
use App\Models\Domain;
use Webpatser\Uuid\Uuid;
use DB;

class TimeConditionController extends Controller
{
    

    public function __construct(){
        $this->storage=Redis::connection();
    }

    public function index($accountId)
    {
        $temporalRules= Cache::rememberForever($accountId . '_temporalrule',function() use($accountId) {
            $temporalRules = TimeCondition::where('domain_uuid',$accountId)->get();
            
            $i=0; 
            $temporalJson = array();
            
            foreach($temporalRules as $temporalRule) {
                
                $temporalJson[$i]=[
                    'id'=> $temporalRule->time_condition_uuid,
                    'name'=>$temporalRule->name,
                    'type'=>$temporalRule->ordinal,
                    'message'=>'message'
                ];
                
                $i++;
            }
            return $temporalJson;
        });
        
        return response()->json([
            'auth_token'=> (string)Auth::getToken(),
            'data'=>$temporalRules,
            'status'=>'success',
            'request_id'=> uniqid(),
            'revision'=> '{REVISION}',
            'status_code'=>200
        ]);
    }

    
    public function show($accountId,$ruleId)
    {
        
        try {
            $temporalRule= Cache::rememberForever($accountId . '_temporalrule_' .$ruleId,function() use($accountId,$ruleId) {
                $temporalRule = TimeCondition::find($ruleId);
                
                $jsonTime['id']=$temporalRule->time_condition_uuid;
                $jsonTime['interval']=$temporalRule->interval;
                $jsonTime['name']=$temporalRule->name;
                $jsonTime['cycle']=$temporalRule->cycle;
                /*$jsonTime['showDelete']=$time->;
                $jsonTime['showSave']=$time->;*/
                $jsonTime['start_date']=$temporalRule->start_date;
                $jsonTime['time_window_start']=$temporalRule->time_window_start;
                $jsonTime['message']='message';
                $jsonTime['time_window_stop']=$temporalRule->time_window_stop;
                // $jsonTime['type']=$ordinal;
                $jsonTime['type']="main_weekdays";
                $jsonTime['wdays']=explode(',', $temporalRule-> wdays);
                
                return $jsonTime;
            });

            return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>$temporalRule,
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);

        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        }
        
    }

   
    public function store(Request $request,$accountId)
    {
        try {
            
            $time=new TimeCondition();
            $time->time_condition_uuid=(string) Uuid::generate();
            $time->domain_uuid=$accountId;
            
            if(isset($request->data['cycle'])){
                $time->cycle=$request->data['cycle'];
            }
            
            if(isset($request->data['interval'])){
                $time->interval=$request->data['interval'];
            }
            
            if(isset($request->data['name'])){
                $time->name=$request->data['name'];
            }
            
            // $time->=$request->data['showSave'];
            
            if(isset($request->data['start_date'])){
                $time->start_date=$request->data['start_date'];
            }
            
            if(isset($request->data['time_window_start'])){
                $time->time_window_start=$request->data['time_window_start'];
            }
            
            if(isset($request->data['time_window_stop'])){
                $time->time_window_stop=$request->data['time_window_stop'];
            }
            
            if(isset($request->data['wdays'])){
                $wdays="";
                foreach ($request->data['wdays'] as $day) {
                    $wdays=$wdays . $day . ",";
                }
                $time->wdays=$wdays;
                $time->days=count($request->data['wdays']);
            }
            
            $time->save();
            Cache::forget($accountId . '_temporalrule');
            
            return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>array_merge(['id'=>$time->time_condition_uuid],$request->data),
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);
            
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        }
        
    }

   
    public function update(Request $request, $accountId,$ruleId)
    {
        try {
            
            $time=TimeCondition::find($ruleId);
            
            if(isset($request->data['cycle'])){
                $time->cycle=$request->data['cycle'];
            }
            
            if(isset($request->data['interval'])){
                $time->interval=$request->data['interval'];
            }
            
            if(isset($request->data['name'])){
                $time->name=$request->data['name'];
            }
            
            // $time->=$request->data['showSave'];
            
            if(isset($request->data['start_date'])){
                $time->start_date=$request->data['start_date'];
            }
            
            if(isset($request->data['time_window_start'])){
                $time->time_window_start=$request->data['time_window_start'];
            }
            
            if(isset($request->data['time_window_stop'])){
                $time->time_window_stop=$request->data['time_window_stop'];
            }
            
            if(isset($request->data['wdays'])){
                $wdays="";
                foreach ($request->data['wdays'] as $day) {
                    $wdays=$wdays . $day . ",";
                }
                $time->wdays=$wdays;
                $time->days=count($request->data['wdays']);
            }

            $time->save();

            Cache::forget($accountId . '_temporalrule');
            Cache::forget($accountId. '_temporalrule_'. $ruleId);

            return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>array_merge(['id'=>$time->time_condition_uuid],$request->data),
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);
            
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        }
    }

    
    public function destroy($accountId,$ruleId)
    {
        if($time=TimeCondition::find($ruleId)){
            if(TimeCondition::find($ruleId)->delete()){
                Cache::forget($accountId . '_temporalrule');
                Cache::forget($accountId. '_temporalrule_'. $ruleId);

                return response()->json([
                    'auth_token'=> (string)Auth::getToken(),
                    'data'=>'',
                    'status'=>'success',
                    'request_id'=> uniqid(),
                    'revision'=> '{REVISION}',
                    'status_code'=>200
                ]);
            }
        }
        
    }

}
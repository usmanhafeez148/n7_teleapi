<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

use Tymon\JWTAuth\Facades\JWTAuth;
// use Auth;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use DB;

use App\Models\Account;
use App\Models\AppStore;
// use App\Models\BlackList;
// use App\Models\CacheTrace;
// use App\Models\CallFlow;
use App\Models\CallForward;
use App\Models\CallRecord;
use App\Models\CallRestriction;
// use App\Models\Conference;
use App\Models\CreditManagement;
// use App\Models\Device;
// use App\Models\DialPlan;
// use App\Models\Directory;
// use App\Models\Domain;
// use App\Models\FaxBox;
// use App\Models\Gateway;
// use App\Models\Group;
// use App\Models\HotDesk;
// use App\Models\Limit;
use App\Models\Media;
// use App\Models\Menu;
// use App\Models\MusicOnHold;
// use App\Models\Number;
// use App\Models\Package;
// use App\Models\PacakgeDestination;
// use App\Models\PacakgeHistory;
// use App\Models\PacakgePolicy;
// use App\Models\PacakgeSubscription;
// use App\Models\ServicePlan;
// use App\Models\TimeCondition;
// use App\Models\TimeConditionSet;
// use App\Models\TrafficRule;
use App\Models\UiHelp;
use App\Models\UiRestriction;
// use App\Models\User;
// use App\Models\VoicemailUsers;


class AccountController extends Controller
{
    
    public function __construct()
    {
        $this->storage=Redis::connection();
    }

    public function index()
    {
        
        $accounts = Cache::rememberForever('account',function() {
            /*return Post::all();*/
            $accounts=Account::all();
            $jsonAccounts=array();
            $i=0;
            foreach ($accounts as $account) {
                $blacklistArray=explode(',', $account->blacklist);
                
                $jsonAccounts[$i]=[
                    'id' => $account->domain_uuid,
                    'name' => $account->name,
                    'realm' => $account->realm,
                    'enabled' => $account->status,
                    'descendants_count' => $account->descendants_count,
                    'is_reseller' => $account->is_reseller,
                    'reseller_id' => $account->reseller_id,
                    'superduper_admin' => $account->superduper_admin,
                    'timezone' => $account->timezone,
                    'created' => $account->created,
                    'billing_status' => $account->billing_mode,
                    'blacklists' => $blacklistArray,
                    'contact' => [
                        'technical' => json_encode($account->contact_technical),
                        'billing' => json_encode($account->contact_billing)
                    ]
                ];
                $i++;
            }

            return $jsonAccounts;
        });

        return response()->json([
            'auth_token'=> (string)JWTAuth::getToken(),
            'data'=>$accounts,
            'status'=>'success',
            'request_id'=> uniqid(),
            'revision'=> '{REVISION}',
            'status_code'=>200
        ]);
    }

    public function listChild($accountId){
        $accounts = Cache::rememberForever($accountId . '_account_child',function() use($accountId){
            $accounts=Account::where('parent_domain',$accountId)->get();

            $jsonAccounts=array();
            $i=0;
            foreach ($accounts as $account) {
                $blacklistArray=explode(',', $account->blacklist);
                
                $jsonAccounts[$i]=[
                    'id' => $account->domain_uuid,
                    'name' => $account->name,
                    'realm' => $account->realm,
                    'enabled' => $account->status,
                    'descendants_count' => $account->descendants_count,
                    'is_reseller' => $account->is_reseller,
                    'reseller_id' => $account->reseller_id,
                    'superduper_admin' => $account->superduper_admin,
                    'timezone' => $account->timezone,
                    'created' => $account->created,
                    'billing_status' => $account->billing_mode,
                    'blacklists' => $blacklistArray,
                    'contact' => [
                        'technical' => json_encode($account->contact_technical),
                        'billing' => json_encode($account->contact_billing)
                    ]
                ];
                $i++;
            }
            return $jsonAccounts;
        });

        return response()->json([
            'auth_token'=> (string)JWTAuth::getToken(),
            'data'=>$accounts,
            'status'=>'success',
            'request_id'=> uniqid(),
            'revision'=> '{REVISION}',
            'status_code'=>200
        ]);
    }

    public function listDescendants($accountId){

        $accounts = Cache::rememberForever($accountId . '_account_descendant',function() use($accountId){

            $accounts=Account::where('resellar_uuid',$accountId)->get();

            $jsonAccounts=array();
            $i=0;
            foreach ($accounts as $account) {
                $blacklistArray=explode(',', $account->blacklist);
                
                $jsonAccounts[$i]=[
                    'id' => $account->domain_uuid,
                    'name' => $account->name,
                    'realm' => $account->realm,
                    'enabled' => $account->status,
                    'descendants_count' => $account->descendants_count,
                    'is_reseller' => $account->is_reseller,
                    'reseller_id' => $account->reseller_id,
                    'superduper_admin' => $account->superduper_admin,
                    'timezone' => $account->timezone,
                    'created' => $account->created,
                    'billing_status' => $account->billing_mode,
                    'blacklists' => $blacklistArray,
                    'contact' => [
                        'technical' => json_encode($account->contact_technical),
                        'billing' => json_encode($account->contact_billing)
                    ]
                ];
                $i++;
            }
            return $jsonAccounts;
        });
        
        return response()->json([
            'auth_token'=> (string)JWTAuth::getToken(),
            'data'=>$accounts,
            'status'=>'success',
            'request_id'=> uniqid(),
            'revision'=> '{REVISION}',
            'status_code'=>200
        ]);
    }

    public function listParents($accountId){
        $accounts = Cache::rememberForever($accountId . '_account_parent',function() use($accountId){

            // here to related query
            $accounts=Account::where('parent_domain',$accountId)->get();

            $jsonAccounts=array();
            $i=0;
            foreach ($accounts as $account) {
                $blacklistArray=explode(',', $account->blacklist);
                
                $jsonAccounts[$i]=[
                    'id' => $account->domain_uuid,
                    'name' => $account->name,
                    'realm' => $account->realm,
                    'enabled' => $account->status,
                    'descendants_count' => $account->descendants_count,
                    'is_reseller' => $account->is_reseller,
                    'reseller_id' => $account->reseller_id,
                    'superduper_admin' => $account->superduper_admin,
                    'timezone' => $account->timezone,
                    'created' => $account->created,
                    'billing_status' => $account->billing_mode,
                    'blacklists' => $blacklistArray,
                    'contact' => [
                        'technical' => json_encode($account->contact_technical),
                        'billing' => json_encode($account->contact_billing)
                    ]
                ];
                $i++;
            }
            return $jsonAccounts;
        });
        
        return response()->json([
            'auth_token'=> (string)JWTAuth::getToken(),
            'data'=>$accounts,
            'status'=>'success',
            'request_id'=> uniqid(),
            'revision'=> '{REVISION}',
            'status_code'=>200
        ]);
    }

    public function show($accountId)
    {
        try {
            $account=Cache::rememberForever('account_' . $accountId,function() use($accountId){

                $account=Account::find($accountId);

                $blacklistArray=explode(',', $account->blacklist);

                $caller_id=[
                    'emergency'=>[
                        'name'=>$account->name,
                        'number'=>''
                    ],
                    'internal'=>[
                        'name'=>$account->name,
                        'number'=>$account->internal_caller_id
                    ],
                    'external'=>[
                        'name'=>$account->name,
                        'number'=>$account->external_caller_id
                    ]
                ];

                $preflow=[
                    'always'=>$account->preflow
                ];

                $dial_plan=[];

                // Music On Hold
                // $music_on_hold=$account->music_on_hold;
                $music_on_hold=[];
                $media=Media::where('domain_uuid',$accountId)->first();
                if($media){
                    $music_on_hold=[
                        'media_id'=>$media->media_uuid
                    ];
                }
                
                // CallRestriction
                $call_restriction=[];
                $restriction=CallRestriction::find($accountId);
                if($restriction){
                    $call_restriction=[
                        'caribbean'=>[
                            'action'=> $restriction->caribbean
                        ],
                        'did_us'=>[
                            'action'=> $restriction->uk_did
                        ],
                        'emergency'=>[
                            'action'=> $restriction->emergency_dispatcher
                        ],
                        'international'=>[
                            'action'=> $restriction->international
                        ],
                        'toll_us'=>[
                            'action'=> $restriction->uk_toll
                        ],
                        'tollfree_us'=>[
                            'action'=> $restriction->uk_toll_free
                        ],
                        'unknown'=>[
                            'action'=> $restriction->unknown
                        ],
                    ];
                }

                // notification and credit management
                $notifications=[];
                $credit=CreditManagement::find($accountId);
                if($credit){
                    $notifications=[
                        'low_balance'=> [
                            'last_notification'=>$credit->last_notification,
                            'sent_low_balance' =>$credit->sent_low_balance,
                            'enabled'=>$credit->low_balance_enabled,
                            'threshold'=>$credit->low_balance_threshold
                        ],
                        'first_occurrence'=>[
                            'sent_initial_registration'=>true
                        ]
                    ];
                }
                
                // ringtones
                $ringtones=[

                ];

                // contact
                $contact= [
                        'technical' => json_encode($account->contact_technical),
                        'billing' => json_encode($account->contact_billing)
                    ];

                // UserInterface Restriction
                $ui_restriction=[];
                $uiRestriction=UiRestriction::find($accountId);
                if($uiRestriction){
                    $ui_restriction=[
                        'myaccount'=>[
                            'account'=>[
                                'show_tab'=>$uiRestriction->account,
                            ],
                            'balance'=>[
                                'show_credit'=>$uiRestriction->balance_show_credit,
                                'show_header'=>$uiRestriction->balance_show_header,
                                'show_minutes'=>$uiRestriction->balance_show_minutes,
                                'show_tab'=>$uiRestriction->balance_show_tab,
                            ],
                            'billing'=>[
                                'show_tab'=>$uiRestriction->billing
                            ],
                            'errorTracker'=>[
                                'show_tab'=>$uiRestriction->errorTracker
                            ],
                            'inbound'=>[
                                'show_tab'=>$uiRestriction->inbound
                            ],
                            'outbound'=>[
                                'show_tab'=>$uiRestriction->outbound
                            ],
                            'service_plan'=>[
                                'show_tab'=>$uiRestriction->service_plan
                            ],
                            'transaction'=>[
                                'show_tab'=>$uiRestriction->transaction
                            ],
                            'twoway'=>[
                                'show_tab'=>$uiRestriction->twoway
                            ],
                            'user'=>[
                                'show_tab'=>$uiRestriction->user
                            ],
                        ]
                    ];
                }
                
                $ui_help=[];

                $jsonAccount=[
                    'id' => $account->domain_uuid,
                    'name' => $account->name,
                    'realm' => $account->realm,
                    'enabled' => $account->status,
                    'descendants_count' => $account->descendants_count,
                    'is_reseller' => $account->is_reseller,
                    'reseller_id' => $account->reseller_id,
                    'superduper_admin' => $account->superduper_admin,
                    'timezone' => $account->timezone,
                    'created' => $account->created,
                    'billing_mode' => $account->billing_mode,
                    'announcement' => $account->announcement,
                    'call_restriction' =>$call_restriction,
                    'caller_id' => $caller_id,
                    'custom_notes' => $account->custom_notes,
                    'dial_plan' => $dial_plan,
                    'music_on_hold' => $music_on_hold,
                    'preflow' => $preflow,
                    'ringtones' => $ringtones,
                    'blacklists' => $blacklistArray,
                    'contact' =>$contact,
                    'notifications'=>$notifications,
                    'ui_restrictions'=>$ui_restriction,
                    'wnm_allow_additions'=>'',
                    'ui_flags'=>$account->ui_flags,
                    'ui_help'=>$ui_help
                ];

                return $jsonAccount;
            });

            return response()->json([
                'auth_token'=> (string)JWTAuth::getToken(),
                'data'=>$account,
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);

        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        } 
    }
    

    public function store(Request $request)
    {
        try {

            $account=new Account();

            $account->domain_uuid=(string) Uuid::generate();
            if(isset($request->data['name']))
            {
                $account->name= $request->data['name'];
            }
            if(isset($request->data['timezone'])
            {
                $account->timezone=$request->data['timezone'];
            }
            if(isset($request->data['realm']))
            {
                $account->realm= $request->data['realm'];
            }

            /*$account->reseller_id=;
            $account->parent_domain=;
            
            
            $account->descendants_count=;
            $account->preflow=;
            
            $account->status=;
            $account->internal_caller_id=;
            $account->external_caller_id=;
            $account->blacklist=;
            $account->is_reseller=;
            $account->superduper_admin=;
            $account->contact_billing=;
            $account->contact_technical=;
            $account->custom_notes=;
            $account->announcement=;
            $account->ui_flags=;
            $account->created=;
            $account->last_updated=;
            $account->billing_status=;
            $account->music_on_hold=;*/

            $account->save();

            Cache::forget('account');
            
            return response()->json([
                'auth_token'=> (string)JWTAuth::getToken(),
                'data'=>['id'=>$account->domain_uuid,$request->data],
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);

        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        }   
    }

    public function storeChild(Request $request,$accountId){
        try {

            $account=new Account();

            $account->domain_uuid=(string) Uuid::generate();
            // $account->reseller_id=;
            $account->parent_domain=$accountId;
            /*$account->name=;
            $account->realm=;
            $account->descendants_count=;
            $account->preflow=;
            $account->timezone=;
            $account->status=;
            $account->internal_caller_id=;
            $account->external_caller_id=;
            $account->blacklist=;
            $account->is_reseller=;
            $account->superduper_admin=;
            $account->contact_billing=;
            $account->contact_technical=;
            $account->custom_notes=;
            $account->announcement=;
            $account->ui_flags=;
            $account->created=;
            $account->last_updated=;
            $account->billing_status=;
            $account->music_on_hold=;*/

            $account->save();

            Cache::forget('account');
            Cache::forget('account_' . $accountId);

            return response()->json([
                'auth_token'=> (string)JWTAuth::getToken(),
                'data'=>['id'=>$account->domain_uuid,$request->data],
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);

        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        }
    }
    
    public function update(Request $request, $accountId)
    {
        try {

            $account=Account::find($accountId);

            /*$account->name=;
            $account->realm=;
            $account->descendants_count=;
            $account->preflow=;
            $account->timezone=;
            $account->status=;
            $account->internal_caller_id=;
            $account->external_caller_id=;
            $account->blacklist=;
            $account->is_reseller=;
            $account->superduper_admin=;
            $account->contact_billing=;
            $account->contact_technical=;
            $account->custom_notes=;
            $account->announcement=;
            $account->ui_flags=;
            $account->created=;
            $account->last_updated=;
            $account->billing_status=;
            $account->music_on_hold=;*/

            $account->save();
            
            Cache::forget('account');
            Cache::forget('account_' . $accountId); 

            return response()->json([
                'auth_token'=> (string)JWTAuth::getToken(),
                'data'=>['id'=>$account->domain_uuid,$request->data],
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);

        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        }
    }

    public function destroy($accountId)
    {
        if($account=Account::find($accountId)){
            if(Account::find($accountId)->delete()){
                
                Cache::forget('account');
                Cache::forget('account_' . $accountId);

                $blacklistArray=explode(',', $account->blacklist);

                $caller_id=[
                    'emergency'=>[
                        'name'=>$account->name,
                        'number'=>''
                    ],
                    'internal'=>[
                        'name'=>$account->name,
                        'number'=>$account->internal_caller_id
                    ],
                    'external'=>[
                        'name'=>$account->name,
                        'number'=>$account->external_caller_id
                    ]
                ];

                $preflow=[
                    'always'=>$account->preflow
                ];

                $dial_plan=[];

                // Music On Hold
                // $music_on_hold=$account->music_on_hold;
                $music_on_hold=[];
                $media=Media::where('domain_uuid',$accountId)->first();
                if($media){
                    $music_on_hold=[
                        'media_id'=>$media->media_uuid
                    ];
                }
                
                // CallRestriction
                $call_restriction=[];
                $restriction=CallRestriction::find($accountId);
                if($restriction){
                    $call_restriction=[
                        'caribbean'=>[
                            'action'=> $restriction->caribbean
                        ],
                        'did_us'=>[
                            'action'=> $restriction->uk_did
                        ],
                        'emergency'=>[
                            'action'=> $restriction->emergency_dispatcher
                        ],
                        'international'=>[
                            'action'=> $restriction->international
                        ],
                        'toll_us'=>[
                            'action'=> $restriction->uk_toll
                        ],
                        'tollfree_us'=>[
                            'action'=> $restriction->uk_toll_free
                        ],
                        'unknown'=>[
                            'action'=> $restriction->unknown
                        ],
                    ];
                }

                // notification and credit management
                $notifications=[];
                $credit=CreditManagement::find($accountId);
                if($credit){
                    $notifications=[
                        'low_balance'=> [
                            'last_notification'=>$credit->last_notification,
                            'sent_low_balance' =>$credit->sent_low_balance,
                            'enabled'=>$credit->low_balance_enabled,
                            'threshold'=>$credit->low_balance_threshold
                        ],
                        'first_occurrence'=>[
                            'sent_initial_registration'=>true
                        ]
                    ];
                }
                
                // ringtones
                $ringtones=[

                ];

                // contact
                $contact= [
                        'technical' => json_encode($account->contact_technical),
                        'billing' => json_encode($account->contact_billing)
                    ];

                // UserInterface Restriction
                $ui_restriction=[];
                $uiRestriction=UiRestriction::find($accountId);
                if($uiRestriction){
                    $ui_restriction=[
                        'myaccount'=>[
                            'account'=>[
                                'show_tab'=>$uiRestriction->account,
                            ],
                            'balance'=>[
                                'show_credit'=>$uiRestriction->balance_show_credit,
                                'show_header'=>$uiRestriction->balance_show_header,
                                'show_minutes'=>$uiRestriction->balance_show_minutes,
                                'show_tab'=>$uiRestriction->balance_show_tab,
                            ],
                            'billing'=>[
                                'show_tab'=>$uiRestriction->billing
                            ],
                            'errorTracker'=>[
                                'show_tab'=>$uiRestriction->errorTracker
                            ],
                            'inbound'=>[
                                'show_tab'=>$uiRestriction->inbound
                            ],
                            'outbound'=>[
                                'show_tab'=>$uiRestriction->outbound
                            ],
                            'service_plan'=>[
                                'show_tab'=>$uiRestriction->service_plan
                            ],
                            'transaction'=>[
                                'show_tab'=>$uiRestriction->transaction
                            ],
                            'twoway'=>[
                                'show_tab'=>$uiRestriction->twoway
                            ],
                            'user'=>[
                                'show_tab'=>$uiRestriction->user
                            ],
                        ]
                    ];
                }
                
                $ui_help=[];

                $jsonAccount=[
                    'id' => $account->domain_uuid,
                    'name' => $account->name,
                    'realm' => $account->realm,
                    'enabled' => $account->status,
                    'descendants_count' => $account->descendants_count,
                    'is_reseller' => $account->is_reseller,
                    'reseller_id' => $account->reseller_id,
                    'superduper_admin' => $account->superduper_admin,
                    'timezone' => $account->timezone,
                    'created' => $account->created,
                    'billing_mode' => $account->billing_mode,
                    'announcement' => $account->announcement,
                    'call_restriction' =>$call_restriction,
                    'caller_id' => $caller_id,
                    'custom_notes' => $account->custom_notes,
                    'dial_plan' => $dial_plan,
                    'music_on_hold' => $music_on_hold,
                    'preflow' => $preflow,
                    'ringtones' => $ringtones,
                    'blacklists' => $blacklistArray,
                    'contact' =>$contact,
                    'notifications'=>$notifications,
                    'ui_restrictions'=>$ui_restriction,
                    'wnm_allow_additions'=>'',
                    'ui_flags'=>$account->ui_flags,
                    'ui_help'=>$ui_help
                ];
                
                return response()->json([
                    'auth_token'=> (string)JWTAuth::getToken(),
                    'data'=>$jsonAccount,
                    'status'=>'success',
                    'request_id'=> uniqid(),
                    'revision'=> '{REVISION}',
                    'status_code'=>200
                ]);
            }
        }
    }
}

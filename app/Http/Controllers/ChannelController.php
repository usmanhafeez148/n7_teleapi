<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

use Auth;
use Illuminate\Http\Request;
use App\Models\CallLog;
use Webpatser\Uuid\Uuid;
use DB;

class ChannelController extends Controller
{
    
    public function __construct(){
        $this->storage=Redis::connection();
    }

    public function index(){
        
        $channels= [
            [
                'answered'=> true,
                'authorizing_id'=> '63fbb9ac78e11f3ccb387928a423798a',
                'authorizing_type'=> 'device',
                'destination'=> 'user_zu0bf7',
                'direction'=> 'outbound',
                'other_leg'=> 'd220c187-e18edc42-bab2459d@10.26.0.91',
                'owner_id'=> '72855158432d790dfb22d03ff64c033e',
                'presence_id'=> 'user_zu0bf7@account.realm.com',
                'timestamp'=> '63573977746',
                'username'=> 'user_zu0bf7',
                'uuid'=> 'dab25c76-7479-4ed2-ba92-6b725d68e351'
            ],
            [
                'answered'=> true,
                'authorizing_id'=> '63fbb9ac78e11f3ccb387928a423798a',
                'authorizing_type'=> 'device',
                'destination'=> 'user_zu0bf7',
                'direction'=> 'outbound',
                'other_leg'=> 'd220c187-e18edc42-bab2459d@10.26.0.91',
                'owner_id'=> '72855158432d790dfb22d03ff64c033e',
                'presence_id'=> 'user_zu0bf7@account.realm.com',
                'timestamp'=> '63573977746',
                'username'=> 'user_zu0bf7',
                'uuid'=> 'dab25c76-7479-4ed2-ba92-6b725d68e351'
            ]
        ];

        return response()->json([
                    'auth_token'=> (string)Auth::getToken(),
                    'data'=>$channels,
                    'status'=>'success',
                    'request_id'=> uniqid(),
                    'revision'=> '{REVISION}',
                    'status_code'=>200
                ]);
    }

    public function listByAccount($accountId){
        $channels= [
            [
                'answered'=> true,
                'authorizing_id'=> '63fbb9ac78e11f3ccb387928a423798a',
                'authorizing_type'=> 'device',
                'destination'=> 'user_zu0bf7',
                'direction'=> 'outbound',
                'other_leg'=> 'd220c187-e18edc42-bab2459d@10.26.0.91',
                'owner_id'=> '72855158432d790dfb22d03ff64c033e',
                'presence_id'=> 'user_zu0bf7@account.realm.com',
                'timestamp'=> '63573977746',
                'username'=> 'user_zu0bf7',
                'uuid'=> 'dab25c76-7479-4ed2-ba92-6b725d68e351'
            ],
            [
                'answered'=> true,
                'authorizing_id'=> '63fbb9ac78e11f3ccb387928a423798a',
                'authorizing_type'=> 'device',
                'destination'=> 'user_zu0bf7',
                'direction'=> 'outbound',
                'other_leg'=> 'd220c187-e18edc42-bab2459d@10.26.0.91',
                'owner_id'=> '72855158432d790dfb22d03ff64c033e',
                'presence_id'=> 'user_zu0bf7@account.realm.com',
                'timestamp'=> '63573977746',
                'username'=> 'user_zu0bf7',
                'uuid'=> 'dab25c76-7479-4ed2-ba92-6b725d68e351'
            ]
        ];

        return response()->json([
                    'auth_token'=> (string)Auth::getToken(),
                    'data'=>$channels,
                    'status'=>'success',
                    'request_id'=> uniqid(),
                    'revision'=> '{REVISION}',
                    'status_code'=>200
                ]);
    }

    public function listByUser($accountId,$userId){
        $channels= [
            [
                'answered'=> true,
                'authorizing_id'=> '63fbb9ac78e11f3ccb387928a423798a',
                'authorizing_type'=> 'device',
                'destination'=> 'user_zu0bf7',
                'direction'=> 'outbound',
                'other_leg'=> 'd220c187-e18edc42-bab2459d@10.26.0.91',
                'owner_id'=> '72855158432d790dfb22d03ff64c033e',
                'presence_id'=> 'user_zu0bf7@account.realm.com',
                'timestamp'=> '63573977746',
                'username'=> 'user_zu0bf7',
                'uuid'=> 'dab25c76-7479-4ed2-ba92-6b725d68e351'
            ],
            [
                'answered'=> true,
                'authorizing_id'=> '63fbb9ac78e11f3ccb387928a423798a',
                'authorizing_type'=> 'device',
                'destination'=> 'user_zu0bf7',
                'direction'=> 'outbound',
                'other_leg'=> 'd220c187-e18edc42-bab2459d@10.26.0.91',
                'owner_id'=> '72855158432d790dfb22d03ff64c033e',
                'presence_id'=> 'user_zu0bf7@account.realm.com',
                'timestamp'=> '63573977746',
                'username'=> 'user_zu0bf7',
                'uuid'=> 'dab25c76-7479-4ed2-ba92-6b725d68e351'
            ]
        ];

        return response()->json([
                    'auth_token'=> (string)Auth::getToken(),
                    'data'=>$channels,
                    'status'=>'success',
                    'request_id'=> uniqid(),
                    'revision'=> '{REVISION}',
                    'status_code'=>200
                ]);
    }

    public function listByDevice($accountId,$deviceId){
        $channels= [
            [
                'answered'=> true,
                'authorizing_id'=> '63fbb9ac78e11f3ccb387928a423798a',
                'authorizing_type'=> 'device',
                'destination'=> 'user_zu0bf7',
                'direction'=> 'outbound',
                'other_leg'=> 'd220c187-e18edc42-bab2459d@10.26.0.91',
                'owner_id'=> '72855158432d790dfb22d03ff64c033e',
                'presence_id'=> 'user_zu0bf7@account.realm.com',
                'timestamp'=> '63573977746',
                'username'=> 'user_zu0bf7',
                'uuid'=> 'dab25c76-7479-4ed2-ba92-6b725d68e351'
            ],
            [
                'answered'=> true,
                'authorizing_id'=> '63fbb9ac78e11f3ccb387928a423798a',
                'authorizing_type'=> 'device',
                'destination'=> 'user_zu0bf7',
                'direction'=> 'outbound',
                'other_leg'=> 'd220c187-e18edc42-bab2459d@10.26.0.91',
                'owner_id'=> '72855158432d790dfb22d03ff64c033e',
                'presence_id'=> 'user_zu0bf7@account.realm.com',
                'timestamp'=> '63573977746',
                'username'=> 'user_zu0bf7',
                'uuid'=> 'dab25c76-7479-4ed2-ba92-6b725d68e351'
            ]
        ];

        return response()->json([
                    'auth_token'=> (string)Auth::getToken(),
                    'data'=>$channels,
                    'status'=>'success',
                    'request_id'=> uniqid(),
                    'revision'=> '{REVISION}',
                    'status_code'=>200
                ]);
    }

    public function show($accountId,$channelId){
        $channels= [
            'answered'=> true,
            'authorizing_id'=> '63fbb9ac78e11f3ccb387928a423798a',
            'authorizing_type'=> 'device',
            'destination'=> 'user_zu0bf7',
            'direction'=> 'outbound',
            'other_leg'=> 'd220c187-e18edc42-bab2459d@10.26.0.91',
            'owner_id'=> '72855158432d790dfb22d03ff64c033e',
            'presence_id'=> 'user_zu0bf7@account.realm.com',
            'timestamp'=> '63573977746',
            'username'=> 'user_zu0bf7',
            'uuid'=> 'dab25c76-7479-4ed2-ba92-6b725d68e351'
        
        ];

        return response()->json([
                    'auth_token'=> (string)Auth::getToken(),
                    'data'=>$channels,
                    'status'=>'success',
                    'request_id'=> uniqid(),
                    'revision'=> '{REVISION}',
                    'status_code'=>200
                ]);
    }

    public function store(Request $request,$accountId,$channelId){
        $channels= [
            'answered'=> true,
            'authorizing_id'=> '63fbb9ac78e11f3ccb387928a423798a',
            'authorizing_type'=> 'device',
            'destination'=> 'user_zu0bf7',
            'direction'=> 'outbound',
            'other_leg'=> 'd220c187-e18edc42-bab2459d@10.26.0.91',
            'owner_id'=> '72855158432d790dfb22d03ff64c033e',
            'presence_id'=> 'user_zu0bf7@account.realm.com',
            'timestamp'=> '63573977746',
            'username'=> 'user_zu0bf7',
            'uuid'=> 'dab25c76-7479-4ed2-ba92-6b725d68e351'
        
        ];

        return response()->json([
                    'auth_token'=> (string)Auth::getToken(),
                    'data'=>$channels,
                    'status'=>'success',
                    'request_id'=> uniqid(),
                    'revision'=> '{REVISION}',
                    'status_code'=>200
                ]);
    }

    public function update(Request $request, $accountId,$channelId){
        $channels= [
            'answered'=> true,
            'authorizing_id'=> '63fbb9ac78e11f3ccb387928a423798a',
            'authorizing_type'=> 'device',
            'destination'=> 'user_zu0bf7',
            'direction'=> 'outbound',
            'other_leg'=> 'd220c187-e18edc42-bab2459d@10.26.0.91',
            'owner_id'=> '72855158432d790dfb22d03ff64c033e',
            'presence_id'=> 'user_zu0bf7@account.realm.com',
            'timestamp'=> '63573977746',
            'username'=> 'user_zu0bf7',
            'uuid'=> 'dab25c76-7479-4ed2-ba92-6b725d68e351'
        
        ];

        return response()->json([
                    'auth_token'=> (string)Auth::getToken(),
                    'data'=>$channels,
                    'status'=>'success',
                    'request_id'=> uniqid(),
                    'revision'=> '{REVISION}',
                    'status_code'=>200
                ]);
    }
    
}

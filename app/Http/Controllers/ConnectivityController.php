<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

use Auth;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use DB;

use App\Models\Conference;
use App\Models\User;

class ConnectivityController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->storage=Redis::connection();
    }

    public function index($accountId){
        $connections=["a3c1a06b5a0f712a20ff17f9870f1fbf"];
        return response()->json([
                    'auth_token'=> (string)Auth::getToken(),
                    'data'=>$connections,
                    'status'=>'success',
                    'request_id'=> uniqid(),
                    'revision'=> '{REVISION}',
                    'status_code'=>200
                ]);
    }

    public function show($accountId,$connectivityId){
        
        $dids=[
            [
                'DIDs'=>[],
                'auth'=>[
                    'auth_method'=>'',
                    'ip'=>'',
                ],
                'cfg'=>[],
                'monitor'=>[
                    'monitor_enabled'=>false,
                ],
                'options'=>[
                    'caller_id'=>[],
                    'delay'=>0,
                    'e911_info'=>[],
                    'enabled'=>true,
                    'failover'=>[],
                    'force_outbound'=>false,
                    'inbound_format'=>'e164',
                    'international'=>false,
                    'media_handling'=>'bypass',
                ],
                'permissions'=>[
                    'users'=>[]
                ],
                'server_name'=>'test1',
                'server_type'=>'Asterisk',
            ],
            [
                'DIDs'=>[],
                'auth'=>[
                    'auth_method'=>'',
                    'ip'=>'',
                ],
                'cfg'=>[],
                'monitor'=>[
                    'monitor_enabled'=>false,
                ],
                'options'=>[
                    'caller_id'=>[],
                    'delay'=>0,
                    'e911_info'=>[],
                    'enabled'=>true,
                    'failover'=>[],
                    'force_outbound'=>false,
                    'inbound_format'=>'e164',
                    'international'=>false,
                    'media_handling'=>'bypass',
                ],
                'permissions'=>[
                    'users'=>[]
                ],
                'server_name'=>'test1',
                'server_type'=>'Asterisk',
            ],

        ];

        $connection=[
            'DIDs_Unassigned'=>[],
            'account'=>[
                'auth_realm'=>'master3.local',
                'credits'=>[
                    'prepay'=>'0.0'
                ],
                'inbound_trunks'=>0,
                'trunks'=>0,
            ],
            'billing_account_id'=>$accountId,
            'id'=>'a3c1a06b5a0f712a20ff17f9870f1fbf',
            'servers'=>$dids,
            'ui_metadata'=>[
                'origin'=>"pbxs",
                'ui'=>"monster-ui",
                'version'=>"4.1-49",
            ]
        ];

        return response()->json([
                    'auth_token'=> (string)Auth::getToken(),
                    'data'=>$connection,
                    'status'=>'success',
                    'request_id'=> uniqid(),
                    'revision'=> '{REVISION}',
                    'status_code'=>200
                ]);
    }

    public function store(Request $request,$accountId){

    }

    public function update(Request $request,$accountId,$connectivityId){

        return response()->json([
                    'auth_token'=> (string)Auth::getToken(),
                    'data'=>[$connectivityId,$request->data],
                    'status'=>'success',
                    'request_id'=> uniqid(),
                    'revision'=> '{REVISION}',
                    'status_code'=>200
                ]);
    }

    public function destroy($accountId,$connectivityId){
        $dids=[
            [
                'DIDs'=>[],
                'auth'=>[
                    'auth_method'=>'',
                    'ip'=>'',
                ],
                'cfg'=>[],
                'monitor'=>[
                    'monitor_enabled'=>false,
                ],
                'options'=>[
                    'caller_id'=>[],
                    'delay'=>0,
                    'e911_info'=>[],
                    'enabled'=>true,
                    'failover'=>[],
                    'force_outbound'=>false,
                    'inbound_format'=>'e164',
                    'international'=>false,
                    'media_handling'=>'bypass',
                ],
                'permissions'=>[
                    'users'=>[]
                ],
                'server_name'=>'test1',
                'server_type'=>'Asterisk',
            ],
            [
                'DIDs'=>[],
                'auth'=>[
                    'auth_method'=>'',
                    'ip'=>'',
                ],
                'cfg'=>[],
                'monitor'=>[
                    'monitor_enabled'=>false,
                ],
                'options'=>[
                    'caller_id'=>[],
                    'delay'=>0,
                    'e911_info'=>[],
                    'enabled'=>true,
                    'failover'=>[],
                    'force_outbound'=>false,
                    'inbound_format'=>'e164',
                    'international'=>false,
                    'media_handling'=>'bypass',
                ],
                'permissions'=>[
                    'users'=>[]
                ],
                'server_name'=>'test1',
                'server_type'=>'Asterisk',
            ],

        ];

        $connection=[
            'DIDs_Unassigned'=>[],
            'account'=>[
                'auth_realm'=>'master3.local',
                'credits'=>[
                    'prepay'=>0.0
                ],
                'inbound_trunks'=>0,
                'trunks'=>0,
            ],
            'billing_account_id'=>$accountId,
            'id'=>'a3c1a06b5a0f712a20ff17f9870f1fbf',
            'servers'=>$dids,
            'ui_metadata'=>[
                'origin'=>"pbxs",
                'ui'=>"monster-ui",
                'version'=>"4.1-49",
            ]
        ];

        return response()->json([
                    'auth_token'=> (string)Auth::getToken(),
                    'data'=>$connection,
                    'status'=>'success',
                    'request_id'=> uniqid(),
                    'revision'=> '{REVISION}',
                    'status_code'=>200
                ]);
    }
}

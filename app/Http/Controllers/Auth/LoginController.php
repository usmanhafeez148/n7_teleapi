<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

use Auth;
use Tymon\JWTAuth\Facades\JWTAuth;

use Webpatser\Uuid\Uuid;
use App\Models\AppStore;
use App\Models\BlacklistApp;
use DB;
use App\Models\User;
use App\Models\Account;

class LoginController extends Controller
{
    
    public function login(Request $request)
    {
        $this->validate($request,[
          'data.credentials' => 'required',
          'data.account_name' => 'required',
        ]);

        try {
            
            $credentials=$request->data['credentials'];
            $account_name=$request->data['account_name'];
            
            $user=User::where('password',$credentials)->first();
            if($user){
                // fetch account related to the user
                $account = Account::where('domain_uuid',$user->domain_uuid)->where('name',$account_name)->first();

                if($account){
                    if ($auth_token = JWTAuth::fromUser($user)) {
                          // update auth_token
                          $user->auth_token=$auth_token;
                          $user->save();

                          // fetch list of all applications against the logged user
                          $apps=AppStore::where('account_uuid',$user->domain_uuid)->get();
                        
                          $jsonApp=array();
                          $i=0; 
                          foreach ($apps as $app) {
                            $jsonApp[$i]=[
                              'id'=>$app->appstore_uuid,
                              'name'=>$app->name,
                              'api_url'=>$app->api_url,
                              'label'=>$app->label,

                            ];
                            $i++;
                          }

                          $authData=[
                            'id'=>$user->user_uuid,
                            "account_id"=> $user->domain_uuid,
                            "apps"=> $jsonApp,
                            "is_reseller"=> $account->is_reseller,
                            "language"=> $user->language,
                            "owner_id"=> $user->user_uuid,
                            "reseller_id"=> $account->resellar_uuid
                          ];
                        
                          return response()->json([
                            "auth_token"=> $auth_token,
                            "data"=>$authData,
                            "request_id"=> uniqid(),
                            "revision"=> "{REVISION}",
                            "status"=> "success"
                          ]);
                    }
                }else{
                    return response()->json([
                      'error'=>'Account not matched'
                    ]);
                }   
            }else{
              return response()->json([
                'error'=>'user not found'
              ]);
            }
          
        } catch (\Exception $e) {
            return response()->json(array(
              'error' => $e->getMessage(),
              'status' => 'failed'
            ));
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
              'error' => $e->getMessage(),
              'status' => 'failed'
            ));
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], 500);

        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
              return response()->json([
                'token_invalid'
              ], 
              500);
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
              return response()->json([
                'token_absent' => $e->getMessage()
              ], 
              500);
        }
    }

    // get '/auth/tokeninfo'
    public function getTokenInfo(){
        try{
            $authToken=Auth::getToken();
            $user=Auth::user();
            if($user){
                $account=Account::find($user->domain_uuid);
                if($account){
                    
                    $apps=AppStore::where('account_uuid',$account->domain_uuid);
                    $jsonApp=[];
                    $i=0;
                    foreach ($apps as $app) {
                      $jsonApp[$i]=[
                            "id"=> $app->appstore_uuid,
                            "name"=> $app->name,
                            "api_url"=> $app->api_url,
                            "label"=> $app->label
                        ];
                      $i++;
                    }

                    $jsonData=[
                        "account_id"=> $account->domain_uuid,
                        "owner_id"=> $user->user_uuid,
                        "method"=> "cb_user_auth",
                        "id"=> $user->user_uuid,
                        "reseller_id"=> $account->resellar_uuid,
                        "is_reseller"=> $account->is_reseller,
                        "account_name"=> $account->name,
                        "language"=> "en-us",
                        "apps"=> $jsonApp
                    ];

                    return response()->json([
                        "auth_token"=> (string)Auth::getToken(),
                        "data"=>$jsonData,
                        "request_id"=> uniqid(),
                        "revision"=> "{REVISION}",
                        "status"=> "success"
                    ]);
                }else{
                    return response()->json([
                          'error'=>'Account not matched'
                        ]);
                }
            }else{
                return response()->json([
                          'error'=>'Account not matched'
                        ]);
            }
        } catch (\Exception $e) {
            return response()->json(array(
              'error' => $e->getMessage(),
              'status' => 'failed'
            ));
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
              'error' => $e->getMessage(),
              'status' => 'failed'
            ));
        }
    }
    
    // post '/auth/tokeninfo'
    public function postTokenInfo(Request $request){
        try{
            $authToken=Auth::getToken();
            $user=Auth::user();
            if($user){
                $account=Account::find($user->domain_uuid);
                if($account){
                    
                    $apps=AppStore::where('account_uuid',$account->domain_uuid)->get();
                    $jsonApp=[];
                    $i=0;
                    foreach ($apps as $app) {
                      $jsonApp[$i]=[
                            "id"=> $app->appstore_uuid,
                            "name"=> $app->name,
                            "api_url"=> $app->api_url,
                            "label"=> $app->label
                        ];
                      $i++;
                    }

                    $jsonData=[
                        "account_id"=> $account->domain_uuid,
                        "owner_id"=> $user->user_uuid,
                        "method"=> "cb_user_auth",
                        "id"=> $user->user_uuid,
                        "reseller_id"=> $account->resellar_uuid,
                        "is_reseller"=> $account->is_reseller,
                        "account_name"=> $account->name,
                        "language"=> "en-us",
                        "apps"=> $jsonApp
                    ];

                    return response()->json([
                        "auth_token"=> (string)Auth::getToken(),
                        "data"=>$jsonData,
                        "request_id"=> uniqid(),
                        "revision"=> "{REVISION}",
                        "status"=> "success"
                    ]);
                }else{
                    return response()->json([
                          'error'=>'Account not matched'
                        ]);
                }
            }else{
                return response()->json([
                          'error'=>'Account not matched'
                        ]);
            }
        } catch (\Exception $e) {
            return response()->json(array(
              'error' => $e->getMessage(),
              'status' => 'failed'
            ));
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
              'error' => $e->getMessage(),
              'status' => 'failed'
            ));
        }
    }

    // get '/user_auth/{authToken}'
    public function fetchAuthToken($authToken){
        try{
            $authToken=Auth::getToken();
            $user=Auth::user();
            if($user){
                $account=Account::find($user->domain_uuid);
                if($account){
                    
                    $apps=AppStore::where('account_uuid',$account->domain_uuid);
                    $jsonApp=[];
                    $i=0;
                    foreach ($apps as $app) {
                      $jsonApp[$i]=[
                            "id"=> $app->appstore_uuid,
                            "name"=> $app->name,
                            "api_url"=> $app->api_url,
                            "label"=> $app->label
                        ];
                      $i++;
                    }

                    $jsonData=[
                        "account_id"=> $account->domain_uuid,
                        "owner_id"=> $user->user_uuid,
                        "method"=> "cb_user_auth",
                        "id"=> $user->user_uuid,
                        "reseller_id"=> $account->resellar_uuid,
                        "is_reseller"=> $account->is_reseller,
                        "account_name"=> $account->name,
                        "language"=> "en-us",
                        "apps"=> $jsonApp
                    ];

                    return response()->json([
                        "auth_token"=> (string)Auth::getToken(),
                        "data"=>$jsonData,
                        "request_id"=> uniqid(),
                        "revision"=> "{REVISION}",
                        "status"=> "success"
                    ]);
                }else{
                    return response()->json([
                          'error'=>'Account not matched'
                        ]);
                }
            }else{
                return response()->json([
                          'error'=>'Account not matched'
                        ]);
            }
        } catch (\Exception $e) {
            return response()->json(array(
              'error' => $e->getMessage(),
              'status' => 'failed'
            ));
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
              'error' => $e->getMessage(),
              'status' => 'failed'
            ));
        }
    }

    public function authRecovery(Request $request){
      $this->validate($request,[
          'data.account_name'=>'required',
          'data.phone_number'=>'required',
          'data.username'=>'required'
      ]);

      try{
          /*
          ui_metadata:{version: "4.1-49", ui: "monster-ui", origin: "auth"}
          ui_url:"http://kazoo.timegroup.ae:32553/"
          */
          
          $recover="Request for password reset handled, email sent to: testuser@gmail.com";

          /*{"timestamp":"2018-01-18T07:42:25",
          "version":"4.1.36",
          "node":"lzhVuLZSoQJGSYL1GdanIw"}*/
          return response()->json([
                    'auth_token'=> "undefined",
                    'data'=>$recover,
                    'status'=>'success',
                    'request_id'=> uniqid(),
                    'revision'=> '{REVISION}',
                    'status_code'=>200
                ]);
      
      } catch (\Illuminate\Database\QueryException $e) {
          return response()->json(array(
              'error' => $e->getMessage(),
              'status' => 'failed',
          ));
      } catch(\Exception $e){
          return response()->json(array(
              'error' => $e->getMessage(),
              'status' => 'failed',
          ));
      } 
    }

    public function execAuthRecovery(Request $request){
    }
}

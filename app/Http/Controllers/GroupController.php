<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

use Auth;
use Illuminate\Http\Request;
use App\Models\Group;
use App\Models\Domain;
use Webpatser\Uuid\Uuid;
use App\Models\CallFlowNumber;
use App\Models\CallFlow;
use App\Models\MusicHold;
use DB;

class GroupController extends Controller
{
    
    public function __construct(){
        $this->storage=Redis::connection();
    }



    public function index($accountId)
    {

        $groups= Cache::rememberForever($accountId . '_group',function() use ($accountId){
            $groups = Group::where('domain_uuid',$accountId)->get();
            
            $jsonGroups = array();
            $i=0; 
            foreach($groups as $group) {
                $features=explode(',', $group->features);
                $numbersJson=[];
                $endpoint=0; 
                
                $callflow=CallFlow::where('group_uuid',$group->group_uuid)->first();

                if($callflow){
                    $numbers=CallFlowNumber::where('callflow_uuid')->get();
                    $j=0;
                    $numbersJson=array();
                    foreach ($numbers as $number) {
                        $numbersJson[$j]= $number->number;
                        $j++;
                    }
                }
                
                foreach (json_decode($group->endpoints) as $value) {
                    $endpoint++;
                }
                
                $jsonGroups[$i]=[
                    'id'=> $group->group_uuid,
                    'name'=>$group->name,
                    'displayName'=>$group->name,
                    'features'=>$features,
                    'endpoints'=>$endpoint,
                    // 'number'=>$numbersJson
                ];

                $i++;
            }
            return $jsonGroups;
        });


        return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>$groups,
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);
    }


    
    public function show($accountId,$groupId)
    {

        $group= Cache::rememberForever($accountId . '_group_' . $groupId,function() use ($accountId,$groupId){
           
            $group = Group::where('group_uuid',$groupId)->where('domain_uuid',$accountId)->first();

            $features=explode(',', $group->features);
            $numbersJson=[];

            $callflow=CallFlow::where('group_uuid',$group->group_uuid)->first();

            if($callflow){
                $numbers=CallFlowNumber::where('callflow_uuid')->get();
                $j=0;
                $numbersJson=array();
                foreach ($numbers as $number) {
                    $numbersJson[$j]= $number->number;
                    $j++;
                }
            }
            
            $jsonGroup['id']= $group->group_uuid;
            $jsonGroup['name']=$group->name;
            $jsonGroup['music_on_hold']= [];
            $jsonGroup['features']=$features;
            $jsonGroup['endpoints']= json_decode($group->endpoints);
            
            return $jsonGroup;
        });
        

        return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>$group,
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);
    }

    public function store(Request $request,$accountId){

        try {

            $group=new Group();
            
            $group->group_uuid=(string) Uuid::generate();
            $group->domain_uuid=$accountId;


            $group->name=$request->data['name'];
            $endpoints=json_encode($request->data['endpoints']);
            
            if(isset($request->data['smartpbx'])){
                $smartpbx=$request->data['smartpbx'];
                if(isset($smartpbx['call_recording'])){
                    
                    if($smartpbx['call_recording']['enabled'] == true){
                        $group->features=$group->features . 'call_recording,';
                    }
                }

                if(isset($smartpbx['next_action'])){
                    if($smartpbx['next_action']['enabled'] == true){
                        $group->features=$group->features . 'next_action,';
                    }
                }

                if(isset($smartpbx['ringback'])){
                    if($smartpbx['ringback']['enabled'] == true){
                        $group->features=$group->features . 'ringback,';
                    }
                }

                if(isset($smartpbx['prepend'])){
                    if($smartpbx['prepend']['enabled'] == true){
                        $group->features=$group->features . 'prepend,';
                    }
                }
            }

            $group->endpoints=utf8_encode($endpoints);
            
                     
            $group->save();
            Cache::forget($accountId . '_group');


            // return response()->json("Usman");
            return response()->json([
                'auth_token'=> '',
                'data'=>array_merge(['id'=>$group->group_uuid],$request->data),
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '',
                'status_code'=>200
            ]);

        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        } catch (\Exception $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        }
    }


    public function update(Request $request, $accountId,$groupId)
    {
        try {
            $group=Group::find($groupId);
           
            $group->name=$request->data['name'];
            $group->endpoints=json_encode($request->data['endpoints']);

            if(isset($request->data['smartpbx'])){
                $smartpbx=$request->data['smartpbx'];
                if(isset($smartpbx['call_recording'])){
                    
                    if($smartpbx['call_recording']['enabled'] == true){
                        $group->features=$group->features . 'call_recording,';
                    }
                }

                if(isset($smartpbx['next_action'])){
                    if($smartpbx['next_action']['enabled'] == true){
                        $group->features=$group->features . 'next_action,';
                    }
                }

                if(isset($smartpbx['ringback'])){
                    if($smartpbx['ringback']['enabled'] == true){
                        $group->features=$group->features . 'ringback,';
                    }
                }

                if(isset($smartpbx['prepend'])){
                    if($smartpbx['prepend']['enabled'] == true){
                        $group->features=$group->features . 'prepend,';
                    }
                }
            }
                
            $group->save();

            Cache::forget($accountId . '_group');
            Cache::forget($accountId . '_group_' . $groupId);
            
            return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>array_merge(['id'=>$group->group_uuid],$request->data),
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);
                     
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        }
    }

    
    public function destroy($accountId,$groupId)
    {
        if($group= Group::find($groupId)){

            if(Group::find($groupId)->delete()){
                Cache::forget($accountId . '_group');
                Cache::forget($accountId . '_group_' . $groupId);

                $jsonGroup['id']= $group->group_uuid;
                $jsonGroup['name']=$group->name;
                $jsonGroup['music_on_hold']= [];
                $jsonGroup['endpoints']= json_decode($group->endpoints);

                return response()->json([
                    'auth_token'=> (string)Auth::getToken(),
                    'data'=>$jsonGroup,
                    'status'=>'success',
                    'request_id'=> uniqid(),
                    'revision'=> '{REVISION}',
                    'status_code'=>200
                ]);
            }
        }

    }

}

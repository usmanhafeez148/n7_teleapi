<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

use Auth;
use Illuminate\Http\Request;
use App\Models\Number;
use App\Models\Domain;
use Webpatser\Uuid\Uuid;
use DB;

class NumberController extends Controller
{

    public function __construct(){
        $this->storage=Redis::connection();
    }

    public function index($accountId)
    {
        $numbers= Cache::rememberForever($accountId . '_number',function() use($accountId) {
            $numbers = Number::where('domain_uuid',$accountId)->get();
            
            $numberJ=array();
            $i=0; 
            foreach($numbers as $number) {

                $features=explode(',', $number->features);
                
                $numberJ[$number->did]=[
                    'assigned_to'=>$number->domain_uuid,
                    'created'=> $number->created,
                    'prepend'=>$number->prepend,
                    'state'=>$number->state,
                    'used_by'=>$number->used_by,
                    'features'=>$features,
                    'features_allowed'=>[
                    ],
                    'features_available'=>[
                        'carrier_name','force_outbound','prepend','ringback',
                        'cnam','e911','inbound_cnam',"outbound_cnam","port"
                    ],
                    'features_denied'=>[
                    ],
                    'updated'=>$number->modified,
                ];
                $i++;
            }
            $jsonNumbers['numbers']=$numberJ;
            $jsonNumbers['casquade_quantity']=0;
            
            return $jsonNumbers;
            
            
        });

        return response()->json([
            'auth_token'=> (string)Auth::getToken(),
            'data'=>$numbers,
            'status'=>'success',
            'request_id'=> uniqid(),
            'revision'=> '{REVISION}',
            'status_code'=>200
        ]);
    }

    public function show($accountId,$number)
    {
        try {

            $number= Cache::rememberForever($accountId . '_number_' . $number,function() use($accountId,$number) {

                $number = Number::where('domain_uuid',$accountId)->where('did',$number)->first();

                // setting up the features 
                $features=explode(',', $number->features);

                $numberJson['id']=$number->did;
                $numberJson['state']=$number->state;
                $numberJson['_read_only']=[
                    "created"=> $number->created,
                    "modified"=> $number->modified,
                    "state"=> $number->state
                ];
                $numberJson['features']=$features;
                // $jsonNumber['my_own_field']="";
                return $numberJson;
            });
            return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>$number,
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);

        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        }    
    }

    public function searchNumber($accountId){
        echo "here to search number";
        // seachNumber
        // prefix={pattern}&quantity={limit}&offset={offset}
        
        //searchBlock
        //  prefix={pattern}&quantity={size}&offset={offset}&blocks={limit}
         
    }

    public function searchCity($accountId){
        echo "here to search number by city";
        // ?city={city}
    }

    public function store(Request $request,$accountId)
    {
        try {

            $number=new Number();
            
            $number->domain_uuid=$accountId;

            $number->did=$number;
            
            $number->used_by='callflow';

            $number->created=date('Y-m-d h:i:s');

            // $number->number_type=$request->data['carrier_name'];
            
            $number->save();
            
            Cache::forget($accountId . '_number');
            
            return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>'',
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);

        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        }
    }

    public function createBlock(Request $request,$accountId){
        try {

            foreach ($request->data['numbers'] as $number) {
                $num=new Number();
                
                $num->domain_uuid=$accountId;

                $number->did=$number;
            
                $number->used_by='callflow';

                $number->created=date('Y-m-d h:i:s');


                // $num->number_type=$request->data['carrier_name'];
                
                $num->save();

                Cache::forget($accountId . '_number');
                
            }

            Cache::forget($accountId . '_number');

            return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>[],
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);

        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        }
    }
   
    public function update(Request $request, $accountId,$numberId)
    {
        try {

            $number=Number::find($numberId);

            // $number->repeat=$request->data['cycle'];

            $number->save();

            Cache::forget($accountId . '_number');
            Cache::forget($accountId . '_number_' . $numberId);


        return response()->json([
            'auth_token'=> (string)Auth::getToken(),
            'data'=>[],
            'status'=>'success',
            'request_id'=> uniqid(),
            'revision'=> '{REVISION}',
            'status_code'=>200
        ]);
        
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        }
    }

    public function destroy($accountId,$number)
    {

        if($number=Number::find($number)){
            if(Number::find($number)->delete()){

                Cache::forget($accountId . '_number');
                Cache::forget($accountId . '_number_' . $number);

                $numberJson=[
                    '_read_only'=>[
                        'created'=>$number->created,
                        'modified'=>$number->updated,
                        'state'=>'deleted'
                    ],
                    'id'=>$number->did,
                    'state'=>'deleted'
                ];
                Cache::tags('number')->flush();
                
                return response()->json([
                    'auth_token'=> (string)Auth::getToken(),
                    'data'=>$numberJson,
                    'status'=>'success',
                    'request_id'=> uniqid(),
                    'revision'=> '{REVISION}',
                    'status_code'=>200
                ]);
            }
        }
        
        
    }

    public function deleteBlock(Request $request,$accountId){
        // return response()->json($request);
        try {

            foreach($request->data['numbers'] as $number) {
                if(Number::find($number)->delete()){
                     Cache::forget($accountId . '_number');
                    Cache::forget($accountId . '_number_' . $number);
                }
            }

            return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>$request->data,
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);

        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        }
    }

    public function activate(Request $request,$accountId,$number){
        
        $number=Number::where('number',$number)->first();
        $numberJson=[];
        if($number){
            $numberJson=[
                '_read_only'=> [
                    'created'=> $number->created,
                    'modified'=> $number->modified,
                    'state'=> $number->state
                ],
                'id'=> $number->did,
                'state'=> $number->state
            ];
        }
        
        return response()->json([
            'auth_token'=> (string)Auth::getToken(),
            'data'=> $numberJson,
            'request_id'=> uniqid(),
            'revision'=> '{REVISION}',
            'status'=> 'success'
        ]);
    }

    public function carrierInfo($accountId){

        $carriers=[ 
                'maximal_prefix_length'=> 3,
                'usable_carriers'=> [
                    'bandwidth2', 'bandwidth', 'inum', 'local', 'inventory', 'managed', 'mdn', 'other', 'simwood', 'telnyx', 'vitelity', 'voip_innovations'
                ],
                'usable_creation_states'=> [
                    'aging', 'available', 'in_service', 'port_in', 'reserved'
                ]
        ];
        return response()->json([
            'auth_token'=> (string)Auth::getToken(),
            'data'=>$carriers,
            'node'=> '',
            'request_id'=> uniqid(),
            'status'=> 'success',
            'timestamp'=> '2017-05-01T20:31:35',
            'version'=> '4.0.0'
        ]);
    }
    
    public function listClassifiers($accountId){
        
        $classifiers=[
            'caribbean'=> [
                'friendly_name'=> 'Caribbean',
                'pretty_print'=> 'SS(###) ### - ####',
                'regex'=> '^\\+?1((?:684|264|268|242|246|441|284|345|767|809|829|849|473|671|876|664|670|787|939|869|758|784|721|868|649|340)\\d{7})$'
            ],
            'did_us'=> [
                'friendly_name'=> 'US DID',
                'pretty_print'=> 'SS(###) ### - ####',
                'regex'=> '^\\+?1?([2-9][0-9]{2}[2-9][0-9]{6})$'
            ],
            'emergency'=> [
                'friendly_name'=> 'Emergency Dispatcher',
                'regex'=> '^(911)$'
            ],
            'international'=> [
                'friendly_name'=> 'International',
                'regex'=> '^(011\\d*)$|^(00\\d*)$'
            ],
            'toll_us'=> [
                'friendly_name'=> 'US Toll',
                'pretty_print'=> 'SS(###) ### - ####',
                'regex'=> '^\\+1(900\\d{7})$'
            ],
            'tollfree_us'=> [
                'friendly_name'=> 'US TollFree',
                'pretty_print=>'=> 'SS(###) ### - ####',
                'regex'=> '^\\+1((?:800|888|877|866|855)\\d{7})$'
            ],
            'unknown'=> [
                'friendly_name'=> 'Unknown',
                'regex'=> '^(.*)$'
            ]
        ];
        return response()->json([
            'auth_token'=> (string)Auth::getToken(),
            'data'=> $classifiers,
            'request_id'=> uniqid(),
            'status'=> 'success',
            'version'=> '4.1.36'
        ]);
        
    }

    public function identify($accountId,$numberId){
        return response()->json('identify');
        /*return response()->json([
            'auth_token'=> (string)Auth::getToken(),
            'data'=> [
                'account_id'=> '',
                'number'=> ''
            ],
            'request_id'=> uniqid(),
            'revision'=> '{REVISION}',
            'status'=> 'success'
        ]);*/
    }
}

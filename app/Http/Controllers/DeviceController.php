<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

use Auth;
use Illuminate\Http\Request;
use App\Models\Device;
use App\Models\Media;
use App\Models\Domain;
use App\Models\CallRestriction;
use App\Models\CallForward;
use Webpatser\Uuid\Uuid;
use DB;

class DeviceController extends Controller
{

    public function __construct(){
        $this->storage=Redis::connection();
    }

    public function index($accountId){
        $devices= Cache::rememberForever($accountId . '_device',function() use ($accountId){
            $devices = Device::where('domain_uuid',$accountId)->get();
            $i=0;
            $deviceJson = array();
            foreach($devices as $device) {
                $deviceJson[$i]=[
                    'id'=> $device->device_uuid,
                    'name'=>$device->device_name,
                    'username'=>$device->device_name,
                    'device_type'=>$device->device_type,
                    'enabled'=>$device->status,
                    'mac_address'=>$device->device_mac_address,
                    'owner_id'=>$device->device_owner_uuid
                // 'messages'=>$value->voicemail_disk_quota
                ];
                $i++;
            }

            return $deviceJson;
        });
        
        return response()->json([
            'auth_token'=> (string)Auth::getToken(),
            'data'=>$devices,
            'status'=>'success',
            'request_id'=> uniqid(),
            'revision'=> '{REVISION}',
            'status_code'=>200
        ]);
    }

    public function show($accountId,$deviceId){
        try {
            $device= Cache::rememberForever($accountId . '_device_' . $deviceId,function() use ($accountId,$deviceId){

                $device = Device::where('domain_uuid',$accountId)->where('device_uuid',$deviceId)->first();
                
                // call_restriction section
                // $restriction=CallRestriction::find($accountId);
                
                // $call_restriction=[];
                // if($restriction!=null or !empty($request)){
                    $call_restriction=[
                        'caribbean'=>[
                            'action'=> $device->caribbean
                        ],
                        'did_us'=>[
                            'action'=> $device->uk_did
                        ],
                        'emergency'=>[
                            'action'=> $device->emergency_dispatcher
                        ],
                        'international'=>[
                            'action'=> $device->international
                        ],
                        'toll_us'=>[
                            'action'=> $device->uk_toll
                        ],
                        'tollfree_us'=>[
                            'action'=> $device->uk_toll_free
                        ],
                        'unknown'=>[
                            'action'=> $device->unknown
                        ],
                    ];
                // }

                // media section
                /*$medias=Media::where('domain_uuid',$accountId)->get();
                $media*/
                $media=[
                    'audio'=> [
                        'codecs'=> explode(',', $device->audio_codec)
                    ],
                    'encryption'=> [
                        'enforce_security'=> false,
                        'methods'=> []
                    ],
                    'fax'=>[
                        'option'=>false
                    ],
                    'fax_option'=>false,
                    'peer_to_peer'=>[

                    ],
                    'video'=> [
                        'codecs'=>  explode(',', $device->video_codec)
                    ]
                ];

                // call_forward section
                $call_forward=[];
                $forward=CallForward::find($device->owner_uuid);
                if($forward){
                    $call_forward=[
                        'direct_calls_only'=>$forward->direct_calls_only,
                        'enabled'=>true,
                        'failover'=>false,
                        'ignore_early_media'=>$forward->ignore_early_media,
                        'keep_caller_id'=>$forward->keep_caller_id,
                        'number'=>$forward->number,
                        'require_keypress'=>$forward->require_keypress,
                        'substitute'=>true,
                    ];
                }

                // caller_id section
                $caller_id=[
                    'emergency'=>[
                        'name'=>'',
                        'number'=>''
                    ],
                    'internal'=>[
                        'name'=>$device->internal_caller_id_name,
                        'number'=>$device->internal_caller_id_number
                    ],
                    'external'=>[
                        'name'=>$device->external_caller_id_name,
                        'number'=>$device->external_caller_id_number
                    ]
                ];

                // contact_list section
                $contact_list=[
                    'exclude'=>$device->hide_contact_list
                ];

                // dial_plans section
                $dial_plan=[];

                // outbound_flags section
                $outbound_flags=[];

                // music_on_hold section
                $music_on_hold=[];

                // ringtones section
                $ringtones=[];

                $deviceJson=[
                    'id'=>$device->device_uuid,
                    'mac_address'=> $device->device_mac_address,
                    'username'=>$device->username,
                    'name'=>$device->device_name,
                    'enabled'=>$device->status,
                    'device_type'=>$device->device_type,
                    'owner_id'=>$device->device_owner_uuid,
                    'call_restriction'=>$call_restriction,
                    'caller_id'=>$caller_id,
                    'contact_list'=>$contact_list,
                    'dial_plan'=>$dial_plan,
                    'exclude_from_queues'=> false,
                    'media'=> $media,
                    'call_forward'=>$call_forward,
                    'outbound_flags'=>$outbound_flags,
                    'music_on_hold'=> $music_on_hold,
                    'mwi_unsolicitated_updates'=> true,
                    'register_overwrite_notify'=> false,
                    'ringtones'=> $ringtones,
                    'sip'=> [
                        'invite_format'=> '',
                        'username'=>$device->sip_username,
                        'method'=> '',
                        'password'=>$device->sip_password,
                        'registration_expiration'=> 300,
                        'expire_seconds'=>$device->expire_seconds
                    ],
                    'suppress_unregister_notifications'=> false
                ];
                return $deviceJson;
            });

            return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>$device,
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);

        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        }
    }

    public function getStatus($accountId){
        $devices= Cache::rememberForever($accountId . '_device_status',function() use ($accountId){
            $devices=Device::where('domain_uuid',$accountId)->get();

            $deviceJson=array();
            $i=0;
            foreach ($devices as $device) {
                $deviceJson[$i]=[
                    'device_id'=>$device->device_uuid,
                    'registered'=>$device->status
                ];
                $i++;
            }
            return $deviceJson;
        });

        return response()->json([
            'auth_token'=> (string)Auth::getToken(),
            'data'=>$devices,
            'status'=>'success',
            'request_id'=> uniqid(),
            'revision'=> '{REVISION}',
            'status_code'=>200
        ]);
    }

    public function create(Request $request,$accountId){
        try {

            $device=new Device();
            $data=$request->data;
            
            // common fields
            $device->device_uuid=(string) Uuid::generate();#primary key
            $device->domain_uuid=$accountId;#foreign Key
            $device->device_type=$data['device_type'];#device_type;

            if(isset($data['name'])){
                $device->device_name=$data['name'];#device_name;
            }

            
            if(isset($data['enabled'])){
                $device->status=$data['enabled'];#device_status;
            }
            
            if(isset($data['owner_id'])){
                $device->device_owner_uuid=$data['owner_id'];
            }
            
            if(isset($data['presence_id'])){
                $device->presence_id=$data['presence_id'];
            }

            if(isset($data['suppress_unregister_notifications'])){
                $device->device_unreg_notify=$data['suppress_unregister_notifications'];
            }
            if(isset($data['mac_address'])){
                $device->device_mac_address=$data['mac_address'];
            }
            
            // ignore_completed_elsewhere
            if(isset($data['ignore_completed_elsewhere'])){
                $device->ignore_completed_elsewhere=$data['ignore_completed_elsewhere'];
            }

            if(isset($data['sip']['password'])){
                $device->sip_password=$data['sip']['password'];
            }

            if(isset($data['sip']['expire_seconds'])){
                $device->expire_seconds=$data['sip']['expire_seconds'];
            }
            
            if(isset($data['sip']['username'])){
                $device->sip_username=$data['sip']['username'];
            }

            if(isset($data['sip']['realm'])){

            }

            if(isset($data['sip']['method'])){

            }

            if(isset($data['sip']['ip'])){

            }

            if(isset($data['sip']['invite_format'])){
                // $device->invite_format=$data['sip']['invite_format'];
            }

            if(isset($data['call_forward']['number'])){
                if(isset($data['owner_id'])){
                    $forward=CallForward::find($data['owner_id']);
                    if(!$forward){
                        $forward=new CallForward();
                    }
                    $forward->user_uuid=(string)$data['owner_id'];
                    $forward->number=$data['call_forward']['number'];
                    $forward->save();
                }
            }
            

            if(isset($data['media'])){

                if(isset($data['media']['encryption'])){
                    $encryption=$data['media']['encryption'];
                }

                if(isset($data['media']['fax'])){
                    $fax=$media['fax'];
                }
                
                if(isset($data['media']['fax_option'])){
                    $fax_option=$data['media']['fax_option'];
                }
                
                if(isset($data['media']['peer_to_peer'])){
                    $peer_to_peer=$data['media']['peer_to_peer'];
                }
                
                if(isset($data['media']['video']['codecs'])){
                    $device->video_codec=implode(',', $data['media']['video']['codecs']);
                }

                if(isset($data['media']['audio']['codecs'])){
                    $device->audio_codec=implode(',', $data['media']['audio']['codecs']);
                }
            }
        

            if(isset($data['call_restriction'])){

                if(isset($data['call_restriction']['caribbean']['action'])){
                    if($data['call_restriction']['caribbean']['action']=='inherit'){
                        $device->caribbean=1;
                    }else{
                        $device->caribbean=0;
                    }
                }

                if(isset($data['call_restriction']['closed_groups']['action'])){
                    if($data['call_restriction']['closed_groups']['action']=='inherit'){
                        $device->closed_groups=1;
                    }else{
                        $device->closed_groups=0;
                    }
                }
                
                if(isset($data['call_restriction']['did_us']['action'])){
                    if($data['call_restriction']['did_us']['action']=='inherit'){
                        $device->uk_did=1;
                    }else{
                        $device->uk_did=0;
                    }
                }
                
                if(isset($data['call_restriction']['emergency']['action'])){
                    if($data['call_restriction']['emergency']['action']=='inherit'){
                        $device->emergency_dispatcher=1;
                    }else{
                        $device->emergency_dispatcher=0;
                    }
                }
                

                if(isset($data['call_restriction']['international']['action'])){
                   if($data['call_restriction']['international']['action']=='inherit'){
                        $device->international=1;
                    }else{
                        $device->international=0;
                    } 
                }
                

                if(isset($data['call_restriction']['toll_us']['action'])){
                    if($data['call_restriction']['toll_us']['action']=='inherit'){
                        $device->uk_toll=1;
                    }else{
                        $device->uk_toll=0;
                    }
                }
                

                if(isset($data['call_restriction']['tollfree_us']['action'])){
                    if($data['call_restriction']['tollfree_us']['action']=='inherit'){
                        $device->uk_toll_free=1;
                    }else{
                        $device->uk_toll_free=0;
                    }
                }
                

                if(isset($data['call_restriction']['unknown']['action'])){
                    if($data['call_restriction']['unknown']['action']=='inherit'){
                        $device->unknown=1;
                    }else{
                        $device->unknown=0;
                    } 
                }
                
            }

            if(isset($data['caller_id]'])){
                if(isset($data['caller_id']['emergency'])){
                    
                }

                if(isset($data['caller_id']['external'])){
                    $device->external_caller_id_name=$data['caller_id']['external']['name'];
                    $device->external_caller_id_number=$data['caller_id']['external']['number'];
                }

                if(isset($data['caller_id']['internal'])){
                    $device->internal_caller_id_name=$data['caller_id']['internal']['name'];
                    $device->internal_caller_id_number=$data['caller_id']['internal']['number'];
                }
            }

            if(isset($data['outbound_flags'])){

            }

            if(isset($data['ringtones'])){
                if(isset($data['ringtones']['internal'])){
                    // $device->internal_ringtones=$data['ringtones']['internal'];
                }
                
                if(isset($data['ringtones']['external'])){
                    // $device->external_ringtones=$data['ringtones']['external'];
                }
            }
            


            if(isset($data['contact_list']['exclude'])){
                $device->hide_contact_list=$data['contact_list']['exclude'];
            }

            if(isset($data['ui_metadata'])){
                if(isset($data['ui_metadata']['origin'])){

                }
                if(isset($data['ui_metadata']['ui'])){

                }
                if(isset($data['ui_metadata']['version'])){

                }
                
            }

            $device->save();

            
            Cache::forget($accountId . '_device');
            Cache::forget($accountId . '_user');
            return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>array_merge(['id'=>$device->device_uuid],$request->data),
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);

        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        }
    }

    public function update(Request $request,$accountId,$deviceId){
        try {

            $device= Device::find($deviceId);
            $data=$request->data;
            
            if(isset($data['name'])){
                $device->device_name=$data['name'];#device_name;
            }

            
            if(isset($data['enabled'])){
                $device->status=$data['enabled'];#device_status;
            }
            
            if(isset($data['owner_id'])){
                $device->device_owner_uuid=$data['owner_id'];
            } else {
                $device->device_owner_uuid=null;
            }
            
            if(isset($data['presence_id'])){
                $device->presence_id=$data['presence_id'];
            }

            if(isset($data['suppress_unregister_notifications'])){
                $device->device_unreg_notify=$data['suppress_unregister_notifications'];
            }
            if(isset($data['mac_address'])){
                $device->device_mac_address=$data['mac_address'];
            }
            
            // ignore_completed_elsewhere
            if(isset($data['ignore_completed_elsewhere'])){
                $device->ignore_completed_elsewhere=$data['ignore_completed_elsewhere'];
            }

            if(isset($data['sip']['password'])){
                $device->sip_password=$data['sip']['password'];
            }

            if(isset($data['sip']['expire_seconds'])){
                $device->expire_seconds=$data['sip']['expire_seconds'];
            }
            
            if(isset($data['sip']['username'])){
                $device->sip_username=$data['sip']['username'];
            }

            if(isset($data['sip']['realm'])){

            }

            if(isset($data['sip']['method'])){

            }

            if(isset($data['sip']['ip'])){

            }
            
            if(isset($data['sip']['invite_format'])){
                // $device->invite_format=$data['sip']['invite_format'];
            }

            if(isset($data['call_forward']['number'])){
                if(isset($data['owner_id'])){
                    $forward=CallForward::find($data['owner_id']);
                    if(!$forward){
                        $forward=new CallForward();
                    }
                    $forward->user_uuid=(string)$data['owner_id'];
                    $forward->number=$data['call_forward']['number'];
                    $forward->save();
                }
            }
            

            if(isset($data['media'])){

                if(isset($data['media']['encryption'])){
                    $encryption=$data['media']['encryption'];
                }

                if(isset($data['media']['fax'])){
                    $fax=$data['media']['fax'];
                }
                
                if(isset($data['media']['fax_option'])){
                    $fax_option=$data['media']['fax_option'];
                }
                
                if(isset($data['media']['peer_to_peer'])){
                    $peer_to_peer=$data['media']['peer_to_peer'];
                }
                
                if(isset($data['media']['video']['codecs'])){
                    $video_codecs=implode(',', $data['media']['audio']['codecs']);
                }

                if(isset($data['media']['audio']['codecs'])){
                    $audio_codecs=implode(',', $data['media']['audio']['codecs']);
                }
            }
        

            if(isset($data['call_restriction'])){

                if(isset($data['call_restriction']['caribbean']['action'])){
                    if($data['call_restriction']['caribbean']['action']=='inherit'){
                        $device->caribbean=1;
                    }else{
                        $device->caribbean=0;
                    }
                }

                if(isset($data['call_restriction']['closed_groups']['action'])){
                    if($data['call_restriction']['closed_groups']['action']=='inherit'){
                        $device->closed_groups=1;
                    }else{
                        $device->closed_groups=0;
                    }
                }
                
                if(isset($data['call_restriction']['did_us']['action'])){
                    if($data['call_restriction']['did_us']['action']=='inherit'){
                        $device->uk_did=1;
                    }else{
                        $device->uk_did=0;
                    }
                }
                
                if(isset($data['call_restriction']['emergency']['action'])){
                    if($data['call_restriction']['emergency']['action']=='inherit'){
                        $device->emergency_dispatcher=1;
                    }else{
                        $device->emergency_dispatcher=0;
                    }
                }
                

                if(isset($data['call_restriction']['international']['action'])){
                   if($data['call_restriction']['international']['action']=='inherit'){
                        $device->international=1;
                    }else{
                        $device->international=0;
                    } 
                }
                

                if(isset($data['call_restriction']['toll_us']['action'])){
                    if($data['call_restriction']['toll_us']['action']=='inherit'){
                        $device->uk_toll=1;
                    }else{
                        $device->uk_toll=0;
                    }
                }
                

                if(isset($data['call_restriction']['tollfree_us']['action'])){
                    if($data['call_restriction']['tollfree_us']['action']=='inherit'){
                        $device->uk_toll_free=1;
                    }else{
                        $device->uk_toll_free=0;
                    }
                }
                

                if(isset($data['call_restriction']['unknown']['action'])){
                    if($data['call_restriction']['unknown']['action']=='inherit'){
                        $device->unknown=1;
                    }else{
                        $device->unknown=0;
                    } 
                }
                
            }

            if(isset($data['caller_id]'])){
                if(isset($data['caller_id']['emergency'])){
                    
                }

                if(isset($data['caller_id']['external'])){
                    $device->external_caller_id_name=$data['caller_id']['external']['name'];
                    $device->external_caller_id_number=$data['caller_id']['external']['number'];
                }

                if(isset($data['caller_id']['internal'])){
                    $device->internal_caller_id_name=$data['caller_id']['internal']['name'];
                    $device->internal_caller_id_number=$data['caller_id']['internal']['number'];
                }
            }

            if(isset($data['outbound_flags'])){

            }

            if(isset($data['ringtones'])){
                if(isset($data['ringtones']['internal'])){
                    $device->internal_ringtones=$data['ringtones']['internal'];
                }
                
                if(isset($data['ringtones']['external'])){
                    $device->external_ringtones=$data['ringtones']['external'];
                }
            }

            if(isset($data['contact_list']['exclude'])){
                $device->hide_contact_list=$data['contact_list']['exclude'];
            }

            if(isset($data['ui_metadata'])){
                if(isset($data['ui_metadata']['origin'])){

                }
                if(isset($data['ui_metadata']['ui'])){

                }
                if(isset($data['ui_metadata']['version'])){

                }
                
            }

            $device->save();
            
            Cache::forget($accountId . '_device_'. $deviceId);
            Cache::forget($accountId . '_device');
            Cache::forget($accountId . '_user');

            return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>array_merge(['id'=>$device->device_uuid],$request->data),
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);

        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));

        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        }
    }

    public function destroy($accountId,$deviceId){
        
        if($device=Device::find($deviceId)){
            if(Device::find($deviceId)->delete()){
                Cache::forget($accountId . '_device_'. $deviceId);
                Cache::forget($accountId . '_device');
                Cache::forget($accountId . '_user');

                // Call Restriction
                $call_restriction=[];
                $restriction=CallRestriction::find($accountId);
                if($restriction){
                    $call_restriction=[
                        'caribbean'=>[
                            'action'=> $restriction->caribbean
                        ],
                        'did_us'=>[
                            'action'=> $restriction->uk_did
                        ],
                        'emergency'=>[
                            'action'=> $restriction->emergency_dispatcher
                        ],
                        'international'=>[
                            'action'=> $restriction->international
                        ],
                        'toll_us'=>[
                            'action'=> $restriction->uk_toll
                        ],
                        'tollfree_us'=>[
                            'action'=> $restriction->uk_toll_free
                        ],
                        'unknown'=>[
                            'action'=> $restriction->unknown
                        ],
                    ];
                }
            
                // ringtones section
                $ringtones=[];

                // Outbound Flags
                $outbound_flags=[];

                // caller_id section
                $caller_id=[
                    'emergency'=>[
                        'name'=>'',
                        'number'=>''
                    ],
                    'internal'=>[
                        'name'=>$device->internal_caller_id_name,
                        'number'=>$device->internal_caller_id_number
                    ],
                    'external'=>[
                        'name'=>$device->external_caller_id_name,
                        'number'=>$device->external_caller_id_number
                    ]
                ];

                // Music On Hold
                $music_on_hold=[];

                // Contact List
                $contact_list=[];

                $dial_plan=[];
                // media section
                /*$medias=Media::where('domain_uuid',$accountId)->get();*/
                $media=[
                    'audio'=> [
                        'codecs'=> [
                            'PCMU'
                        ]
                    ],
                    'encryption'=> [
                        'enforce_security'=> false,
                        'methods'=> []
                    ],
                    'fax'=>[
                        'option'=>false
                    ],
                    'fax_option'=>false,
                    'peer_to_peer'=>[

                    ],
                    'video'=> [
                        'codecs'=> []
                    ]
                ];
           
                // call_forward section
                $call_forward=[
                    'direct_calls_only'=>false,
                    'enabled'=>true,
                    'failover'=>false,
                    'ignore_early_media'=>true,
                    'keep_caller_id'=>true,
                    'number'=>"+971566114341",
                    'require_keypress'=>false,
                    'substitute'=>true,
                ];

                $deviceJson=[
                    'id'=>$device->device_uuid,
                    'mac_address'=> $device->device_mac_address,
                    'username'=>$device->username,
                    'name'=>$device->device_name,
                    'enabled'=>$device->status,
                    'device_type'=>$device->device_type,
                    'owner_id'=>$device->device_owner_uuid,
                    'call_restriction'=>$call_restriction,
                    'caller_id'=>$caller_id,
                    'contact_list'=>$contact_list,
                    'dial_plan'=>$dial_plan,
                    'exclude_from_queues'=> false,
                    'media'=> $media,
                    'call_forward'=>$call_forward,
                    'outbound_flags'=>$outbound_flags,
                    'music_on_hold'=> $music_on_hold,
                    'mwi_unsolicitated_updates'=> true,
                    'register_overwrite_notify'=> false,
                    'ringtones'=> $ringtones,
                    'sip'=> [
                        'invite_format'=> '',
                        'username'=>$device->sip_username,
                        'method'=> '',
                        'password'=>$device->sip_password,
                        'registration_expiration'=> 300,
                        'expire_seconds'=>$device->expire_seconds
                    ],
                    'suppress_unregister_notifications'=> false
                ];

                return response()->json([
                    'auth_token'=> (string)Auth::getToken(),
                    'data'=>$deviceJson,
                    'status'=>'success',
                    'request_id'=> uniqid(),
                    'revision'=> '{REVISION}',
                    'status_code'=>200
                ]);

            }
        }
    }
}

<?php 
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

use App\Models\ServicePlan;
use Auth;
use Illuminate\Http\Request;
use App\Models\Account;
use App\Models\Limit;
use Webpatser\Uuid\Uuid;
use DB;

class ServicesPlanController extends Controller
{
	
	public function index($accountId){

		$servicePlans= ServicePlan::where('domain_uuid',$accountId)->get();
            
        $i=0;
        $jsonPlans = array();

        foreach($servicePlans as $plan) {
            $jsonPlans[$i]=[
                'id'=> $plan->plan_uuid,
                'name'=>$plan->name,
                'description'=>$plan->description,
            ];

            $i++;
        }
           
        return response()->json([
            'auth_token'=> Auth::getToken(),
            'data'=>$jsonPlans,
            'status'=>'success',
            'request_id'=> uniqid(),
            'revision'=> '{REVISION}',
            'status_code'=>200
        ]);
	}

	// available service plans
	public function available($accountId){
		try{

			$servicePlan= ServicePlan::where('domain_uuid',$accountId)->first();

			$jsonPlan=[];
			if($servicePlan){
				$jsonPlan=[
			        // 'account_quantities'=> $servicePlan->account_quantities,
			        'cascade_quantities'=> json_decode($servicePlan->cascade_quantities),
			        'id'=>$servicePlan->plan_uuid,
			        'plans'=> json_decode($servicePlan->plan),
			        'billing_id'=> $servicePlan->billing_id,
			        'reseller'=> $servicePlan->reseller,
			        'reseller_id'=> $servicePlan->reseller_id,
			        'dirty'=> $servicePlan->dirty,
			        'in_good_standing'=> $ServicePlan->in_good_standing,
			        'items'=> json_decode($servicePlan->items),
			        'name'=>$servicePlan->name
			    ];
			}
	        
			
	        return response()->json([
	            'auth_token'=> Auth::getToken(),
	            'data'=>$jsonPlan,
	            'status'=>'success',
	            'request_id'=> uniqid(),
	            'revision'=> '{REVISION}',
	            'status_code'=>200
	        ]);

		} catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        }
	}

	// enabled service plan
	public function current($accountId)
	{
		try{

			$servicePlan= ServicePlan::where('domain_uuid',$accountId)->first();

			$jsonPlan=[];
			if($servicePlan){
				$jsonPlan=[
			        // 'account_quantities'=> $servicePlan->account_quantities,
			        'cascade_quantities'=> json_decode($servicePlan->cascade_quantities),
			        'id'=>$servicePlan->plan_uuid,
			        'plans'=> json_decode($servicePlan->plan),
			        'billing_id'=> $servicePlan->billing_id,
			        'reseller'=> $servicePlan->reseller,
			        'reseller_id'=> $servicePlan->reseller_id,
			        'dirty'=> $servicePlan->dirty,
			        'in_good_standing'=> $ServicePlan->in_good_standing,
			        'items'=> json_decode($servicePlan->items),
			        'name'=>$servicePlan->name
			    ];
			}
	        
			
	        return response()->json([
	            'auth_token'=> Auth::getToken(),
	            'data'=>$jsonPlan,
	            'status'=>'success',
	            'request_id'=> uniqid(),
	            'revision'=> '{REVISION}',
	            'status_code'=>200
	        ]);

		} catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        }
		
	}

	public function show($accountId,$planId){
		try {
            $servicePlan=ServicePlan::where('domain_uuid',$accountId)->where('plan_uuid',$planId)->first();

           
            $jsonPlan=[];
			if($servicePlan){
				$jsonPlan=[
			        // 'account_quantities'=> $servicePlan->account_quantities,
			        'cascade_quantities'=> json_decode($servicePlan->cascade_quantities),
			        'id'=>$servicePlan->plan_uuid,
			        'plans'=> json_decode($servicePlan->plan),
			        'billing_id'=> $servicePlan->billing_id,
			        'reseller'=> $servicePlan->reseller,
			        'reseller_id'=> $servicePlan->reseller_id,
			        'dirty'=> $servicePlan->dirty,
			        'in_good_standing'=> $ServicePlan->in_good_standing,
			        'items'=> json_decode($servicePlan->items),
			        'name'=>$servicePlan->name
			    ];
			}
	        
			
	        return response()->json([
	            'auth_token'=> Auth::getToken(),
	            'data'=>$jsonPlan,
	            'status'=>'success',
	            'request_id'=> uniqid(),
	            'revision'=> '{REVISION}',
	            'status_code'=>200
	        ]);

        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        }
	}

	public function store(Request $request,$accountId){

	}

	public function destroy($accountId,$planId){

	}

	public function reconciliate(Request $request,$accountId){

	}

	public function synchronize(Request $request,$accountId){
		
	}
	public function override(Request $request, $accountId){
		if(isset($request))
		{
			return $request->all();
		}

	}
}
<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

use Auth;
use App\Models\CallRestriction;
use Illuminate\Http\Request;
use App\Models\Account;
use App\Models\Limit;
use Webpatser\Uuid\Uuid;
use DB;

class LimitController extends Controller
{
    
    public function __construct()
    {
        $this->storage=Redis::connection();
    }

    public function getLimit($accountId){
         // $limit= Cache::rememberForever($accountId . '_limit',function() 
        try {
            $limit = Cache::rememberForever($accountId . '_limit',function() use($accountId){
                $limit=Limit::find($accountId);
                $restriction=CallRestriction::find($accountId);

                $call_restriction=[];
                if($restriction){
                    $call_restriction=[
                        'caribbean'=>[
                            'action'=> $restriction->caribbean
                        ],
                        'did_us'=>[
                            'action'=> $restriction->uk_did
                        ],
                        'emergency'=>[
                            'action'=> $restriction->emergency_dispatcher
                        ],
                        'international'=>[
                            'action'=> $restriction->international
                        ],
                        'toll_us'=>[
                            'action'=> $restriction->uk_toll
                        ],
                        'tollfree_us'=>[
                            'action'=> $restriction->uk_toll_free
                        ],
                        'unknown'=>[
                            'action'=> $restriction->unknown
                        ],
                    ];
                }
                
                $limitJson=[
                    'allow_postpay'=>$limit->allow_postpay,
                    'allow_prepay'=>$limit->allow_prepay,
                    'call_restriction'=>$call_restriction,
                    'id'=>$limit->domain_uuid,
                    'inbound_trunks'=>$limit->inbound_trunks,
                    'max_postpay_amount'=>true,
                    'outbound_trunks'=>$limit->outbound_trunks,
                    'twoway_trunks'=>$limit->twoway_trunks, 
                ];
                return $limitJson; 
            });

            return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>$limit,
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);

        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        }
    }

    public function updateLimit(Request $request,$accountId){

    }

}

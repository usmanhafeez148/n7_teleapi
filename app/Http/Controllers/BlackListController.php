<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

use Auth;
use Illuminate\Http\Request;
use App\Models\BlackList;
use Webpatser\Uuid\Uuid;
use DB;

class BlackListController extends Controller
{
    
    public function __construct(){
        $this->storage=Redis::connection();
    }

    public function index($accountId)
    {
        $blacklists = Cache::rememberForever($accountId . '_blacklist',function() use($accountId){

            $blacklists=BlackList::where('domain_uuid',$accountId)->get();
            $i=0; 
            $blacklistJson = array();
            foreach($blacklists as $blacklist) {
                $blacklistJson[$i]=[
                'id'=> $blacklist->blacklist_uuid,
                'name'=>$blacklist->name,
                ];
                $i++;
            }
            return $blacklistJson;
        });

        return response()->json([
                'auth_token'=> Auth::getToken(),
                'data'=>$blacklists,
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);
    }

    public function show($accountId,$blacklistId)
    {
        try {
            $blacklist = Cache::rememberForever($accountId . '_blacklist_' . $blacklistId,function() use($accountId,$blacklistId){
                
                $blacklist=BlackList::where('blacklist_uuid',$blacklistId)->where('domain_uuid',$accountId)->first();
                
                $blacklistJson=[];
                
                if($blacklist){
                    $blacklistJson['id']=$blacklist->blacklist_uuid;
                    $blacklistJson['name']=$blacklist->name;
                    $blacklistJson['should_block_anonymous']=$blacklist->should_block_anonymous;
                    $blacklistJson['numbers']=json_decode($blacklist->numbers);
                }
                
                return $blacklistJson;
            });
            return response()->json([
                'auth_token'=> '',
                'data'=>$blacklist,
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);
        
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        }
    }

    

    public function store(Request $request,$accountId)
    {
        try {
            $blacklist=new BlackList();
            
            $blacklist->blacklist_uuid=(string) Uuid::generate();
            $blacklist->domain_uuid=$accountId;
            $blacklist->name=$request->data['name'];
            $blacklist->should_block_anonymous=$request->data['should_block_anonymous'];
            $blacklist->numbers=json_encode($request->data['numbers']);

            $blacklist->save();
            
            Cache::forget($accountId . '_blacklist');

            return response()->json([
                'auth_token'=> Auth::getToken(),
                'data'=>array_mege(['id'=>$blacklist->blacklist_uuid],$request->data),
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);

        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
               
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',

            ));
        }
    }

    
    public function update(Request $request, $accountId, $blacklistId)
    {
        try {
            $blacklist=BlackList::find($blacklistId);
            
            $blacklist->name=$request->data['name'];
            $blacklist->should_block_anonymous=$request->data['should_block_anonymous'];
            $blacklist->numbers=json_encode($request->data['numbers']);

            $blacklist->save();

            Cache::forget($accountId . '_blacklist_'. $blacklistId);
            Cache::forget($accountId . '_blacklist');


            return response()->json([
                'auth_token'=> Auth::getToken(),
                'data'=>array_mege(['id'=>$blacklist->blacklist_uuid],$request->data),
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);
            
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        }
    }

    
    public function destroy($accountId,$blacklistId)
    {
        if($blacklist=BlackList::find($blacklistId)){
            if(BlackList::find($blacklistId)->delete()){
                
                Cache::forget($accountId . '_blacklist_'. $blacklistId);
                Cache::forget($accountId . '_blacklist');

                // blacklist json
                $blacklistJson['id']=$blacklist->blacklist_uuid;
                $blacklistJson['name']=$blacklist->name;
                $blacklistJson['should_block_anonymous']=$blacklist->should_block_anonymous;
                $blacklistJson['numbers']=json_decode($blacklist->numbers);

                return response()->json([
                    'auth_token'=> Auth::getToken(),
                    'data'=>$blacklistJson,
                    'status'=>'success',
                    'request_id'=> uniqid(),
                    'revision'=> '{REVISION}',
                    'status_code'=>200
                ]);
            }
        }

        
    }
}

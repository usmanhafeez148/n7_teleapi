<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

use Auth;
use Illuminate\Http\Request;
use App\Models\CallLog;
use Webpatser\Uuid\Uuid;
use DB;

class CallLogController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->storage=Redis::connection();
    }

    
    public function index($accountId)
    {
        $callLogs= Cache::remember($accountId . '_calllog',10,function() use($accountId){

            $callLogs=CallLog::where('domain_uuid',$accountId)->get();
            $callLogsArray=array();
            $i=0;
            foreach ($callLogs as $callLog) {
                $callLogsArray[$i]=[
                    'id'=>$callLog->cdr_id,
                    'call_id'=> $callLog->cdr_id, 
                    'caller_id_name'=>$callLog->caller_id_name,
                    'caller_id_number'=> $callLog->caller_id_number,
                    'authorizing_id'=>$callLog->authorizing_uuid,
                    'billing_seconds'=>$callLog->billing_seconds,
                    'bridge_id'=>$callLog->cdr_id,
                    'call_id'=>$callLog->cdr_id,
                    'call_priority'=>"",
                    'call_type'=>"",
                    'callee_id_name'=>$callLog->callee_id_name,
                    'callee_id_number'=>$callLog->callee_id_number,
                    'calling_from'=>"",
                    'cost'=>"0",
                    'datetime'=>$callLog->datetime,
                    'dialed_number'=>$callLog->dialed_number,
                    'direction'=>$callLog->direction,
                    'duration_seconds'=>$callLog->duration_seconds,
                    'from'=>"6684@master3.local",
                    'hangup_cause'=>$callLog->hangup_cause,
                    'iso_8601'=>"2017-12-20",
                    'media_recordings'=>$callLog->media_recordings,
                    'other_leg_call_id'=>"",
                    'owner_id'=>$callLog->owner_uuid,
                    'rate'=>"0.0",
                    'rate_name'=>"",
                    'recording_url'=>"",
                    'request'=>"1001@master3.local",
                    'reseller_call_type'=>"",
                    'reseller_cost'=>"0",
                    'rfc_1036'=>"Wed, 20 Dec 2017 14:00:35 GMT",
                    'timestamp'=>"63680997635",
                    'to'=>"1001@master3.local",
                    'unix_timestamp'=>"1513778435",
                ];
                $i++;
            }
            return $callLogsArray;
        });
        
        return response()->json([
            'auth_token'=>(string)Auth::getToken(),
            'data'=>$callLogs,
            'request_id'=>uniqid(),
            'status'=>'success',
            'status_code'=>200
        ]);
    }

    public function interaction($accountId){
        $callLogs= Cache::remember($accountId . '_calllog',10,function() use($accountId){

            $callLogs=CallLog::where('domain_uuid',$accountId)->get();
            $callLogsArray=array();
            $i=0;
            foreach ($callLogs as $callLog) {
                $callLogsArray[$i]=[
                    'id'=>$callLog->cdr_id,
                    'call_id'=> $callLog->cdr_id, 
                    'caller_id_name'=>$callLog->caller_id_name,
                    'caller_id_number'=> $callLog->caller_id_number,
                    'authorizing_id'=>$callLog->authorizing_uuid,
                    'billing_seconds'=>$callLog->billing_seconds,
                    'bridge_id'=>$callLog->cdr_id,
                    'call_id'=>$callLog->cdr_id,
                    'call_priority'=>"",
                    'call_type'=>"",
                    'callee_id_name'=>$callLog->callee_id_name,
                    'callee_id_number'=>$callLog->callee_id_number,
                    'calling_from'=>"",
                    'cost'=>"0",
                    'datetime'=>$callLog->datetime,
                    'dialed_number'=>$callLog->dialed_number,
                    'direction'=>$callLog->direction,
                    'duration_seconds'=>$callLog->duration_seconds,
                    'from'=>"6684@master3.local",
                    'hangup_cause'=>$callLog->hangup_cause,
                    'iso_8601'=>"2017-12-20",
                    'media_recordings'=>$callLog->media_recordings,
                    'other_leg_call_id'=>"",
                    'owner_id'=>$callLog->owner_uuid,
                    'rate'=>"0.0",
                    'rate_name'=>"",
                    'recording_url'=>"",
                    'request'=>"1001@master3.local",
                    'reseller_call_type'=>"",
                    'reseller_cost'=>"0",
                    'rfc_1036'=>"Wed, 20 Dec 2017 14:00:35 GMT",
                    'timestamp'=>"63680997635",
                    'to'=>"1001@master3.local",
                    'unix_timestamp'=>"1513778435",
                ];
                $i++;
            }
            return $callLogsArray;
        });
        
        return response()->json([
            'auth_token'=>'',
            'data'=>$callLogs,
            'request_id'=>uniqid(),
            'status'=>'success',
            'status_code'=>200
        ]);
    }
    
   
    public function show($accountId,$callLogId)
    {
        /*$data=[
            'app_name'=>"ecallmgr",
            'app_version'=>"4.0.0",
            'billing_seconds'=>"11",
            'call_direction'=>"inbound",
            'call_id'=>"3470911290@10.10.9.112",
            'caller_id_name'=>"Account Admin",
            'caller_id_number'=>"6684",
            'channel_call_state'=>"HANGUP",
            'channel_created_time'=>'',
            'channel_name'=>"",
            'channel_state'=>"REPORTING"
            'custom_channel_vars'=>[
                'account_id'=> "914e0a1f2170de4cbe63dbd8b911983b", 
                'account_name'=> "master3"
            ]
            'custom_sip_headers'=>[]
            'disposition'=>"ANSWER",
            'duration_seconds'=>"12",
            'event_category'=>"call_event",
            'event_name'=>"CHANNEL_DESTROY",
            'from'=>"6684@master3.local",
            'from_tag'=>"1151563241",
            'from_uri'=>"user_3rr7x7es6k@master3.local",
            'hangup_cause'=>"NORMAL_CLEARING",
            'hangup_code'=>"sip:200",
            'id'=>"201712-3470911290@10.10.9.112",
            'interaction_id'=>"63680997635-4e7a2cd6",
            'interaction_key'=>"4e7a2cd6",
            'interaction_time'=>'63680997635',
            'local_sdp'=>"",
            'media_server'=>"kazoo.timegroup.ae",
            'msg_id'=>"1513778447254921",
            'node'=>"ecallmgr@kazoo.timegroup.ae",
            'presence_id'=>"6684@master3.local",
            'remote_sdp'=>"",
            'request'=>"1001@master3.local",
            'ringing_seconds'=>"0",
            'switch_nodename'=>"freeswitch@kazoo.timegroup.ae",
            'switch_uri'=>"sip:10.10.9.103:11000",
            'switch_url'=>"sip:mod_sofia@10.10.9.103:11000",
            'timestamp'=>'63680997647',
            'to'=>"1001@master3.local",
            'to_tag'=>"F1amSUcQ6t71D",
            'to_uri'=>"1001@master3.local",
            'user_agent'=>"Yealink SIP-T22P 7.73.0.50"
        ];*/
        
        /*$callLog=Cache::tags('callLog:' . $callLogId)->remember('callLog:' . $callLogId,1,function() use($callLogId){
            return CallLog::find($callLogId);
        });*/
    }
    
    public function destroy($accountId,$callLogId){
    }
}

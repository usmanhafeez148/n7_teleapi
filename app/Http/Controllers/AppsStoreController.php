<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

use Auth;
use Illuminate\Http\Request;
use App\Models\AppStore;
use App\Models\BlackListApp;
use App\Models\Account;
use App\Models\Limit;
use App\Models\User;
use Webpatser\Uuid\Uuid;
use DB;

class AppsStoreController extends Controller
{
    
    public function __construct()
    {
        $this->storage=Redis::connection();
    }

    public function index($accountId)
    {
        // $appStores = Cache::rememberForever($accountId . '_apps_store',function() use($accountId){
            $appStores=AppStore::where('account_uuid',$accountId)->get();
            
            $jsonApp=[];
            $i=0;
            foreach ($appStores as $appStore) {
                
                $features=explode(',', $appStore->features);
                
                $jsonApp[$i]=[
                    'account_id'=>$appStore->account_uuid,
                    'api_url'=>$appStore->api_url,
                    'i18n'=>json_decode($appStore->i18n),
                    'id'=>$appStore->appstore_uuid,
                    'masqueradable'=>$appStore->masqueradable,
                    'name'=>$appStore->name,
                    'tags'=>explode(',',$appStore->tags),
                    'allowed_users'=>$appStore->allowed_users,
                    'users'=>json_decode($appStore->users),
                ];
                
                $i++;
            }
            // return $jsonApp;
        // });
        
        return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>$jsonApp,
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);
    }

    public function show($accountId,$appId)
    {
        try {
            // $appStore=Cache::rememberForever($accountId . '_apps_store_' . $appId,function() use($accountId,$appId){
                
                $appStore=AppStore::where('account_uuid',$accountId)->where('appstore_uuid',$appId)->first();
                $jsonApp=[];

                if($appStore){
                    
                    $features=explode(',', $appStore->features);
                    $screenshots=explode(',', $appStore->screenshots);
                    $jsonApp=[
                        'account_id'=>$appStore->account_id,
                        'api_url'=>$appStore->api_url,
                        'i18n'=>json_decode($appStore->i18n),
                        'id'=>$appStore->appstore_uuid,
                        'masqueradable'=>$appStore->masqueradable,
                        'name'=>$appStore->name,
                        'tags'=>json_decode($appStore->tags),
                        'allowed_users'=>$appStore->allowed_users,
                        'author'=>'',
                        'users'=>json_encode($appStore->users),
                        'urls'=>[
                            'howto'=>'',
                            'documentation'=>''
                        ],
                        'screenshots'=>$screenshots,
                        'published'=>'',
                        'price'=>'',
                        'icon'=>$appStore->icon,
                        'license'=>''
                    ];
                }
                
                // return $jsonApp;
            // });
            
            return response()->json([
                    'auth_token'=> (string)Auth::getToken(),
                    'data'=>$jsonApp,
                    'status'=>'success',
                    'request_id'=> uniqid(),
                    'revision'=> '{REVISION}',
                    'status_code'=>200
                ]);
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        } 
    }
    
    public function appsLink($accountId){

        $appsLink = Cache::rememberForever($accountId . '_apps_link',function() use($accountId){
            $account=Account::where('domain_uuid',$accountId)->first();
            
            $jsonAppLink=[];
            if($account){
                $appStores=AppStore::where('account_uuid',$accountId)->get();

                $jsonApps=[];
                $i=0;
                foreach ($appStores as $appStore) {
                    $jsonApps[$i]= [
                        'api_url'=> $appStore->api_url,
                        'id'=> $appStore->appstore_uuid,
                        'label'=> $appStore->label,
                        'name'=> $appStore->name
                    ];
                    
                    $i++;
                }

                
                $jsonAppLink=[
                    'account'=> [
                            'account_id'=> $account->domain_uuid,
                            'account_name'=> $account->name,
                            'is_master'=> false,
                            'is_reseller'=> $account->is_reseller,
                            'language'=> $user->language,
                            'reseller_id'=> $account->resellar_uuid
                        ],
                    'auth_token'=> [
                        'account_id'=> $account->domain_uuid,
                        'account_name'=> $account->name,
                        'apps'=>$jsonApps,
                        'is_master'=> false,
                        'is_reseller'=> $account->is_reseller,
                        'language'=> $user->language,
                        'method'=> '',
                        'owner_id'=> Auth::user()->user_uuid,
                        'reseller_id'=> $account->resellar_uuid
                    ]
                ];
            }
            
            return $jsonAppLink;
        });
        
        return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>$appsLink,
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);
        
    }

    public function install(Request $request,$accountId,$appId)
    {
        try{

            // specific,all,admins
            $appStore=AppStore::find($appId);
            
            if(isset($request->data['allowed_users'])){
                if($request->data['allowed_users']=="specific"){
                    $appStore->allowed_users=$request->data['allowed_users'];
                    $appStore->users=json_encode($request->data['users']);
                }elseif($request->data['allowed_users']=="all"){
                    $appStore->allowed_users=$request->data['allowed_users'];
                }elseif($request->data['allowed_users']=="admins"){
                    $appStore->allowed_users=$request->data['allowed_users'];
                }
                
            }
            
            $appStore->save();
            
            Cache::forget($account . '_apps_link');
            Cache::forget($account . '_apps_store');
            Cache::forget($account . '_blacklist_apps_store');
            
            Cache::forget($account . '_apps_link_' . $appId);
            Cache::forget($account . '_apps_store_' . $appId);
            Cache::forget($account . '_blacklist_apps_store_' . $appId);

            return response()->json([
                    'auth_token'=> (string)Auth::getToken(),
                    'data'=>['id'=>$appStore->appstore_uuid,$request->data],
                    'status'=>'success',
                    'request_id'=> uniqid(),
                    'revision'=> '{REVISION}',
                    'status_code'=>200
                ]);
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        }
    }

    public function updatePermissions(Request $request,$accountId,$appId){
        try{

            // specific,all,admins
            $appStore=AppStore::find($appId);
            
            if(isset($request->data['allowed_users'])){
                if($request->data['allowed_users']=="specific"){
                    $appStore->allowed_users=$request->data['allowed_users'];
                    $appStore->users=json_encode($request->data['users']);
                }elseif($request->data['allowed_users']=="all"){
                    $appStore->allowed_users=$request->data['allowed_users'];
                }elseif($request->data['allowed_users']=="admins"){
                    $appStore->allowed_users=$request->data['allowed_users'];
                }
                
            }
            
            $appStore->save();
            
            Cache::forget($account . '_apps_link');
            Cache::forget($account . '_apps_store');
            Cache::forget($account . '_blacklist_apps_store');
            
            Cache::forget($account . '_apps_link_' . $appId);
            Cache::forget($account . '_apps_store_' . $appId);
            Cache::forget($account . '_blacklist_apps_store_' . $appId);

            return response()->json([
                    'auth_token'=> (string)Auth::getToken(),
                    'data'=>['id'=>$appStore->appstore_uuid,$request->data],
                    'status'=>'success',
                    'request_id'=> uniqid(),
                    'revision'=> '{REVISION}',
                    'status_code'=>200
                ]);
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        }
    }

    public function uninstall($accountId,$appId)
    {
        try{
            $appStore=AppStore::find($appId);
            $appStore->allowed_users="";
            $appStore->users=null;
            
            $appStore->save();
            
            Cache::forget($account . '_apps_link');
            Cache::forget($account . '_apps_store');
            Cache::forget($account . '_blacklist_apps_store');
            
            Cache::forget($account . '_apps_link_' . $appId);
            Cache::forget($account . '_apps_store_' . $appId);
            Cache::forget($account . '_blacklist_apps_store_' . $appId);

            return response()->json([
                    'auth_token'=> (string)Auth::getToken(),
                    'data'=>['id'=>$appStore->appstore_uuid,$request->data],
                    'status'=>'success',
                    'request_id'=> uniqid(),
                    'revision'=> '{REVISION}',
                    'status_code'=>200
                ]);
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        }
    }
    
    public function getIcon($accountId,$appId)
    {
        return response()->json('okIcon');
        try{
            $appStore=AppStore::where('account_uuid',$accountId)->where('appstore_uuid',$appId)->first();
            
            return response()->json([
                    'auth_token'=> (string)Auth::getToken(),
                    'data'=>$appStore,
                    'status'=>'success',
                    'request_id'=> uniqid(),
                    'revision'=> '{REVISION}',
                    'status_code'=>200
                ]);
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        }
    }

    public function blacklisted($accountId){

        $blackListApps = Cache::rememberForever($accountId . '_blacklist_apps_store',function() use($accountId){
            $user=Auth::user();
            $jsonApps=[];
            if($user){
                
                $apps=BlackListApp::where('user_uuid',$user->user_uuid)->get();
                
                if($apps){
                    $i=0;
                    foreach ($apps as $app) {
                        $jsonApps[$i]=(string)$app->appstore_uuid;
                        $i++;
                    }
                }
            }
            return $jsonApps;
        });
        
        return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>$blackListApps,
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);
    }

    public function updateBlacklist(Request $request,$accountId){
        return response()->json('ok');
        try{

            Cache::forget($account . '_apps_link');
            Cache::forget($account . '_apps_store');
            Cache::forget($account . '_blacklist_apps_store');
            
            Cache::forget($account . '_apps_link_' . $appId);
            Cache::forget($account . '_apps_store_' . $appId);
            Cache::forget($account . '_blacklist_apps_store_' . $appId);

            return response()->json([
                    'auth_token'=> (string)Auth::getToken(),
                    'data'=>'',
                    'status'=>'success',
                    'request_id'=> uniqid(),
                    'revision'=> '{REVISION}',
                    'status_code'=>200
                ]);
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        }
    }

}

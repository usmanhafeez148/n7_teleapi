<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

use Auth;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use DB;

use App\Models\Conference;
use App\Models\User;

class ConferenceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->storage=Redis::connection();
    }

    public function index($accountId)
    {
        $conferences= Cache::rememberForever($accountId . '_conference',function() use($accountId){

            $conferences=Conference::where('domain_uuid',$accountId)->get();

            $i=0;
            $conferenceJson = array();

            foreach($conferences as $conference) {

                $numbers=explode(',', $conference->conference_numbers);
                
                $pins=explode(',', $conference->pins);

                $conferenceJson[$i]=[
                    'id'=> $conference->conference_uuid,
                    'name'=>$conference->name,
                    'owner_id'=>$conference->owner_uuid,
                    'conference_numbers'=>$numbers,
                    'is_locked'=>true,
                    'members'=>'',
                    'admins'=> 0,
                    'duration'=> 0,
                    'users'=>[],
                    'member'=>[
                        'join_deaf'=> $conference->join_deaf,
                        'join_muted'=> $conference->join_muted,
                        'numbers'=> $numbers,
                        'pins'=> $pins
                    ],

                    'moderator'=>[
                        'join_deaf'=> $conference->join_deaf,
                        'join_muted'=> $conference->join_muted,
                        'numbers'=> $numbers,
                        'pins'=> $pins
                    ],
                ];

                $i++;
            }

            return $conferenceJson;
        });

        return response()->json([
            'auth_token'=> (string)Auth::getToken(),
            'data'=>$conferences,
            'status'=>'success',
            'request_id'=> uniqid(),
            'revision'=> '{REVISION}',
            'status_code'=>200
        ]);
    }

    public function getPins($accountId){
        echo "Conference pins";
    }
    
    public function show($accountId,$conferenceId)
    {
        try {

            $conference= Cache::rememberForever($accountId . '_conference_'. $conferenceId,function() use($accountId,$conferenceId){

                $conference=Conference::where('domain_uuid',$accountId)->where('conference_uuid',$conferenceId)->first();
                
                $conferenceJson=[];
                if($conference){
                    $participants=[
                        [
                            'call_id'=> '',
                            'conference_name'=> "",
                            'conference_uuid'=> "",
                            'switch_hostname'=> "",
                            'floor'=> false,
                            'hear'=> true,
                            'speak'=> true,
                            'talking'=> false,
                            'mute_detect'=> false,
                            'participant_id'=> 1,
                            'energy_level'=> 20,
                            'current_energy'=> 0,
                            'video'=> false,
                            'is_moderator'=> false,
                            'join_time'=> 63635217275,
                            'duration'=> 10
                        ]
                    ];

                    $numbers=explode(',', $conference->conference_numbers);
                    $pins=explode(',', $conference->pins);

                    $conferenceJson['owner_id']=$conference->owner_uuid;
                    $conferenceJson['id']=$conference->conference_uuid;
                    $conferenceJson['name']=$conference->name;
                    $conferenceJson['conference_numbers']=$numbers;
                    $conferenceJson['conference_numbers_string']='Numbers';
                    $conferenceJson['is_locked']=true;
                    $conferenceJson['members']='';
                    $conferenceJson['member']=[
                        'join_deaf'=> $conference->join_deaf,
                        'join_muted'=> $conference->join_muted,
                        'numbers'=> $numbers,
                        'pins'=> $pins
                    ];
                    $conferenceJson['admins']=0;
                    $conferenceJson['duration']=0;
                    $conferenceJson['users']=0;

                    $conferenceJson['moderator']=[
                        'join_deaf'=> $conference->join_deaf,
                        'join_muted'=> $conference->join_muted,
                        'numbers'=> $numbers,
                        'pins'=> $pins
                    ];
                    $conferenceJson['play_entry_tone']=$conference->play_entry_tone;
                    $conferenceJson['play_exit_tone']=$conference->play_exit_tone;
                    $conferenceJson['play_name']="Play Name";
                    $conferenceJson['_read_only']=[
                        'members'=> 0,
                        'admins'=> 0,
                        'duration'=> 0,
                        'is_locked'=> false,
                        'participants'=> $participants
                    ];
                }
               
                return $conferenceJson;
            });

            return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>$conference,
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);
            
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed',
            ));
        } catch(\Exception $e){
           return response()->json(array(
            'error' => $e->getMessage(),
            'status' => 'failed',
        ));
       }
    }


    public function getStatus($accountId,$conferenceId){
        echo "status";
    }

    public function store(Request $request,$accountId)
    {   
        
        try {
            $conference=new Conference();
            $conference->conference_uuid=(string) Uuid::generate();
            $conference->domain_uuid=$accountId;

            $conference->name=$request->data['name'];
            
            if(isset($request->data['owner_id'])){
                $conference->owner_uuid=$request->data['owner_id'];
                $user=User::find($conference->owner_uuid);
                if($user){
                    $user->features= $user->features . "conferencing,";
                    $user->save();
                    Cache::forget($accountId . '_user');
                    Cache::forget($accountId . '_user_' . $user->user_uuid);
                }
            }
            
            if(isset($request->data['conference_numbers'])){
                $numbers="";
                foreach ($request->data['conference_numbers'] as $number) {
                    $numbers=$numbers . $number . ',';
                }
                $conference->conference_numbers=$numbers;
            }
            
            
            if(isset($request->data['member'])){
                if(isset($request->data['member']['pins'])){
                    $pins="";

                    foreach ($request->data['member']['pins'] as $pin) {
                        $pins=$pins . $pin . ',';
                    }
                    $conference->pins=$pin;
                }

                if(isset($request->data['member']['join_muted'])){
                    $conference->join_muted=$request->data['member']['join_muted'];
                }

                if(isset($request->data['member']['join_deaf'])){
                    $conference->join_deaf=$request->data['member']['join_deaf'];
                }
            
            }
            
            if(isset($request->data['play_entry_tone'])){
                $conference->play_entry_tone=$request->data['play_entry_tone'];
            }

            if(isset($request->data['play_exit_tone'])){
                $conference->play_exit_tone=$request->data['play_exit_tone'];
            }
            
            $conference->save();

            Cache::forget($accountId . '_conference');

            return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>array_merge(['id'=>$conference->conference_uuid],$request->data),
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);
            
        } catch (\Illuminate\Database\QueryException $e) {
           return response()->json(array(
            'error' => $e->getMessage(),
            'status' => 'failed',
        ));
       } catch(\Exception $e){
           return response()->json(array(
            'error' => $e->getMessage(),
            'status' => 'failed',
        ));
       }
    }

    public function putAction(Request $request,$accountId,$conferenceId){
        echo "put actions";
    }

    public function postAction(Request $request,$accountId,$conferenceId,$action){
        echo "put actions";
    }
    public function update(Request $request, $accountId,$conferenceId)
    {
        try {
            $conference=Conference::find($conferenceId);
            $conference->name=$request->data['name'];
            
            if(isset($request->data['owner_id'])){
                $conference->owner_uuid=$request->data['owner_id'];
                $user=User::find($conference->owner_uuid);
                if($user){
                    $user->features= $user->features . "conferencing,";
                    $user->save();
                    Cache::forget($accountId . '_user');
                    Cache::forget($accountId . '_user_' . $user->user_uuid);
                }
            }
            
            if(isset($request->data['conference_numbers'])){
                $numbers="";
                foreach ($request->data['conference_numbers'] as $number) {
                    $numbers=$numbers . $number . ',';
                }
                $conference->conference_numbers=$numbers;
            }
            
            
            if(isset($request->data['member'])){
                if(isset($request->data['member']['pins'])){
                    $pins="";

                    foreach ($request->data['member']['pins'] as $pin) {
                        $pins=$pins . $pin . ',';
                    }
                    $conference->pins=$pin;
                }

                if(isset($request->data['member']['join_muted'])){
                    $conference->join_muted=$request->data['member']['join_muted'];
                }

                if(isset($request->data['member']['join_deaf'])){
                    $conference->join_deaf=$request->data['member']['join_deaf'];
                }
            
            }
            
            if(isset($request->data['play_entry_tone'])){
                $conference->play_entry_tone=$request->data['play_entry_tone'];
            }

            if(isset($request->data['play_exit_tone'])){
                $conference->play_exit_tone=$request->data['play_exit_tone'];
            }
            
            $conference->save();
            
            Cache::forget($accountId . '_conference_'. $conferenceId);
            Cache::forget($accountId . '_conference');
            

            return response()->json([
                'auth_token'=> (string)Auth::getToken(),
                'data'=>array_merge(['id'=>$conference->conference_uuid],$request->data),
                'status'=>'success',
                'request_id'=> uniqid(),
                'revision'=> '{REVISION}',
                'status_code'=>200
            ]);
            
        } catch (\Illuminate\Database\QueryException $e) {
           return response()->json(array(
            'error' => $e->getMessage(),
            'status' => 'failed',
        ));
       } catch(\Exception $e){
           return response()->json(array(
            'error' => $e->getMessage(),
            'status' => 'failed',
        ));
       }
    }

    
    public function destroy($accountId,$conferenceId)
    {
        if($conference=Conference::find($conferenceId)){
            if(Conference::find($conferenceId)->delete()){

                Cache::forget($accountId . '_conference_'. $conferenceId);
                Cache::forget($accountId . '_conference');
                
                return response()->json([
                    'auth_token'=> (string)Auth::getToken(),
                    'data'=>'',
                    'status'=>'success',
                    'request_id'=> uniqid(),
                    'revision'=> '{REVISION}',
                    'status_code'=>200
                ]);
            }
        }
        
    }

    public function listServers($accountId){
        echo "listServers";
    }

    public function getServer($accountId,$serverId){
        echo "getServer";
    }

    public function createServer(Request $request,$accountId){
        echo "createServer";
    }

    public function updateServer(Request $request,$accountId){
        echo "updateServer";
    }

    public function deleteServer($accountId,$serverId){
        echo "deleteServer";
    }

    public function addParticipant(Request $request,$accountId,$conferenceId){
        echo "addParticipants";
    }

    public function muteParticipant(Request $request,$accountId,$conferenceId,$participantId){
        echo "muteParticipant";
    }

    public function unmuteParticipant(Request $request,$accountId,$conferenceId,$participantId){
        echo "unmuteParticipant";
    }

    public function deafParticipant(Request $request,$accountId,$conferenceId,$participantId){
        echo "deafParticipant";
    }

    public function undeafParticipant(Request $request,$accountId,$conferenceId,$participantId){
        echo "undeafParticipant";
    }

    public function kickParticipant(Request $request,$accountId,$conferenceId,$participantId){
        echo "kickParticipant";
    }

    public function createNotification(Request $request,$accountId,$notificationType){
        echo "createNotification";
    }

    public function updateNotification(Request $request,$accountId,$notificationType){
        echo "updateNotification";
    }
}

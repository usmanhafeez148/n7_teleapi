<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

use Auth;
use Illuminate\Http\Request;
use App\Models\Account;
use App\Models\Limit;
use Webpatser\Uuid\Uuid;
use DB;

class PortController extends Controller
{
    
    public function __construct()
    {
        $this->storage=Redis::connection();
    }

    public function index($accountId)
    {
      
    }

    public function listDescendants($accountId){

    }


    public function show($accountId,$portRequestId)
    {
       
    }
    

    public function store(Request $request,$accountId)
    {
        
    }

    
    public function update(Request $request, $accountId,$portRequestId)
    {
        
    }

    
    public function destroy($accountId,$portRequestId)
    {

    }

    public function listAttachments($accountId,$portRequestId){

    }

    public function getAttachment($accountId,$portRequestId,$documentName){

    }

    public function createAttachment(Request $request,$accountId,$portRequestId){
        // ?filename={documentName}
    }

    public function updateAttachment(Request $request,$accountId,$portRequestId,$documentName){

    }

    public function deleteAttachment($accountId,$portRequestId,$documentName){

    }

    public function changeState($accountId,$portRequestId,$state){

    }
}

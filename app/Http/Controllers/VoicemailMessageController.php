<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

use Auth;
use Illuminate\Http\Request;
use App\Models\VoicemailMessage;
use Webpatser\Uuid\Uuid;
use DB;

class VoicemailMessageController extends Controller
{
    
    public function __construct(){
        $this->storage=Redis::connection();
    }

    public function index($accountId)
    {
        $voicemailMessages= Cache::rememberForever($accountId . '_voicemail_message',function() use($accountId) {
            $voicemailMessages = VoicemailMessage::all();

            $i=0; 
            $jsonMessages = array();
            foreach($voicemailMessages as $message) {

                $jsonMessages[$i]=[
                    'id'=> $message->voicemail_message_uuid,
                ];
                $i++;
            }
            return $jsonMessages;
        });

        return response()->json([
            'auth_token'=> (string)Auth::getToken(),
            'data'=>$voicemailMessages,
            'status'=>'success',
            'request_id'=> uniqid(),
            'revision'=> '{REVISION}',
            'status_code'=>200
        ]);
    }

    

    public function show($accountId,$voicemessageId)
    {
    }

    
    public function store(Request $request,$accountId)
    {
    }

    
    public function update(Request $request, $accountId,$voicemailmessageId)
    {
    }

    
    public function destroy($accountId,$voicemessageId)
    {
        if($voicemailMessage=VoicemailMessage::find($voicemessageId)){
            if(VoicemailMessage::find($voicemessageId)->delete()){
                Cache::forget($accountId . '_voicemail_message');
                Cache::forget($accountId . '_voicemail_message_' . $voicemessageId);
                return response()->json([
                    'auth_token'=> (string)Auth::getToken(),
                    'data'=>'',
                    'status'=>'success',
                    'request_id'=> uniqid(),
                    'revision'=> '{REVISION}',
                    'status_code'=>200
                ]);
            }
        }
    }

     public function listMessages($accountId,$voicemailId){

        $voiceMessages= Cache::rememberForever($accountId . '_' . $voicemailId . '_voicemail_message',function() use($accountId,$voicemailId) {
            
            $voiceMessages=DB::table('n7c_ca_voicemail_message')
            ->join('n7c_ca_voicemail_users', 'n7c_ca_voicemail_users.vmbox_uuid', '=', 'n7c_ca_voicemail_message.owner_uuid', 'left outer')
            ->where('n7c_ca_voicemail_message.owner_uuid', '=', $voicemailId)
            ->get();

            $i=0;
            $jsonMessages = array();
            foreach($voiceMessages as $message) {
                
                $jsonMessages[$i]=[
                    'call_id'=> $message->voicemail_message_uuid,
                    'caller_id_name'=>'Usman',
                    "caller_id_number"=> "1001",
                    'timestamp'=>$message->timestamp,
                    'from'=>$message->from,
                    'to'=>$message->to,
                    'length'=>$message->length,
                    'folder'=> 'new',    
                    'media_id'=> 'media_id'
                ];

                $i++;
            }
            return $jsonMessages;
        });

        
        return response()->json([
            'auth_token'=> (string)Auth::getToken(),
            'data'=>$voiceMessages,
            'status'=>'success',
            'request_id'=> uniqid(),
            'revision'=> '{REVISION}',
            'status_code'=>200
        ]);
    }

    
    public function deleteMessages($accountId,$voicemailId){
        try {

            $delete=VoicemailMessage::where('domain_uuid',$accountId)->where('owner_uuid',$voicemailId)->get();

            if($delete->delete()){
                Cache::forget($accountId . '_' . $voicemailId . '_voicemail_message');
                
                return response()->json([
                    'auth_token'=> (string)Auth::getToken(),
                    'data'=>$delete,
                    'status'=>'success',
                    'request_id'=> uniqid(),
                    'revision'=> '{REVISION}',
                    'status_code'=>200
                ]);
            }
            
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        } catch(\Exception $e){
            return response()->json(array(
                'error' => $e->getMessage(),
                'status' => 'failed'
            ));
        }
    }

    public function updateMessages(Request $request,$accountId,$voicemailId){
    }
}

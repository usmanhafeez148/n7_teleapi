<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

use Auth;
use Illuminate\Http\Request;

use App\Models\BlackList;
use App\Models\CreditManagement;
use Webpatser\Uuid\Uuid;
use DB;

class BalanceController extends Controller
{
    
    public function __construct(){
        $this->storage=Redis::connection();
    }

    public function currentBalance($accountId){
        $balance=CreditManagement::find($accountId);
        
        $balanceJson=[];
        if($balance){
            $balanceJson=[
                'balance'=>$balance->balance_credit
            ];
        }
        return response()->json([
                    'auth_token'=> (string)Auth::getToken(),
                    'data'=>$balanceJson,
                    'status'=>'success',
                    'request_id'=> uniqid(),
                    'revision'=> '{REVISION}',
                    'status_code'=>200
                ]);
    }

    public function Credits($accountId){
        return response()->json([
                    'auth_token'=> (string)Auth::getToken(),
                    'data'=>['balance'=>0],
                    'status'=>'success',
                    'request_id'=> uniqid(),
                    'revision'=> '{REVISION}',
                    'status_code'=>200
                ]);
    }
}

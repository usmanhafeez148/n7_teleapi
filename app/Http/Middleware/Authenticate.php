<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;
use App\Models\User;
use Tymon\JWTAuth\Facades\JWTAuth;
class Authenticate
{
    /**
     * The authentication guard factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory  $auth
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    
    // public function handle($request, Closure $next, $guard = null)
    public function handle($request, Closure $next)
    {   
        if($request->header('X-Auth-Token')){
            $actual_token = $request->header('X-Auth-Token');
            $request->headers->set('Authorization','Bearer '.$actual_token);
            return $next($request);
        
        }elseif($request->input('auth_token')){
            $t=$request->auth_token;
            $request->headers->set('Authorization','Bearer '.$t);
            return $next($request);
        }elseif(isset($request->input('data')['token'])){
            $token=$request->data['token'];
            if($token){
                $request->headers->set('Authorization','Bearer '.$token);
                return $next($request);
            }
        }
        
        return response()->json('unAuthenticated');
    }
}

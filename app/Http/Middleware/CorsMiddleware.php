<?php 
namespace App\Http\Middleware;

class CorsMiddleware {

  public function handle($request, \Closure $next)
  {
  	header('Accept: application/json');
  	header('Access-Control-Allow-Origin','*');
    header('Access-Control-Allow-Credentials',false);
    header('Access-Control-Expose-Headers','*');
    header('Access-Control-Allow-Methods','*');
    header('Access-Control-Allow-Headers','*');
    header('Access-Control-Max-Age',0);
    
    // $request->add(['request_id' => uniqid()]);

    $response = $next($request);

    $response->header('Access-Control-Allow-Methods', 'GET,HEAD,PUT,POST,DELETE,PATCH,OPTIONS');
    $response->header('Access-Control-Allow-Origin','*');
    $response->header('Access-Control-Allow-Headers', $request->header('Access-Control-Request-Headers'));
    $response->header('Access-Control-Allow-Origin', '*');

    return $response;
  }
}
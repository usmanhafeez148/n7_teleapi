<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

class PackagePolicy extends Model
{
	/* configurations*/
	// setting primary_key
	protected $primaryKey = null;
	// setting primary_key behaviour
	public $incrementing = false;
	// setting table_name
	protected $table="n7c_ca_usage_policy";
	
	// enable/disable timestamps fields
	public $timestamps=false;

	// hidden fields
	// protected $hidden = ['domain_uuid'];
	
	// what does maps do
	// protected $maps = ['id' => 'domain_uuid'];
	
	// setting fields which to be fillable while updating and creating
	// protected $fillable=['account_name'];
	
	//  setting fields which to be showable/visible while listing and getting
	// protected $visible = ['name'];
	
	// Attribute Type Casting
	// protected $casts = [ 'is_admin' => 'boolean',];
	
	// appending custom Attributes/Fields with Model #Accessor is nessessory_method for this implementation
	// protected $appends = ['account_id'];
	
	/*public function getAccountIdAttribute()
	{
	    return $this->attributes['domain_uuid'];
	}*/

	/**************************/
	// matutors=> relational functions with other models
}
<?php

/*===================================Test=============================*/
// use Webpatser\Uuid\Uuid;
// use Illuminate\Support\Facades\Cache;
// use Illuminate\Support\Facades\Redis;
// use Illuminate\Support\Facades\Hash;
// use Illuminate\Support\Facades\Auth;
// use App\Models\User;

$router->get('/',function(){
	echo "Telephone Api";
	
});
$router->get('/test',['middleware'=>['api','auth:api'],function(){
	dd(Auth::user());
echo Auth::getToken();
}]);

$router->get('/accounts/{accountId}/clear/{appId}',function($accountId,$appId){
	Cache::forget($accountId . '_apps_store');
    Cache::forget($accountId . '_apps_store_' . $appId);
    echo "Apps cache is cleared";
});

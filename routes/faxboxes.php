<?php 
$router->group(['middleware' => ['api','auth:api']], function () use ($router) {
	// faxbox.list
	$router->get('/accounts/{accountId}/faxboxes','FaxBoxController@index');
	// faxbox.get
	$router->get('/accounts/{accountId}/faxboxes/{faxboxId}','FaxBoxController@show');
	// faxbox.create
	$router->put('/accounts/{accountId}/faxboxes','FaxBoxController@store');
	// faxbox.update
	$router->post('/accounts/{accountId}/faxboxes/{faxboxId}','FaxBoxController@update');
	// faxbox.delete
	$router->delete('/accounts/{accountId}/faxboxes/{faxboxId}','FaxBoxController@destroy');
	// faxes.listInbound
	$router->get('/accounts/{accountId}/inbound-faxes','FaxBoxController@inboundFaxes');
	// faxes.listOutbound
	$router->get('/accounts/{accountId}/outbound-faxes','FaxBoxController@outboundFaxes');
});
<?php 
$router->group(['middleware' => ['api','auth:api']], function () use ($router) {
	// group.list
	$router->get('/accounts/{accountId}/groups','GroupController@index');
	// group.get
	$router->get('/accounts/{accountId}/groups/{groupId}','GroupController@show');
	// group.create
	$router->put('/accounts/{accountId}/groups','GroupController@store');
	// group.update
	$router->post('/accounts/{accountId}/groups/{groupId}','GroupController@update');
	// group.delete
	$router->delete('/accounts/{accountId}/groups/{groupId}','GroupController@destroy');
});
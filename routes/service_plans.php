<?php 
$router->group(['middleware' => ['api','auth:api']], function () use ($router) {
	// servicePlan.list
	$router->get('/accounts/{accountId}/service_plans','ServicesPlanController@index');
	$router->post('/accounts/{accountId}/service_plans/override', 'ServicesPlanController@override');
	
	// servicePlan.listAvailable
	$router->get('/accounts/{accountId}/service_plans/available/{planId}','ServicesPlanController@available');
	
	// servicePlan.listCurrent
	$router->get('/accounts/{accountId}/service_plans/current','ServicesPlanController@current');

	// servicePlan.get
	$router->get('/accounts/{accountId}/service_plans/{planId}','ServicesPlanController@show');
	// servicePlan.getAvailable
	// 'getAvailable': { verb: 'GET', url: 'accounts/{accountId}/service_plans/available/{planId}' }

	// servicePlan.add
	$router->post('/accounts/{accountId}/service_plans/{planId}','ServicesPlanController@store');
	// 'addMany': { verb: 'POST', url: 'accounts/{accountId}/service_plans/' },
	
	// 'update': { verb: 'POST', url: 'accounts/{accountId}/service_plans/' },

	// servicePlan.remove
	$router->delete('/accounts/{accountId}/service_plans/{planId}','ServicesPlanController@destroy');
	// 'removeMany': { verb: 'DELETE', url: 'accounts/{accountId}/service_plans/' },
	
	// servicePlan.reconciliate
	$router->post('/accounts/{accountId}/service_plans/reconciliation','ServicesPlanController@reconciliate');

	// servicePlan.synchronize
	$router->post('/accounts/{accountId}/service_plans/synchronization','ServicesPlanController@synchronize');
});

/*

	'addManyOverrides': { verb: 'POST', url: 'accounts/{accountId}/service_plans/override' },
	
	'getCsv': { verb: 'GET', url: 'accounts/{accountId}/service_plans/current?depth=4&identifier=items&accept=csv' },
					
 */
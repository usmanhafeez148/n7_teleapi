<?php 
$router->group(['middleware' => ['api','auth:api']], function () use ($router) {
	// Conference.list
	$router->get('/accounts/{accountId}/conferences','ConferenceController@index');
	// conference.getPins
	$router->get('/accounts/{accountId}/conferences/pins','ConferenceController@getPins');
	
	// conference.status
	$router->get('/accounts/{accountId}/conferences/{conferenceId}/status','ConferenceController@getStatus');
	// Conference.get
	$router->get('/accounts/{accountId}/conferences/{conferenceId}','ConferenceController@show');
	
	// Conference.create
	$router->put('/accounts/{accountId}/conferences','ConferenceController@store');
	// conference.action
	$router->put('/accounts/{accountId}/conferences/{conferenceId}','ConferenceController@putAction');
	
	// Conference.update
	$router->post('/accounts/{accountId}/conferences/{conferenceId}','ConferenceController@update');
	// Conference.delete
	$router->delete('/accounts/{accountId}/conferences/{conferenceId}','ConferenceController@destroy');

	/*==========================Conference Participants==============================*/

	// conference.participantsList
	$router->get('accounts/{accountId}/conferences/{conferenceId}/participants',function(){
		return response()->json('conference.participantsList');
	});


	// conference.participantsBulkAction
	 $router->put('accounts/{accountId}/conferences/{conferenceId}/participants',function(){
	 	return response()->json('conference.participantsBulkAction');
	 });

	// conference.participantsAction
	 $router->put('accounts/{accountId}/conferences/{conferenceId}/participants/{participantId}',function(){
	 	return response()->json('conference.participantsAction');
	 });

	// conference.addParticipant
	$router->post('/accounts/{accountId}/conferences/{conferenceId}/add_participant','ConferenceController@addParticipant');

	// conference.muteParticipant
	$router->post('/accounts/{accountId}/conferences/{conferenceId}/mute/{participantId}','ConferenceController@muteParticipant');
	// conference.unmuteParticipant
	$router->post('/accounts/{accountId}/conferences/{conferenceId}/unmute/{participantId}','ConferenceController@unmuteParticipant');
	// conference.deafParticipant
	$router->post('/accounts/{accountId}/conferences/{conferenceId}/deaf/{participantId}','ConferenceController@deafParticipant');
	// conference.undeafParticipant
	$router->post('/accounts/{accountId}/conferences/{conferenceId}/undeaf/{participantId}','ConferenceController@undeafParticipant');

	// conference.kickParticipant
	$router->post('/accounts/{accountId}/conferences/{conferenceId}/kick/{participantId}','ConferenceController@kickParticipant');

	/*=================================Conference Servers=======================*/

	// conference.listServers
	$router->get('/accounts/{accountId}/conferences_servers','ConferenceController@listServers');
	// conference.getServer
	$router->get('/accounts/{accountId}/conferences_servers/{serverId}','ConferenceController@getServer');
	// conference.createServer
	$router->put('/accounts/{accountId}/conferences_servers','ConferenceController@createServer');
	// conference.updateServer
	$router->post('/accounts/{accountId}/conferences_servers/{serverId}','ConferenceController@updateServer');
	// conference.deleteServer
	$router->delete('/accounts/{accountId}/conferences_servers/{serverId}','ConferenceController@deleteServer');

	
	/*================================Conference Notifications======================*/
	// conference.getNotification
	/*
	// conference.createNotification
	$router->put('/accounts/{accountId}/notify/conference_{notificationType}','ConferenceController@createNotification');
	// conference.updateNotification
	$router->post('/accounts/{accountId}/notify/conference_{notificationType}','ConferenceController@updateNotification');*/
});
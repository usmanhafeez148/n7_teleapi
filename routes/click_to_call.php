<?php 
$router->group(['middleware' => ['api','auth:api']], function () use ($router) {
	// clicktocall.create
	$router->put('accounts/{accountId}/clicktocall',function(){
		
	});
	// clicktocall.get
	$router->get('accounts/{accountId}/clicktocall/{clickToCallId}',function(){
		
	});
	// clicktocall.update
	// $router->get('accounts/{accountId}/clicktocall/{clickToCallId}');
	// clicktocall.delete
	$router->delete('accounts/{accountId}/clicktocall/{clickToCallId}',function(){
		
	});
	// clicktocall.list
	$router->get('accounts/{accountId}/clicktocall',function(){
		
	});
	// clicktocall.connect
	$router->post('accounts/{accountId}/clicktocall/{clickToCallId}/connect',function(){
		
	});
	
});
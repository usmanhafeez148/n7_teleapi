<?php 
$router->group(['middleware' => ['api','auth:api']], function () use ($router) {
	/*=================================Voicemails===============================*/
	// voicemail.list
	$router->get('/accounts/{accountId}/vmboxes','VoicemailUsersController@index');
	// voicemail.get
	$router->get('/accounts/{accountId}/vmboxes/{voicemailId}','VoicemailUsersController@show');
	// voicemail.create
	$router->put('/accounts/{accountId}/vmboxes','VoicemailUsersController@store');
	// voicemail.update
	$router->post('/accounts/{accountId}/vmboxes/{voicemailId}','VoicemailUsersController@update');
	// voicemail.delete
	$router->delete('/accounts/{accountId}/vmboxes/{voicemailId}','VoicemailUsersController@destroy');

	//voicemail.listMessages
	$router->get('/accounts/{accountId}/vmboxes/{voicemailId}/messages','VoicemailMessageController@listMessages');
	//voicemail.deleteMessages
	$router->delete('/accounts/{accountId}/vmboxes/{voicemailId}/messages','VoicemailMessageController@deleteMessages');
	// voicemail.updateMessages
	$router->post('accounts/{accountId}/vmboxes/{voicemailId}/messages','VoicemailMessageController@updateMessages');
});
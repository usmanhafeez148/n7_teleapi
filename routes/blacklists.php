<?php 
$router->group(['middleware' => ['api','auth:api']], function () use ($router) {
	// blacklist.list
	$router->get('/accounts/{accountId}/blacklists','BlackListController@index');

	// blacklist.get
	$router->get('/accounts/{accountId}/blacklists/{blacklistId}','BlackListController@show');
	// blacklist.create
	$router->put('/accounts/{accountId}/blacklists','BlackListController@store');
	// blacklist.update
	$router->post('/accounts/{accountId}/blacklists/{blacklistId}','BlackListController@update');
	// blacklist.delete
	$router->delete('/accounts/{accountId}/blacklists/{blacklistId}','BlackListController@destroy');
});
<?php 
$router->group(['middleware' => ['api','auth:api']], function () use ($router) {
	//menu.list
	$router->get('/accounts/{accountId}/menus','MenuController@index');
	//menu.get
	$router->get('/accounts/{accountId}/menus/{menuId}','MenuController@show');
	//menu.create
	$router->put('/accounts/{accountId}/menus','MenuController@create');
	//menu.delete
	$router->delete('/accounts/{accountId}/menus/{menuId}','MenuController@delete');
	//menu.update
	$router->post('/accounts/{accountId}/menus/{menuId}','MenuController@update');
});
<?php 

/*==============================Authentication========================*/
// user.auth_create
$router->put('/user_auth','Auth\LoginController@login');

// Get Token Information

// auth.getTokenInfo
$router->get('/auth/tokeninfo','Auth\LoginController@getTokenInfo');
// auth.postTokenInfo

$router->post('/auth/tokeninfo',['middleware'=>['api','auth:api'],'uses'=>'Auth\LoginController@postTokenInfo']);

// auth.recovery
$router->put('/user_auth/recovery','Auth\LoginController@authRecovery');

// auth.recoveryResetId
$router->post('/user_auth/recovery','Auth\LoginController@execAuthRecovery');

/*
// fetchAuthToken
$router->get('/user_auth/{authToken}','Auth\LoginController@fetchAuthToken');


// auth.impersonate
// $router->put('/accounts/{accountId}/users/{userId}/user_auth','Auth\LoginController@impersonate');
*/
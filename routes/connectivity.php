<?php 
$router->group(['middleware' => ['api','auth:api']], function () use ($router) {
	// connectivity.list
	$router->get('/accounts/{accountId}/connectivity','ConnectivityController@index');
	// connectivity.get
	$router->get('/accounts/{accountId}/connectivity/{connectivityId}','ConnectivityController@show');
	// connectivity.create
	$router->put('/accounts/{accountId}/connectivity','ConnectivityController@store');
	// connectivity.update
	$router->post('/accounts/{accountId}/connectivity/{connectivityId}','ConnectivityController@update');
	// connectivity.delete
	$router->delete('/accounts/{accountId}/connectivity/{connectivityId}','ConnectivityController@destroy');
});
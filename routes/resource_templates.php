<?php 
$router->group(['middleware' => ['api','auth:api']], function () use ($router) {
	//resourceTemplates.list
	$router->get('/accounts/{accountId}/resource_templates','ResourceTemplateController@index');
	//resourceTemplates.get
	$router->get('/accounts/{accountId}/resource_templates/{resourceId}','ResourceTemplateController@show');
	//resourceTemplates.create
	$router->put('/accounts/{accountId}/resource_templates','ResourceTemplateController@store');
	//resourceTemplates.update
	$router->post('/accounts/{accountId}/resource_templates/{resourceId}','ResourceTemplateController@update');
	//resourceTemplates.delete
	$router->delete('/accounts/{accountId}/resource_templates/{resourceId}','ResourceTemplateController@destroy');
});
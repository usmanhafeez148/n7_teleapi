<?php 
$router->group(['middleware' => ['api','auth:api']], function () use ($router) {
	// account.list
	$router->get('/accounts','AccountController@index');
	
	// account.get
	$router->get('/accounts/{accountId}','AccountController@show');
	// account.create
	$router->put('/accounts/{accountId}','AccountController@store');
	// account.update
	$router->post('/accounts/{accountId}','AccountController@update');
	// account.delete
	$router->delete('/accounts/{accountId}','AccountController@destroy');
	// account.listDescendants
	$router->get('/accounts/{accountId}/descendants','AccountController@listDescendants');
	// account.listParents
	$router->get('/accounts/{accountId}/tree','AccountController@listParents');
	// account.listChildren
	$router->get('/accounts/{accountId}/children','AccountController@listChild');
	
});
<?php 
$router->group(['middleware' => ['api','auth:api']], function () use ($router) {
	/*=================================TemporalRules============================*/

	// temporalRule.list
	$router->get('/accounts/{accountId}/temporal_rules','TimeConditionController@index');
	// temporalRule.get
	$router->get('/accounts/{accountId}/temporal_rules/{ruleId}','TimeConditionController@show');
	// temporalRule.create
	$router->put('/accounts/{accountId}/temporal_rules','TimeConditionController@store');
	// temporalRule.update
	$router->post('/accounts/{accountId}/temporal_rules/{ruleId}','TimeConditionController@update');
	// temporalRule.delete
	$router->delete('/accounts/{accountId}/temporal_rules/{ruleId}','TimeConditionController@destroy');

	/*=================================TemporalSetRules=========================*/

	// temporalSetRule.list
	$router->get('/accounts/{accountId}/temporal_rules_sets','TimeConditionSetController@index');
	// temporalRule.get
	$router->get('/accounts/{accountId}/temporal_rules_sets/{ruleId}','TimeConditionSetController@show');
	// temporalRule.create
	$router->put('/accounts/{accountId}/temporal_rules_sets','TimeConditionSetController@store');
	// temporalRule.update
	$router->post('/accounts/{accountId}/temporal_rules_sets/{ruleId}','TimeConditionSetController@update');
	// temporalRule.delete
	$router->delete('/accounts/{accountId}/temporal_rules_sets/{ruleId}','TimeConditionSetController@destroy');
});
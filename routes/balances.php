<?php 
$router->group(['middleware' => ['api','auth:api']], function () use ($router) {
	// balance.get
	$router->get('/accounts/{accountId}/transactions/current_balance','BalanceController@currentBalance');

	// balance.getMonthly
	$router->get('/accounts/{accountId}/transactions/monthly_recurring',function(){
		// created_from={from}&created_to={to}
	});
	
	// balance.getSubscriptions
	$router->get('/accounts/{accountId}/transactions/subscriptions',function(){

	});
	
	// balance.getCharges
	$router->get('/accounts/{accountId}/transactions',function(){
		// created_from={from}&created_to={to}&reason={reason}
	});
	
	// balance.add
	$router->put('/accounts/{accountId}/braintree/credits','BalanceController@Credits');
	
	// balance.remove
	$router->delete('/accounts/{accountId}/transactions/debit',function(){

	});
});
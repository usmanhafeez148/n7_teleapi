<?php 
$router->group(['middleware' => ['api','auth:api']], function () use ($router) {
	//ports
	//port.list
	$router->get('/accounts/{accountId}/port_requests','PortController@index');
	//port.listDescendants
	$router->get('/accounts/{accountId}/port_requests/descendants','PortController@listDescendants');
	// port.get
	$router->get('/accounts/{accountId}/port_requests/{portRequestId}','PortController@show');
	//port.create
	$router->put('/accounts/{accountId}/port_requests','PortController@store');
	//port.update
	$router->post('/accounts/{accountId}/port_requests/{portRequestId}','PortController@update');
	//port.delete
	$router->delete('/accounts/{accountId}/port_requests/{portRequestId}','PortController@destroy');

	//Port Attatchments
	//port.listAttachments
	$router->get('/accounts/{accountId}/port_requests/{portRequestId}/attachments','PortController@listAttachments');
	//port.getAttachment
	$router->get('/accounts/{accountId}/port_requests/{portRequestId}/attachments/{documentName}','PortController@getAttachment');
	//port.createAttachment
	$router->put('/accounts/{accountId}/port_requests/{portRequestId}/attachments','PortController@createAttachment');
	//port.updateAttachment
	$router->post('/accounts/{accountId}/port_requests/{portRequestId}/attachments/{documentName}','PortController@updateAttachment');
	//port.deleteAttachment
	$router->delete('/accounts/{accountId}/port_requests/{portRequestId}/attachments/{documentName}','PortController@deleteAttachment');
	//port.changeState
	$router->patch('/accounts/{accountId}/port_requests/{portRequestId}/{state}','PortController@changeState');
});
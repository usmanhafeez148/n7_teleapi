<?php 
$router->group(['middleware' => ['api','auth:api']], function () use ($router) {
	// media.list
	$router->get('/accounts/{accountId}/media','MediaController@index');
	// media.get
	$router->get('/accounts/{accountId}/media/{mediaId}','MediaController@show');
	// media.create
	$router->put('/accounts/{accountId}/media','MediaController@store');
	// media.update
	$router->post('/accounts/{accountId}/media/{mediaId}','MediaController@update');
	// media.upload
	$router->post('/accounts/{accountId}/media/{mediaId}/raw','MediaController@upload');
	// media.delete
	$router->delete('/accounts/{accountId}/media/{mediaId}','MediaController@destroy');
});
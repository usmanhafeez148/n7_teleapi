<?php 
$router->group(['middleware' => ['api','auth:api']], function () use ($router) {
	// cdrs.list
	$router->get('/accounts/{accountId}/cdrs','CallLogController@index');
	// cdrs.listByInteraction
	$router->get('/accounts/{accountId}/cdrs/interaction','CallLogController@interaction');
	// cdrs.get
	$router->get('/accounts/{accountId}/cdrs/legs/{cdrId}','CallLogController@show');

	// Channel
	// channel.list
	$router->get('/accounts/{accountId}/channels',function(){

	});
});
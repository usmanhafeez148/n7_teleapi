<?php 
$router->group(['middleware' => ['api','auth:api']], function () use ($router) {
	// limits.get
	$router->get('/accounts/{accountId}/limits','LimitController@getLimit');
	// limits.update
	$router->post('/accounts/{accountId}/limits','LimitController@updateLimit');
});
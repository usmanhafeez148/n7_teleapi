<?php 
$router->group(['middleware' => ['api','auth:api']], function () use ($router) {
	// channels.create
	$router->put('/accounts/{accountId}/channels/{channelId}','ChannelController@store');
	// channels.update
	$router->post('/accounts/{accountId}/channels/{channelId}','ChannelController@update');
	// channels.listByAccountAndDevice
	$router->get('/accounts/{accountId}/devices/{deviceId}/channels','ChannelController@listByDevice');
	// channels.listByAccountAndUser
	$router->get('/accounts/{accountId}/users/{userId}/channels','ChannelController@listByUser');
	// channels.listByAccount
	$router->get('/accounts/{accountId}/channels','ChannelController@listByAccount');
	// channels.get
	$router->get('/accounts/{accountId}/channels/{channelId}','ChannelController@show');
	// channels.list
	$router->get('/channels','ChannelController@index');
	
});
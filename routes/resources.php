<?php 
$router->group(['middleware' => ['api','auth:api']], function () use ($router) {
	/*=========================Global Resources========================*/
	// globalResources.list
	$router->get('/resources','ResourceController@globalIndex');
	// globalResources.get
	$router->get('/resources/{resourceId}','ResourceController@globalShow');
	// globalResources.create
	$router->put('/resources','ResourceController@globalStore');
	// globalResources.update
	$router->post('/resources/{resourceId}','ResourceController@globalUpdate');
	// globalResources.delete
	$router->delete('/globalResources/{resourceId}','ResourceController@globalDestroy');
	//Jobs
	// globalResources.listJobs 
	// $router->get('/resources/jobs','ResourceController@globalJobList');
	// globalResources.getJob
	$router->get('/resources/jobs/{jobId}','ResourceController@globalJobGet');
	// globalResources.createJob
	$router->put('/resources/jobs','ResourceController@globalJobCreate');
	//Collection
	//globalResources.updateCollection 
	// $router->post('/resources/collection','ResourceController@globalUpdateCollection');


	/*=========================Local Resources========================*/
	// localResources.list
	$router->get('/accounts/{accountId}/resources','ResourceController@localIndex');
	// localResources.get
	$router->get('/accounts/{accountId}/resources/{resourceId}','ResourceController@localShow');
	// localResources.create
	$router->put('/accounts/{accountId}/resources','ResourceController@localStore');
	// globalResources.update
	$router->post('/accounts/{accountId}/resources/{resourceId}','ResourceController@localUpdate');
	// localResources.delete
	$router->delete('/accounts/{accountId}/resources/{resourceId}','ResourceController@localDestroy');
	//Jobs
	// localResources.listJobs 
	$router->get('/accounts/{accountId}/resources/jobs','ResourceController@localJobList');
	// localResources.getJob
	$router->get('/accounts/{accountId}/resources/jobs/{jobId}','ResourceController@localJobGet');
	// localResources.createJob
	$router->put('/accounts/{accountId}/resources/jobs','ResourceController@localJobCreate');
	//Collections
	//localResources.updateCollection 
	$router->post('/accounts/{accountId}/resources/collection','ResourceController@localUpdateCollection');
});
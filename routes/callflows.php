<?php 
$router->group(['middleware' => ['api','auth:api']], function () use ($router) {
	// callflow.list
	$router->get('/accounts/{accountId}/callflows','CallFlowController@index');
	// callflow.get
	$router->get('/accounts/{accountId}/callflows/{callflowId}','CallFlowController@show');
	// callflow.searchByNumber
	$router->get('/accounts/{accountId}/callflows/searchByNumber/{value}','CallFlowController@searchByNumber');
	// callflow.create
	$router->put('/accounts/{accountId}/callflows','CallFlowController@store');
	// callflow.update
	$router->post('/accounts/{accountId}/callflows/{callflowId}','CallFlowController@update');
	// callflow.delete
	$router->delete('/accounts/{accountId}/callflows/{callflowId}','CallFlowController@destroy');
});
<?php 
$router->group(['middleware' => ['api','auth:api']], function () use ($router) {
	// appsLink.list
	$router->get('/accounts/{accountId}/apps_link/authorize','AppsStoreController@appsLink');
	
});
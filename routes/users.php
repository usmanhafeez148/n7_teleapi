<?php 
$router->group(['middleware' => ['api','auth:api']], function () use ($router) {
	// user.list
	$router->get('/accounts/{accountId}/users','UserController@index');
	// user.get
	$router->get('/accounts/{accountId}/users/{userId}','UserController@show');
	// user.update
	$router->post('/accounts/{accountId}/users/{userId}','UserController@update');
	// user.create
	$router->put('/accounts/{accountId}/users','UserController@store');
	// user.delete
	$router->delete('/accounts/{accountId}/users/{userId}','UserController@destroy');
	
	// user.quickcall
	$router->get('accounts/{accountId}/users/{userId}/quickcall/{number}',function(){

	});

	// user.hotdesks
	$router->get('accounts/{accountId}/users/{userId}/hotdesks',function(){

	});

	// user.updatePresence
	$router->post('accounts/{accountId}/users/{userId}/presence',function(){

	});
});
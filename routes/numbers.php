<?php 


$router->group(['middleware' => ['api','auth:api']], function () use ($router) {
 	//numbers.listClassifiers 
	$router->get('/accounts/{accountId}/phone_numbers/classifiers','NumberController@listClassifiers');	
	// numbers.search // numbers.searchBlocks
	// $router->get('/accounts/{accountId}/phone_numbers/search','NumberController@searchNumber');

	// numbers.searchCity
	// $router->get('/accounts/{accountId}/phone_numbers/prefix','NumberController@searchCity');
	// numbers.getCarrierInfo
	$router->get('/accounts/{accountId}/phone_numbers/carriers_info','NumberController@carrierInfo');
	
	// numbers.identify
	// $router->get('/accounts/{accountId}/phone_numbers/{phone_numbers}/identify','NumberController@identify');
	
	
	// numbers.activate
	$router->put('/accounts/{accountId}/phone_numbers/{phone_numbers}/activate','NumberController@activate');
	// numbers.create
	$router->put('/accounts/{accounts}/phone_numbers','NumberController@store');
	// number.createBlock
	$router->put('/accounts/{accountId}/phone_numbers_block','NumberController@createBlock');
	// numbers.update
	$router->post('/accounts/{accountId}/phone_numbers/{phoneNumber}','NumberController@update');
	// numbers.delete
	$router->delete('/accounts/{accountId}/phone_numbers/{phoneNumber}','NumberController@destroy');
	// numbers.deleteBlock
	$router->delete('/accounts/{accountId}/phone_numbers_block','NumberController@deleteBlock');

	// numbers.get
	$router->get('/accounts/{accountId}/phone_numbers/{phoneNumber}','NumberController@show');
	// numbers.list
	$router->get('/accounts/{accountId}/phone_numbers','NumberController@index');
});



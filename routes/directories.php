<?php 
$router->group(['middleware' => ['api','auth:api']], function () use ($router) {
	// directory.list
	$router->get('/accounts/{accountId}/directories','DirectoryController@index');
	// directory.get
	$router->get('/accounts/{accountId}/directories/{directoryId}','DirectoryController@show');
	// directory.create
	$router->put('/accounts/{accountId}/directories','DirectoryController@store');
	// directory.update
	$router->post('/accounts/{accountId}/directories/{directoryId}','DirectoryController@update');
	// directory.delete
	$router->delete('/accounts/{accountId}/directories/{directoryId}','DirectoryController@destroy');
});
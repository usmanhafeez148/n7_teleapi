<?php 

$router->group(['middleware' => ['api','auth:api']], function () use ($router) {
	// appsStore.getIcon
	$router->get('/accounts/{accountId}/apps_store/{appId}/icon','AppsStoreController@getIcon');
	
	// appsStore.fetchScreens
	// $router->post('/accounts/{accountId}/apps_store/{appId}/screenshot/{appScreenshotIndex}','AppsStoreController@getScreen');
	
	// appsStore.getBlacklist
	$router->get('/accounts/{accountId}/apps_store/blacklist','AppsStoreController@blacklisted');

	// appsStore.updateBlacklist
	$router->post('/accounts/{accountId}/apps_store/blacklist','AppsStoreController@updateBlacklist');

	// appsStore.update
	$router->post('/accounts/{accountId}/apps_store/{appId}','AppsStoreController@updatePermissions');
	
	// appsStore.delete
	$router->delete('/accounts/{accountId}/apps_store/{appId}','AppsStoreController@uninstall');
	// appsStore.add
	$router->put('/accounts/{accountId}/apps_store/{appId}','AppsStoreController@install');
	// appsStore.get
	$router->get('/accounts/{accountId}/apps_store/{appId}','AppsStoreController@show');
	// appsStore.list
	$router->get('/accounts/{accountId}/apps_store','AppsStoreController@index');
});
<?php 
$router->group(['middleware' => ['api','auth:api']], function () use ($router) {
	//device list
	$router->get('/accounts/{accountId}/devices','DeviceController@index');
	// device.getStatus
	$router->get('/accounts/{accountId}/devices/status','DeviceController@getStatus');
	//device.get
	$router->get('/accounts/{accountId}/devices/{deviceId}','DeviceController@show');
	// device.quickcall
	// $router->get('/accounts/{accountId}/devices/{deviceId}/quickcall/{number}',function(){

	// });
	// 
	//device.create
	$router->put('/accounts/{accountId}/devices','DeviceController@create');
	// device.sync
	$router->post('v2/accounts/{accountId}/devices/{deviceId}/sync',function(){

	});
	// device.update
	$router->post('/accounts/{accountId}/devices/{deviceId}','DeviceController@update');
	// device.delete
	$router->delete('/accounts/{accountId}/devices/{deviceId}','DeviceController@destroy');

});
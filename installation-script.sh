sudo rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
sudo rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm

#installing php
yum install php70w

#installing mysql lib for php
sudo yum install php70w-mysqlnd php70w-mbstring

#move to /tmp and install composer
cd /tmp
sudo curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer

sudo yum install php70w-xml

cd /var/www/html/n7_api/
cp .env.example .env
#add db details in above file

#generate unique jwt secret
php artisan jwt:secret

npm install -g gulp


